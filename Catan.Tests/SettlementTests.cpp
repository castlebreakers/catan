#include <catch.hpp>
#include <../Catan.Core/Settlement.h>

TEST_CASE("Settlement Constructor")
{
	Settlement settlement(Piece::Color::Blue);

	REQUIRE(settlement.getColor() == Piece::Color::Blue);
}