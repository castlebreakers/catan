#include <catch.hpp>
#include <../Catan.Core/Thief.h>
#include <../Catan.Core/Exceptions.h>
#include <../Catan.Core/HumanPlayer.h>

TEST_CASE("Thief Constructor")
{
	Board board;

	Thief thief(board);

	SECTION("Thief is initially placed on a Desert tile")
	{
		auto thiefTile = board.getTile(thief.getRow(), thief.getColumn());
		auto thiefTileType = thiefTile->resource().getType();

		REQUIRE(thiefTileType == Resource::Type::Nothing);
	}
}

TEST_CASE("Move Thief")
{
	Board board;

	Thief thief(board);

	uint8_t row;
	uint8_t column;

	SECTION("Thief cannot be placed on a non-resource tile")
	{
		//Lower bound is 1 and upper bound is 5.
		row = 1; column = 5;
		REQUIRE_NOTHROW(thief.move(row, column, board));

		row = 0; column = 6;
		REQUIRE_THROWS_AS(thief.move(row, column, board), Exceptions::InvalidThiefPositionException);

		row = 0; column = 5;
		REQUIRE_THROWS_AS(thief.move(row, column, board), Exceptions::InvalidThiefPositionException);

		row = 1; column = 6;
		REQUIRE_THROWS_AS(thief.move(row, column, board), Exceptions::InvalidThiefPositionException);
	}

}

TEST_CASE("Get players with adjacent pieces to tile")
{
	Board board;

	Thief thief(board);

	HumanPlayer player1("DaniBv", Piece::Color::Blue);
	HumanPlayer player2("ISilviu", Piece::Color::White);

	//Players place settlements on the same tile.
	player1.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper), true);
	player2.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom), true);

	//Get that tile.
	auto& tile = board.getTile(3, 3);

	//A set containing the colors of the players with a settlement on that tile.
	std::unordered_set<Piece::Color> playersColors;

	//Get those players.
	thief.getPlayersWithAdjacentPiecesToTile(*tile, playersColors);

	//Get their colors.
	auto color1 = *std::next(playersColors.begin(), 0);
	auto color2 = *std::next(playersColors.begin(), 1);

	REQUIRE(color1 == player1.getColor());

	REQUIRE(color2 == player2.getColor());

	REQUIRE_FALSE(color1 == player2.getColor());

	REQUIRE_FALSE(color2 == player1.getColor());

}