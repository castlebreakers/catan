#include <catch.hpp>
#include <../Catan.Core/DevelopmentCard.h>

TEST_CASE("Development Card Constructor")
{
	SECTION("Default Constructor")
	{
		DevelopmentCard card;

		REQUIRE(card.getType() == DevelopmentCard::Type::None);

		REQUIRE(card.isApplied());
	}

	SECTION("Custom Constructor")
	{
		DevelopmentCard card(DevelopmentCard::Type::Monopoly);

		REQUIRE(card.getType() == DevelopmentCard::Type::Monopoly);

		REQUIRE_FALSE(card.isApplied());
	}
}

TEST_CASE("Apply a Development Card")
{
	DevelopmentCard card(DevelopmentCard::Type::Soldier);

	card.apply();

	REQUIRE(card.isApplied());
}