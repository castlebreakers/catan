#include <catch.hpp>
#include <../Catan.Core/Resource.h>

TEST_CASE("Resource Constructor")
{
	SECTION("Default constructor")
	{
		Resource resource;

		REQUIRE(resource.getType() == Resource::Type::NotAType);

		REQUIRE(resource.getTileNumber() == 0);

	}
	
	SECTION("Custom constructor")
	{
		Resource resource(Resource::Type::Brick, 6);

		REQUIRE(resource.getType() == Resource::Type::Brick);

		REQUIRE(resource.getTileNumber() == 6);

	}
}