#include "catch.hpp"

#include <Catan.Core/Bank.h>
#include <Catan.Core/Exceptions.h>

TEST_CASE("Bank Constructor")
{
	CHECK_NOTHROW(Bank());	
}

TEST_CASE("Draw a Development Card")
{
	Bank bank;

	DevelopmentCard card;

	static constexpr uint8_t kNumberOfDevelopmentCards = 25;

	for (auto index = 0; index <= kNumberOfDevelopmentCards; ++index)
	{
		if (index == kNumberOfDevelopmentCards)
		{
			REQUIRE_THROWS_AS(card = bank.drawCard(), Exceptions::BankOutOfCardsException);
		}
		else
		{
			card = bank.drawCard();
		}
	}
}