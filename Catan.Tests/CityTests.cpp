#include <catch.hpp>
#include <../Catan.Core/City.h>

TEST_CASE("City Constructor")
{
	City city(Piece::Color::White);

	REQUIRE(city.getColor() == Piece::Color::White);
}