#include <catch.hpp>
#include <../Catan.Core/Tile.h>

TEST_CASE("Tile Constructor")
{
	SECTION("Resource constructor")
	{
		Resource resource(Resource::Type::Brick, 6);

		Tile tile(resource);
		
		REQUIRE(tile.resource().getType() == Resource::Type::Brick);

		REQUIRE(tile.resource().getTileNumber() == 6);
	}
	
	SECTION("Harbor constructor")
	{
		Harbor harbor(Harbor::Type::Generic);

		Tile tile(harbor);

		REQUIRE(tile.harbor().getType() == Harbor::Type::Generic);

	}
}

TEST_CASE("Variant access")
{
	SECTION("Resource")
	{
		Resource resource(Resource::Type::Brick, 6);

		Tile tile(resource);

		REQUIRE(tile.harbor().getType() == Harbor::Type::NotAType);
	}

	SECTION("Harbor")
	{
		Harbor harbor(Harbor::Type::Generic);

		Tile tile(harbor);

		REQUIRE(tile.resource().getType() == Resource::Type::NotAType);
	}
}