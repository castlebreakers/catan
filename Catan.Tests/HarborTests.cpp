#include <catch.hpp>
#include <../Catan.Core/Harbor.h>

TEST_CASE("Harbor Constructor")
{
	SECTION("Default constructor")
	{
		Harbor harbor;

		REQUIRE(harbor.getType() == Harbor::Type::NotAType);
	}

	SECTION("Custom constructor")
	{
		Harbor harbor(Harbor::Type::Generic);

		REQUIRE(harbor.getType() == Harbor::Type::Generic);
	}
}