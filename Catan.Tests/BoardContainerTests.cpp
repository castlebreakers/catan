#include <catch.hpp>
#include <../Catan.Core/BoardContainer.h>

TEST_CASE("Board Container Constructor")
{
	BoardContainer container;

	SECTION("Prepare Board Container")
	{
		REQUIRE(container.getContainer().size() == BoardContainer::kBoardSize);
	}

	SECTION("Populate Board Container")
	{
		SECTION("Water")
		{
			for (auto[row, column] : container.kWaterPositions)
				REQUIRE(container.getTile(row, column)->resource().getType() == Resource::Type::Water);
		}
		
		SECTION("Harbors")
		{
			uint8_t row;
			uint8_t column;

			row = 0; column = 3;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Generic);

			row = 0; column = 5;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Wool);
			
			row = 1; column = 6;
			
			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Generic);
		
			row = 2; column = 1;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Ore);

			row = 3; column = 6;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Generic);

			row = 4; column = 0;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Grain);

			row = 5; column = 4;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Brick);

			row = 6; column = 0;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Generic);

			row = 6; column = 2;

			REQUIRE(container.getTile(row, column)->harbor().getType() == Harbor::Type::Lumber);
		}

		SECTION("Resources")
		{
			auto resourceTiles = container.getTilesByNumber();

			for (auto tile : resourceTiles)
			{
				REQUIRE(tile.get().resource().getType() != Resource::Type::NotAType);
				REQUIRE(tile.get().resource().getType() != Resource::Type::Water);
			}
		}
	}
}

TEST_CASE("Get tiles by number")
{
	BoardContainer container;

	SECTION("Get all tiles")
	{
		auto allTiles = container.getTilesByNumber();
		
		for (auto tile : allTiles)
		{
			REQUIRE(tile.get().resource().getType() != Resource::Type::NotAType);
			REQUIRE(tile.get().resource().getType() != Resource::Type::Water);
		}
	}

	SECTION("Get by number")
	{
		auto tiles = container.getTilesByNumber(6);

		REQUIRE(tiles.size() == 2);
	}
}
