#include <catch.hpp>
#include <../Catan.Core/Hand.h>
#include <../Catan.Core/Exceptions.h>

TEST_CASE("Add resources to hand")
{
	Hand hand;

	SECTION("Add default number of resources")
	{
		hand.add(Resource::Type::Brick);

		REQUIRE(hand.totalNumberOfResourceCards() == 1);
	}

	SECTION("Add custom number of resources")
	{
		hand.add(Resource::Type::Wool, 3);

		REQUIRE(hand.totalNumberOfResourceCards() == 3);
	}
}

TEST_CASE("Discard resources from hand")
{
	Hand hand;

	hand.add(Resource::Type::Brick, 3);

	SECTION("Discard default number of resources")
	{
		hand.discard(Resource::Type::Brick);

		REQUIRE(hand.totalNumberOfResourceCards() == 2);
	}

	SECTION("Discard custom number of resources")
	{
		hand.discard(Resource::Type::Brick, 2);

		REQUIRE(hand.totalNumberOfResourceCards() == 1);
	}

	SECTION("Discard an insufficient number of resources")
	{
		REQUIRE_THROWS_AS(hand.discard(Resource::Type::Brick, 10), Exceptions::ResourceNotEnoughException);
	}

	SECTION("Discard a non-existing resource")
	{
		REQUIRE_THROWS_AS(hand.discard(Resource::Type::Wool, 2), Exceptions::ResourceNotAvailableException);
	}
}

TEST_CASE("Return the number of resources of a certain type")
{
	Hand hand;

	hand.add(Resource::Type::Brick, 5);
	hand.add(Resource::Type::Wool, 2);

	SECTION("If there is at least one resource of that type")
	{
		REQUIRE(hand.numberOfCards(Resource::Type::Brick) == 5);
		REQUIRE_FALSE(hand.numberOfCards(Resource::Type::Wool) == 5);
	}

	SECTION("If there are no resources of that type")
	{
		REQUIRE(hand.numberOfCards(Resource::Type::Ore) == 0);
	}
}

TEST_CASE("Return total number of resources")
{
	Hand hand;

	hand.add(Resource::Type::Brick, 4);
	hand.add(Resource::Type::Wool, 2);
	hand.add(Resource::Type::Ore, 1);
	hand.add(Resource::Type::Lumber, 3);

	REQUIRE(hand.totalNumberOfResourceCards() == 10);
}

TEST_CASE("Add a Development Card")
{
	Hand hand;

	DevelopmentCard developmentCard;

	hand.add(developmentCard);

	REQUIRE(hand.totalNumberOfDevelopmentCards() == 1);
}

TEST_CASE("Return total number of Development Cards")
{
	Hand hand;

	DevelopmentCard card1;
	DevelopmentCard card2;
	DevelopmentCard card3;

	hand.add(card1);
	hand.add(card2);
	hand.add(card3);

	REQUIRE(hand.totalNumberOfDevelopmentCards() == 3);
}

TEST_CASE("Get an unused Development Card from hand")
{
	Hand hand;

	DevelopmentCard card1(DevelopmentCard::Type::Monopoly);
	DevelopmentCard card2(DevelopmentCard::Type::Soldier);
	
	hand.add(card1);
	hand.add(card2);

	auto monopolyCard = hand[DevelopmentCard::Type::Monopoly];

	REQUIRE(monopolyCard.getType() == DevelopmentCard::Type::Monopoly);

	REQUIRE_FALSE(monopolyCard.isApplied());
}

TEST_CASE("Mark a Development Card as applied")
{
	Hand hand;

	SECTION("If the card's type is different than 'none' then mark it as applied")
	{
		//This constructor initializes m_type with a valid type and m_isApplied with false
		DevelopmentCard card(DevelopmentCard::Type::VictoryPoint);

		hand.add(card);

		auto& devCard = hand[DevelopmentCard::Type::VictoryPoint];

		hand.markAsApplied(devCard);

		REQUIRE(devCard.isApplied());
	}
}

TEST_CASE("Discard half of the number of resources in hand")
{
	Hand hand;

	hand.add(Resource::Type::Brick, 4);
	hand.add(Resource::Type::Wool, 2);
	hand.add(Resource::Type::Ore, 1);
	hand.add(Resource::Type::Lumber, 3);

	hand.discardHalf();

	REQUIRE(hand.totalNumberOfResourceCards() == 5);
}