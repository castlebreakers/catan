#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "../Catan.Core/HumanPlayer.h"
#include "../Catan.Core/Exceptions.h"
#include <Catan.Core/Bank.h>

TEST_CASE("Human Player Constructor")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);

	SECTION("Human Player Constructor correctness")
	{
		REQUIRE(player.getName() == "Bogdan");

		REQUIRE(player.getColor() == Piece::Color::Blue);

		REQUIRE_FALSE(player.getName() == "Silviu");

		REQUIRE_FALSE(player.getColor() == Piece::Color::White);

		// When we construct a player , he has 0 points
		REQUIRE(player.getNumberOfPoints() == 0);

		REQUIRE_FALSE(player.getNumberOfPoints() == 10);

	}
}

TEST_CASE("Human Player Rolls Dices->Dice numbers must be beetween 1 and 6")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);
	HumanPlayer::DiceRoll newDiceRoll = player.requestDiceInput();
	//Catch 2 doesn't support complex expression ( a!=b && b>a )

	CHECK(newDiceRoll.first >= 1); 
	CHECK(newDiceRoll.first <= 6);

	CHECK(newDiceRoll.second >= 1);
	CHECK(newDiceRoll.second <= 6);
		
}

TEST_CASE("Human Player Rolls Dice")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);
	HumanPlayer::DiceRoll newDiceRoll = player.requestDiceInput();

	CHECK_FALSE(newDiceRoll.first >= 7);
	CHECK_FALSE(newDiceRoll.first <= 0);

	CHECK_FALSE(newDiceRoll.second >= 7);
	CHECK_FALSE(newDiceRoll.second <= 0);
}

TEST_CASE("Human Player places a settlement -> First Turn ")
{
	SECTION("It's first turn !") //-> first turn we don't take resources and we don't mind if the settlment does't connect with a road
	{
		HumanPlayer player("Bogdan", Piece::Color::Blue);
		HumanPlayer anotherPlayer("Daniel", Piece::Color::Orange);
		Board board;
		/*No exception should be thrown because :
		- valid position
		- no resource required
		- no adjacent settlement/city that might interfere with the player placing that settlement
		*/
		REQUIRE_NOTHROW(player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom), true));

		/*Exception should be thrown because :
		- there's allready a a piece on that corner
		*/
		REQUIRE_THROWS_AS(anotherPlayer.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom), true), Exceptions::PieceAlreadyExistsException);

		// - adjacent intersection must be empty if someone tries to place a settlment
		REQUIRE_THROWS_AS(anotherPlayer.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight), true), Exceptions::CouldNotPlacePieceException);

		// - (0,4) isn't a valid position to place -> there's water!
		REQUIRE_THROWS_AS(anotherPlayer.placeSettlement(board, HumanPlayer::CornerPosition(0, 4, Hex::Corner::Upper), true), Exceptions::TileNotAccessibleException);

	}
}

TEST_CASE("Human Player places a settlement -> Not first turn Turn ")
{
	SECTION("It's not the first turn")
	{
		HumanPlayer player("Bogdan", Piece::Color::Blue);
		Board board;

		// - it's a valid location to place but the player doesn't have any resources , so let's give him some :)  
		REQUIRE_THROWS_AS(player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom)) , Exceptions::ResourceNotEnoughException);

		player.hand.add(Resource::Type::Brick);
		player.hand.add(Resource::Type::Lumber);
		player.hand.add(Resource::Type::Wool);
		player.hand.add(Resource::Type::Grain);

		// - still throws error because we must connect the placed settlement with an existing road 
		REQUIRE_THROWS_AS(player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom)) , Exceptions::AdjacentRoadNotExistingException);

		// so we will pretend it's first turn so we can place a seetlment and 2 roads :D
		REQUIRE_NOTHROW(player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Bottom) , true));
		REQUIRE_NOTHROW(player.placeRoad(board, HumanPlayer::EdgePosition(4, 2, Hex::Edge::Right), true));
		REQUIRE_NOTHROW(player.placeRoad(board, HumanPlayer::EdgePosition(4, 2, Hex::Edge::BottomRight), true));

		//now we place settlement and take players resource
		REQUIRE_NOTHROW(player.placeSettlement(board, HumanPlayer::CornerPosition(4, 2, Hex::Corner::Bottom)));

		//and we will check if he has any resource left
		REQUIRE(player.hand.totalNumberOfResourceCards() == 0);

	}
}

TEST_CASE("Human Player places a road - FT")
{
	SECTION("First turn")
	{
		HumanPlayer player("Bogdan", Piece::Color::Blue);
		Board board;

		// even if is first turn , we can't place a road without being connected to another road/settlment/city
		REQUIRE_THROWS_AS(player.placeRoad(board, HumanPlayer::EdgePosition(3, 3, Hex::Edge::Right),true) ,Exceptions::NoNeighbourPieceFoundException);

		//so we will place a settlment there
		player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight), true);

		//and now we will try to place a road that connect to that settlement
		REQUIRE_NOTHROW(player.placeRoad(board, HumanPlayer::EdgePosition(4, 3, Hex::Edge::UpperLeft), true));

		//and place a road that will connect to the road we placed before
		REQUIRE_NOTHROW(player.placeRoad(board, HumanPlayer::EdgePosition(4, 3, Hex::Edge::Left), true));
	}
}

TEST_CASE("Human Player places a road")
{
	SECTION("It's not the first turn")
	{
		HumanPlayer player("Bogdan", Piece::Color::Blue);
		Board board;

		player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight), true);

		// Trows because player doesn't have necessary resource to buy a road
		REQUIRE_THROWS_AS(player.placeRoad(board, HumanPlayer::EdgePosition(3, 3, Hex::Edge::Right)), Exceptions::ResourceNotAvailableException);

		//so let's give that player some resource to spend
		player.hand.add(Resource::Type::Brick, 5);
		player.hand.add(Resource::Type::Lumber, 5);

		//and try again to place a road
		REQUIRE_NOTHROW(player.placeRoad(board, HumanPlayer::EdgePosition(3, 3, Hex::Edge::Right)));

		//let's see how many resource the player has -> he got to have 8 (4 brick and 4 lumber)

		REQUIRE(player.hand.numberOfCards(Resource::Type::Brick) == 4);
		REQUIRE(player.hand.numberOfCards(Resource::Type::Lumber) == 4);

		// and the total number of resources shoult be 8 
		REQUIRE(player.hand.totalNumberOfResourceCards() == 8);
	}
}

TEST_CASE("Human Player Places a City")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);
	Board board;

	//Throws because the player doesn't have any resource
	REQUIRE_THROWS_AS(player.placeCity(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight)) , Exceptions::ResourceNotEnoughException);

	//so let's give him some
	player.hand.add(Resource::Type::Grain, 6);
	player.hand.add(Resource::Type::Ore, 4);

	//still throws because or player doesn't have a settlement there
	REQUIRE_THROWS_AS(player.placeCity(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight)), Exceptions::CouldNotPlaceCityException);

	//so let's give him the opportunity to place one for free 
	REQUIRE_NOTHROW(player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight), true));

	//and now upgrade it to a city !! -> bug from catch2
	//REQUIRE_NOTHROW(player.placeCity(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomRight)));

}

TEST_CASE("Victory Point Card")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);

	//we dont have a victory point card but we still try to play it -> throws exception
	REQUIRE_THROWS_AS(player.playDevelopmentCard() , Exceptions::RequestedCardNotFoundException) ;

	//lets give him a victory point card
	player.hand.add(DevelopmentCard(DevelopmentCard::Type::VictoryPoint));
	REQUIRE_NOTHROW(player.playDevelopmentCard());

	//if we played a victory point means the player should have 1 point
	REQUIRE(player.getNumberOfPoints() == 1);

}

TEST_CASE("Year of Plenty Card")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);

	//we dont have a year of plenty card but we still try to play it -> throws exception
	REQUIRE_THROWS_AS(player.playDevelopmentCard(Resource::Type::Brick , Resource::Type::Grain), Exceptions::RequestedCardNotFoundException);

	//lets give him a year of plenty card
	player.hand.add(DevelopmentCard(DevelopmentCard::Type::YearOfPlenty));

	REQUIRE_NOTHROW(player.playDevelopmentCard(Resource::Type::Brick, Resource::Type::Grain));

	//-> players must have one Brick and Grain resource

	REQUIRE(player.hand.numberOfCards(Resource::Type::Brick) == 1);
	REQUIRE(player.hand.numberOfCards(Resource::Type::Grain) == 1);
}

TEST_CASE("Road Building Card")
{
	HumanPlayer player("Bogdan", Piece::Color::Blue);
	Board board;
	//we dont have a Road Building card but we still try to play it -> throws exception
	REQUIRE_THROWS_AS(player.playDevelopmentCard(board,HumanPlayer::EdgePosition(3, 3,Hex::Edge::Right), 
													   HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperRight)),
													   Exceptions::RequestedCardNotFoundException);

	//lets give him a year of plenty card
	player.hand.add(DevelopmentCard(DevelopmentCard::Type::RoadBuilding));

	//and make him place a settlment too 
	player.placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::UpperRight), true);

	REQUIRE_NOTHROW(player.playDevelopmentCard(board, HumanPlayer::EdgePosition(3, 3, Hex::Edge::Right),
												      HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperRight)));

}

TEST_CASE("Monopoly Card ")
{
	std::vector<std::shared_ptr<HumanPlayer>> players {
		std::make_shared<HumanPlayer>("Bogdanel", Piece::Color::Red),
		std::make_shared<HumanPlayer>("Stefi", Piece::Color::Blue),
		std::make_shared<HumanPlayer>("DaniBv", Piece::Color::White)
	};

	players[0]->hand.add(Resource::Type::Brick, 3);
	players[0]->hand.add(Resource::Type::Lumber, 3);

	players[1]->hand.add(Resource::Type::Brick, 3);
	players[1]->hand.add(Resource::Type::Lumber, 3);

	players[2]->hand.add(Resource::Type::Brick, 3);
	players[2]->hand.add(Resource::Type::Lumber, 3);
	
	auto player = std::make_shared<HumanPlayer>("ISilviu", Piece::Color::Orange);

	//lets give player1 a Monopoly card
	player->hand.add(DevelopmentCard(DevelopmentCard::Type::Monopoly));

	REQUIRE_NOTHROW(player->playDevelopmentCard(players, Resource::Type::Brick));

	//lets see if player1 got all the brick resources , he should have 9 bricks
	REQUIRE(player->hand.numberOfCards(Resource::Type::Brick) == 9);
}

TEST_CASE("Soldier Card")
{
	Board board;
	Thief thief(board);

	auto player = std::make_shared<HumanPlayer>("ISilviu", Piece::Color::Orange);
	
	std::vector<std::shared_ptr<HumanPlayer>> players{
			std::make_shared<HumanPlayer>("Bodi", Piece::Color::Red),
			std::make_shared<HumanPlayer>("Stefi", Piece::Color::Blue)
	};

	players[0]->hand.add(Resource::Type::Brick);
	players[0]->hand.add(Resource::Type::Lumber);

	players[1]->hand.add(Resource::Type::Grain);
	players[1]->hand.add(Resource::Type::Wool);

	players[1]->placeSettlement(board, HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper ),true);

	//lets give player1 a Solider card
	player->hand.add(DevelopmentCard(DevelopmentCard::Type::Soldier));

	player->playDevelopmentCard(3, 3, board, thief, players);

	//now player1 should have one resource
	REQUIRE(player->hand.totalNumberOfResourceCards() == 1);

	//and player2 one resource beacause player1 stole one from him
	REQUIRE(player->hand.totalNumberOfResourceCards() == 1);
}

TEST_CASE("Buy a development Card")
{
	HumanPlayer player1("Daniel", Piece::Color::Blue);
	Bank bank;

	//let's say we drawed a card from bank
	DevelopmentCard card = bank.drawCard();

	//can't buy that card because the player doesn't have the necessary resource to buy that card
	REQUIRE_THROWS_AS(player1.buy(card), Exceptions::ResourceNotEnoughException);

	//let's give him resource
	player1.hand.add(Resource::Type::Ore);
	player1.hand.add(Resource::Type::Wool);
	player1.hand.add(Resource::Type::Grain);

	REQUIRE_NOTHROW(player1.buy(card));
}

TEST_CASE("Trade with Bank")
{
	HumanPlayer player1("Daniel", Piece::Color::Blue);
	
	player1.hand.add(Resource::Type::Brick, 3);

	//can't trade if you don't have 4 resource of a kind
	REQUIRE_THROWS_AS(player1.tradeResourcesWithBank(Resource::Type::Brick, Resource::Type::Grain) , Exceptions::ResourceNotEnoughException);

	player1.hand.add(Resource::Type::Brick);

	REQUIRE_NOTHROW(player1.tradeResourcesWithBank(Resource::Type::Brick, Resource::Type::Grain));

	//now the player should have zero bricks and one grain
	REQUIRE(player1.hand.numberOfCards(Resource::Type::Brick) == 0);
	REQUIRE(player1.hand.numberOfCards(Resource::Type::Grain) == 1);

}