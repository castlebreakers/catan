#include <catch.hpp>
#include <../Catan.Core/Game.h>

TEST_CASE("Get available road edges")
{
	auto players = {
	  std::make_shared<HumanPlayer>("Bogdanel", Piece::Color::Red),
	  std::make_shared<HumanPlayer>("Stefi"    , Piece::Color::Blue)
	};

	auto game = Game::create(players.begin(), players.end());

	game->placeSettlementFirstTurn(HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperLeft));

	auto availableEdges = game->getAvailableEdges();

	SECTION("Total number of available edges")
	{
		REQUIRE(availableEdges.size() == 8);
	}
	
	SECTION("Available edges positions")
	{
		REQUIRE(availableEdges[0] == HumanPlayer::EdgePosition(2, 3, Hex::Edge::Right));
		REQUIRE(availableEdges[1] == HumanPlayer::EdgePosition(2, 3, Hex::Edge::BottomLeft));
		REQUIRE(availableEdges[2] == HumanPlayer::EdgePosition(2, 4, Hex::Edge::BottomLeft));
		REQUIRE(availableEdges[3] == HumanPlayer::EdgePosition(2, 4, Hex::Edge::Left));
		REQUIRE(availableEdges[4] == HumanPlayer::EdgePosition(3, 2, Hex::Edge::UpperRight));
		REQUIRE(availableEdges[5] == HumanPlayer::EdgePosition(3, 2, Hex::Edge::Right));
		REQUIRE(availableEdges[6] == HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperRight));
		REQUIRE(availableEdges[7] == HumanPlayer::EdgePosition(3, 3, Hex::Edge::Left));
	}	
}

TEST_CASE("Get available settlement corners")
{
	auto players = {
	  std::make_shared<HumanPlayer>("Bodi", Piece::Color::Red),
	  std::make_shared<HumanPlayer>("Stefi"    , Piece::Color::Blue)
	};

	auto game = Game::create(players.begin(), players.end());

	game->placeSettlementFirstTurn(HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperLeft));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::Left));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 2, Hex::Edge::UpperRight));

	auto availableSettlementsCorners = game->getAvailableCornersSettlement();

	SECTION("Total number of available corners")
	{
		REQUIRE(availableSettlementsCorners.size() == 6);
	}

	SECTION("Available edges positions")
	{
		REQUIRE(availableSettlementsCorners[0] == HumanPlayer::CornerPosition(2, 2, Hex::Corner::BottomRight));
		REQUIRE(availableSettlementsCorners[1] == HumanPlayer::CornerPosition(2, 3, Hex::Corner::BottomLeft));
		REQUIRE(availableSettlementsCorners[2] == HumanPlayer::CornerPosition(3, 2, Hex::Corner::Upper));
		REQUIRE(availableSettlementsCorners[3] == HumanPlayer::CornerPosition(3, 2, Hex::Corner::BottomRight));
		REQUIRE(availableSettlementsCorners[4] == HumanPlayer::CornerPosition(3, 3, Hex::Corner::BottomLeft));
		REQUIRE(availableSettlementsCorners[5] == HumanPlayer::CornerPosition(4, 2, Hex::Corner::Upper));
	}
}

TEST_CASE("Get available city corners")
{
	auto players = {
	  std::make_shared<HumanPlayer>("Bodi", Piece::Color::Red),
	  std::make_shared<HumanPlayer>("Stefi"    , Piece::Color::Blue)
	};

	auto game = Game::create(players.begin(), players.end());

	game->placeSettlementFirstTurn(HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperLeft));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::Left));

	auto availableCitysCorners = game->getAvailableCornersCity();

	SECTION("Total number of available corners")
	{
		REQUIRE(availableCitysCorners.size() == 3);
	}

	SECTION("Available edges positions")
	{
		REQUIRE(availableCitysCorners[0] == HumanPlayer::CornerPosition(2, 3, Hex::Corner::BottomRight));
		REQUIRE(availableCitysCorners[1] == HumanPlayer::CornerPosition(2, 4, Hex::Corner::BottomLeft));
		REQUIRE(availableCitysCorners[2] == HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper));
	}
}

TEST_CASE("Recieve resources on dice roll")
{
	auto players = {
	  std::make_shared<HumanPlayer>("Bodi", Piece::Color::Red),
	  std::make_shared<HumanPlayer>("Stefi"    , Piece::Color::Blue)
	};

	auto game = Game::create(players.begin(), players.end());
	game->placeSettlementFirstTurn(HumanPlayer::CornerPosition(3, 3, Hex::Corner::Upper));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::UpperLeft));
	game->placeRoadFirstTurn(HumanPlayer::EdgePosition(3, 3, Hex::Edge::Left));

	auto tileNumber = game->board->getTile(3, 3)->resource().getTileNumber();

	SECTION("When Thief is placed on a non-desert tile with settlements/cities")
	{
		game->thief->move(3, 3, *game->board);

		game->receiveResource(HumanPlayer::DiceRoll(tileNumber - 3, 3));

		REQUIRE(game->getCurrentPlayer()->hand.totalNumberOfResourceCards() == 0);
	}
}