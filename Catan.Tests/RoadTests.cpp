#include <catch.hpp>
#include <../Catan.Core/Road.h>

TEST_CASE("Road Constructor")
{
	Road road(Piece::Color::Orange);

	REQUIRE(road.getColor() == Piece::Color::Orange);
}