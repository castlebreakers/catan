#pragma once
#include <cstdint>

#include <Catan.Core/Tile.h>
#include <Catan.Core/BoardContainer.h>

#include "catan_core_exports.h"

class Thief;

class CATAN_CORE_EXPORTS Board
{
	friend class Thief;
public:
	Board();

	std::optional<Tile>& getTile(uint8_t row, uint8_t column);

	const std::optional<Tile>& getTile(uint8_t row, uint8_t column) const;

	const BoardContainer& getBoardContainer() const noexcept;

	const std::vector<std::reference_wrapper<Tile>> getTiles(uint16_t number) noexcept;

	const std::vector<std::reference_wrapper<Tile>> getTiles() noexcept;

	BoardContainer::pair getIndices(const Tile& tile) const noexcept;

	BoardContainer::pair getDesertTile() const noexcept;
private:
	BoardContainer m_container;
};

