#include "DevelopmentCard.h"

 DevelopmentCard::DevelopmentCard() :
	m_type(DevelopmentCard::Type::None),
	m_isApplied(true)
{
}

constexpr DevelopmentCard::DevelopmentCard(Type type) noexcept :
	m_type(type),
	m_isApplied(false)
{
}

constexpr DevelopmentCard::Type DevelopmentCard::getType() const noexcept
{
	return m_type;
}

constexpr bool DevelopmentCard::isApplied() const noexcept
{
	return m_isApplied;
}

void DevelopmentCard::apply() noexcept
{
	m_isApplied = true;
}
