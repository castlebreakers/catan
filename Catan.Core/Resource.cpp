#include "Resource.h"

constexpr Resource::Resource() noexcept :
	m_type(kDefaultResourceType),
	m_tileNumber(0)
{}

constexpr Resource::Resource(Type type, uint16_t tileNumber) noexcept :
	m_type(type),
	m_tileNumber(tileNumber)
{}

constexpr Resource::Type Resource::getType() const noexcept
{
	return m_type;
}

constexpr uint16_t Resource::getTileNumber() const noexcept
{
	return m_tileNumber;
}
