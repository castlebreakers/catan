#pragma once

#include "catan_core_exports.h"

#include <Catan.Core/DevelopmentCard.h>

class HumanPlayer;
using CornerPosition = std::tuple<uint8_t, uint8_t, Hex::Corner>;

class CATAN_CORE_EXPORTS IPlayerObserver
{
public:
	virtual ~IPlayerObserver() = default;

	virtual void onDevelopmentCardPlay(const std::shared_ptr<HumanPlayer>& player, DevelopmentCard::Type type) = 0;

	virtual void onNumberOfPointsChange(const std::shared_ptr<HumanPlayer>& player, uint8_t numberOfPoints) = 0;

	virtual void onPiecePlace(const std::shared_ptr<HumanPlayer>& player, CornerPosition position) = 0;
};


