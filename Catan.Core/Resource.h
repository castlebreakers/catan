#pragma once
#include <cstdint>

#include "catan_core_exports.h"

class CATAN_CORE_EXPORTS Resource
{
public:

	enum class Type : uint8_t
	{
		Brick,
		Wool,
		Ore,
		Grain,
		Lumber,
		Nothing,
		NotAType,
		Water
	};

public:
	constexpr Resource() noexcept;

	constexpr Resource(Type type, uint16_t tileNumber) noexcept;

	constexpr Type getType() const noexcept;

	constexpr uint16_t getTileNumber() const noexcept;

private:
	Type m_type;

	uint16_t m_tileNumber;

	static constexpr Resource::Type kDefaultResourceType = Resource::Type::NotAType;
};

constexpr bool CATAN_CORE_EXPORTS operator == (const Resource& lhs, const Resource& rhs) noexcept
{
	return lhs.getType() == rhs.getType() && lhs.getTileNumber() == rhs.getTileNumber();
}