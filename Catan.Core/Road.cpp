#include "Road.h"

constexpr Road::Road(Piece::Color color) noexcept:
	Piece(color)
{}

uint8_t Road::getNumberOfPoints() const noexcept
{
	return kNumberOfPoints;
}
