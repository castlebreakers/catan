#include "Game.h"
#include <Catan.Utils/polymorphics.hpp>
#include <Catan.Core/Exceptions.h>
#include <algorithm>
#include <unordered_map>

namespace
{
	using str = std::string_view;

	const auto harborPosition = std::unordered_map<str, std::pair<uint8_t, uint8_t>>
	{
		{	str{"UpperLeft"}      , std::make_pair(0, 3)	},
		{   str{"Upper"}          , std::make_pair(0, 5)	},
		{	str{"UpperRight"}     , std::make_pair(1, 6)	},
		{	str{"Right"}          , std::make_pair(3, 6)	},
		{	str{"BottomRight"}    , std::make_pair(5, 4)	},
		{	str{"Bottom"}         , std::make_pair(6, 2)	},
		{	str{"BottomLeft"}     , std::make_pair(6, 0)	},
		{	str{"BottomUpperLeft"}, std::make_pair(4, 0)	},
		{	str{"UpperBottomLeft"}, std::make_pair(2, 1)	}
	};

}

std::shared_ptr<HumanPlayer> getPlayer(std::vector<std::shared_ptr<HumanPlayer>>& players, Piece::Color color)
{
	for (auto& player : players)
	{
		if (player->getColor() == color)
			return player;
	}
	return nullptr;
}

const std::shared_ptr<HumanPlayer> Game::getCurrentPlayer() const noexcept
{
	return m_currentPlayer;
}


void Game::nextPlayer()
{
	static bool wasSecondTurn = false;

	auto currentPlayer = players->front();

	players->erase(players->begin());
	players->emplace_back(currentPlayer);

	if ((*players->front()) == initialFirst && (*players->back()) == initialLast)
	{
		if (!wasSecondTurn)
			++m_turnNumber;
	}

	static constexpr uint8_t kSpecialTurnNumberSpecifier = 2;

	if (m_turnNumber == kSpecialTurnNumberSpecifier && (*players->front()) == initialLast &&
		(*players->back()) == initialFirst)
		++m_turnNumber;

	if (!wasSecondTurn)
	{
		if (m_turnNumber == kSpecialTurnNumberSpecifier)
		{
			wasSecondTurn = true;
			std::reverse(std::begin(*players), std::end(*players));

			m_currentPlayer = players->front();

			return;
		}
	}

	static bool hasSecondTurnPassed = false;
	if (m_turnNumber > kSpecialTurnNumberSpecifier && !hasSecondTurnPassed)
	{
		hasSecondTurnPassed = true;
		std::reverse(std::begin(*players), std::end(*players));
	}

	m_currentPlayer = players->front();
}

void Game::onDevelopmentCardPlay(const std::shared_ptr<HumanPlayer>& player, DevelopmentCard::Type type)
{
	if (type == DevelopmentCard::Type::Soldier)
	{
		auto it = m_soldierCardPoints.find(player);
		static constexpr uint8_t kDefaultNumberOfPointsToAdd = 1;

		if (it == std::end(m_soldierCardPoints))
		{
			m_soldierCardPoints[player] = kDefaultNumberOfPointsToAdd;
		}
		else
		{
			m_soldierCardPoints[player] += kDefaultNumberOfPointsToAdd;
		}
	}
}

void Game::onNumberOfPointsChange(const std::shared_ptr<HumanPlayer>& player, uint8_t numberOfPoints)
{
	static constexpr uint8_t kDefaultNumberOfPointsNeededToWin = 10;

	if (numberOfPoints == kDefaultNumberOfPointsNeededToWin)
	{
		m_hasWinner = true;
		m_winner = player;
	}
}

void Game::onPiecePlace(const std::shared_ptr<HumanPlayer>& player, HumanPlayer::CornerPosition position)
{
	auto[row, column, corner] = position;

	auto it = std::find(std::begin(BoardContainer::kHarborPositions), std::end(BoardContainer::kHarborPositions),
		std::make_pair(row, column));

	if (it != std::end(BoardContainer::kHarborPositions))
	{
		auto& tile = board->getTile(row, column);

		if (tile)
		{
			/*m_playerHarborOpening[player].push_back(tile->harbor().getType());
*/
			auto harborType = tile->harbor().getType();
			
			static constexpr uint8_t kSpecificPortNumberOfResources = 2;
			static constexpr uint8_t kGenericPortNumberOfResources = 3;

			auto playerRequirements = m_playerTradeRequirements.find(player);
			if (playerRequirements == std::end(m_playerTradeRequirements))
				return;

			if (harborType == Harbor::Type::Generic)
			{
				/*auto it = std::find_if(std::begin(playerRequirements), std::end(*playerRequirements),
					[harborType](std::pair<Harbor::Type, uint8_t>& requirement)
				{
					return requirement.first == harborType;
				});

				if (it != std::end(playerRequirements))
					*it = std::make_pair(harborType, kGenericPortNumberOfResources);
*/
		/*		m_playerTradeRequirements[player].push_back(std::make_pair(harborType, kGenericPortNumberOfResources));
		*/	}
			else if (harborType != Harbor::Type::NotAType)
			{
				/*auto it = std::find_if(std::begin(playerRequirements), std::end(playerRequirements),
					[harborType](std::pair<Harbor::Type, uint8_t>& requirement)
				{
					return requirement.first == harborType;
				});

				if (it != std::end(playerRequirements))
					*it = std::make_pair(harborType, kSpecificPortNumberOfResources);*/

		/*		m_playerTradeRequirements[player].push_back(std::make_pair(harborType, kSpecificPortNumberOfResources));
		*/	}

		}
	}
}

void Game::placeSettlementFirstTurn(HumanPlayer::CornerPosition settlementPosition)
{
	m_currentPlayer->placeSettlement(*board, settlementPosition, true);
}

void Game::placeRoadFirstTurn(HumanPlayer::EdgePosition roadPosition)
{
	m_currentPlayer->placeRoad(*board, roadPosition, true);
}

void Game::receiveResource(HumanPlayer::DiceRoll roll)
{
	std::vector<std::reference_wrapper<Tile>> tiles;

	tiles = board->getTiles(roll.first + roll.second);

	for (auto& tile : tiles)
	{
		if (tile.get().resource().getType() == Resource::Type::Water || 
			tile.get().resource().getType() == Resource::Type::NotAType)
			continue;

		auto[row, column] = board->getIndices(tile);
		if (thief->getRow() == row && thief->getColumn() == column)
			continue;

		auto&[firstIterator, lastIterator] = tile.get().hex.getCornerPieces();

		for(; firstIterator != lastIterator; ++firstIterator)
		{
			if (firstIterator->second)
			{
				auto pieceColor = firstIterator->second->getColor();
				auto& player = getPlayer(*players, pieceColor);

				if (!player)
					continue;

				if (instanceof<Settlement>(firstIterator->second))
				{
					player->hand.add(tile.get().resource().getType());
				}

				if (instanceof<City>(firstIterator->second))
				{
					player->hand.add(tile.get().resource().getType(), 2);
				}
			}
		}
	}
}

void Game::receiveResource(HumanPlayer::CornerPosition corner)
{
	auto&[row, column, cornerHex] = corner;

	auto& tile = board->getTile(row, column);

	auto addTileResourceToHand = [this](const std::optional<Tile>& tile) {
		if (!tile) return;
		if (tile->resource().getType() == Resource::Type::Water ||
			tile->resource().getType() == Resource::Type::NotAType)
			return;
		m_currentPlayer->hand.add(tile->resource().getType());
	};

	if (tile->hex[cornerHex])
	{
		m_currentPlayer->hand.add(tile->resource().getType());
	}

	switch (cornerHex)
	{
	case Hex::Corner::Upper:
	{
		auto& upperLeftTile = board->getTile(row - 1, column);
		auto& upperRightTile = board->getTile(row - 1, column + 1);

		addTileResourceToHand(upperLeftTile);
		addTileResourceToHand(upperRightTile);

		break;
	}
	case Hex::Corner::UpperLeft:
	{
		auto& upperLeftTile = board->getTile(row - 1, column);
		auto& leftTile = board->getTile(row, column - 1);

		addTileResourceToHand(upperLeftTile);
		addTileResourceToHand(leftTile);

		break;
	}
	case Hex::Corner::UpperRight:
	{
		auto& upperRightTile = board->getTile(row - 1, column + 1);
		auto& rightTile = board->getTile(row, column + 1);

		addTileResourceToHand(upperRightTile);
		addTileResourceToHand(rightTile);

		break;
	}
	case Hex::Corner::Bottom:
	{
		auto& bottomLeftTile = board->getTile(row + 1, column - 1);
		auto& bottomRightTile = board->getTile(row + 1, column);

		addTileResourceToHand(bottomLeftTile);
		addTileResourceToHand(bottomRightTile);

		break;
	}
	case Hex::Corner::BottomRight:
	{
		auto& rightTile = board->getTile(row, column + 1);
		auto& bottomRightTile = board->getTile(row + 1, column);

		addTileResourceToHand(rightTile);
		addTileResourceToHand(bottomRightTile);

		break;
	}
	case Hex::Corner::BottomLeft:
	{
		auto& leftTile = board->getTile(row - 1, column);
		auto& bottomLeftTile = board->getTile(row + 1, column - 1);
		
		addTileResourceToHand(leftTile);
		addTileResourceToHand(bottomLeftTile);

		break;
	}
	}
}

void Game::tradeResourcesWithHarbor(std::pair<uint8_t, uint8_t> position, Resource::Type givenType, Resource::Type requestedType, HumanPlayer& player)
{
	auto&[row, column] = position;
	std::optional<Tile> tile = board->getTile(row, column);
	
	static constexpr uint8_t kNumberOfResourcesToTradeWithGeneric = 3;
	static constexpr uint8_t kNumberOfResourcesToTradeWithSpecific = 2;

	if (tile)
	{
		
		if (auto[kRow, kColumn] = harborPosition.at("UpperLeft"); 
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::BottomRight])
			{
				if (tile->hex[Hex::Corner::BottomRight]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::Bottom])
			{
				if (tile->hex[Hex::Corner::Bottom]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("Upper");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::BottomLeft])
			{
				if (tile->hex[Hex::Corner::BottomLeft]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Wool)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::Bottom])
			{
				if (tile->hex[Hex::Corner::Bottom]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Wool)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("UpperRight");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::BottomLeft])
			{
				if (tile->hex[Hex::Corner::BottomLeft]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::Bottom])
			{
				if (tile->hex[Hex::Corner::Bottom]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("Right");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::BottomLeft])
			{
				if (tile->hex[Hex::Corner::BottomLeft]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::UpperLeft])
			{
				if (tile->hex[Hex::Corner::UpperLeft]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("BottomRight");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::UpperLeft])
			{
				if (tile->hex[Hex::Corner::UpperLeft]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Brick)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::Upper])
			{
				if (tile->hex[Hex::Corner::Upper]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Brick)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("BottomLeft");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::Upper])
			{
				if (tile->hex[Hex::Corner::Upper]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::UpperRight])
			{
				if (tile->hex[Hex::Corner::UpperRight]->getColor() == player.getColor())
				{
					if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithGeneric)
					{
						player.hand.discard(givenType, kNumberOfResourcesToTradeWithGeneric);
						player.hand.add(requestedType);
					}
					else
						throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");

				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("Bottom");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::UpperLeft])
			{
				if (tile->hex[Hex::Corner::UpperLeft]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Lumber)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::Upper])
			{
				if (tile->hex[Hex::Corner::Upper]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Lumber)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("BottomUpperLeft");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::UpperRight])
			{
				if (tile->hex[Hex::Corner::UpperRight]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Grain)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::BottomRight])
			{
				if (tile->hex[Hex::Corner::BottomRight]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Grain)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}

		if (auto[kRow, kColumn] = harborPosition.at("UpperBottomLeft");
			row == kRow && column == kColumn)
		{
			if (tile->hex[Hex::Corner::UpperRight])
			{
				if (tile->hex[Hex::Corner::UpperRight]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Ore)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");

			if (tile->hex[Hex::Corner::BottomRight])
			{
				if (tile->hex[Hex::Corner::BottomRight]->getColor() == player.getColor())
				{
					if (givenType == Resource::Type::Ore)
					{
						if (player.hand.numberOfCards(givenType) >= kNumberOfResourcesToTradeWithSpecific)
						{
							player.hand.discard(givenType, kNumberOfResourcesToTradeWithSpecific);
							player.hand.add(requestedType);
						}
						else
							throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
					}
					else
						throw Exceptions::ResourceNotAvailableException("Invalid given type for that harbor!");
				}
				else
					throw Exceptions::NoNeighbourPieceFoundException("It's not your settlement there!");
			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You don't have a settlement there!");
		}
	}
	else
		throw Exceptions::TileNotAccessibleException("That is not a valid position!");
}

void Game::diceInputWasSeven(HumanPlayer& currentPlayer, uint8_t row, uint8_t column)
{
	std::for_each(std::begin(*players), std::end(*players),
		[](std::shared_ptr<HumanPlayer> player)
	{
		static constexpr uint8_t kMinimumNumberOfCardsNeededToDiscard = 7;
		
		if (const auto totalNumberOfCards = player->hand.totalNumberOfResourceCards();
			totalNumberOfCards > kMinimumNumberOfCardsNeededToDiscard)
		{
			player->hand.discardHalf();
		}
	});

	thief->move(row, column, *board);
	auto resourceType = thief->applySpecialEffect(*board, *players);

	if (resourceType != Resource::Type::NotAType)
		currentPlayer.hand.add(resourceType);
}

std::vector<HumanPlayer::EdgePosition> Game::getAvailableEdges()
{
	const auto& boardContainer = board->getBoardContainer();
	const auto& container = boardContainer.getContainer();

	std::vector<HumanPlayer::EdgePosition> availablePositions;

	for (std::size_t row = 0; row < container.size(); ++row)
	{
		for (std::size_t column = 0; column < container[row].size(); ++column)
		{
			if (container[row][column])
			{
				if (auto resource = container[row][column]->resource();
					resource.getType() == Resource::Type::NotAType || resource.getType() == Resource::Type::Water)
					continue;

				static constexpr uint8_t kNumberOfEdges = 6;
				for (uint8_t index = 0; index < kNumberOfEdges; ++index)
				{
					auto position = HumanPlayer::EdgePosition(row, column, static_cast<Hex::Edge>(index));

					if (container[row][column]->hex[static_cast<Hex::Edge>(index)])
						continue;

					if (m_currentPlayer->canPlaceRoad(*board, *container[row][column], position))
						availablePositions.push_back(std::move(position));
				}
			}
		}
	}

	return availablePositions;
}

std::vector<HumanPlayer::CornerPosition>  Game::getAvailableCornersSettlement(bool firstTurn)
{
	const auto& container = board->getBoardContainer().getContainer();

	std::vector<HumanPlayer::CornerPosition> availablePositions;

	for (std::size_t row = 0; row < container.size(); ++row)
	{
		for (std::size_t column = 0; column < container[row].size(); ++column)
		{
			if (container[row][column])
			{
				if (auto resource = container[row][column]->resource();
					resource.getType() == Resource::Type::NotAType || resource.getType() == Resource::Type::Water)
					continue;

				static constexpr uint8_t kNumberOfCorners = 6;
				for (uint8_t index = 0; index < kNumberOfCorners; ++index)
				{
					auto position = HumanPlayer::CornerPosition(row, column, static_cast<Hex::Corner>(index));

					if (container[row][column]->hex[static_cast<Hex::Corner>(index)])
						continue;


					if (m_currentPlayer->doAdjacentIntersectionsContainNoPieces(*board, *container[row][column], position))
					{
						if (!firstTurn)
						{
							if (m_currentPlayer->hasAdjacentRoad(*board, *container[row][column], position))
							{
								availablePositions.push_back(std::move(position));
							}
						}
						else
							availablePositions.push_back(std::move(position));
							
					}
				}
			}
		}
	}

	return availablePositions;
}

const std::unordered_map<std::shared_ptr<HumanPlayer>, std::vector<std::pair<Harbor::Type, uint8_t>>>& Game::getPlayerTradeRequirements() const noexcept
{
	return m_playerTradeRequirements;
}

std::vector<HumanPlayer::CornerPosition> Game::getAvailableCornersCity()
{
	const auto& container = board->getBoardContainer().getContainer();

	std::vector<HumanPlayer::CornerPosition> availablePositions;

	for (std::size_t row = 0; row < container.size(); ++row)
	{
		for (std::size_t column = 0; column < container[row].size(); ++column)
		{
			if (container[row][column])
			{
				if (auto resource = container[row][column]->resource();
					resource.getType() == Resource::Type::NotAType || resource.getType() == Resource::Type::Water)
					continue;

				static constexpr uint8_t kNumberOfCorners = 6;
				for (uint8_t index = 0; index < kNumberOfCorners; ++index)
				{
					auto position = HumanPlayer::CornerPosition(row, column, static_cast<Hex::Corner>(index));

					if (auto hex = container[row][column].value().hex[std::get<Hex::Corner>(position)]; 
						instanceof<Settlement>(hex))
					{
						if(hex->getColor() == m_currentPlayer->getColor())
						{
							availablePositions.push_back(std::move(position));
						}
					}
				}
			}
		}
	}

	return availablePositions;
}

constexpr uint16_t Game::getTurnNumber() const noexcept
{
	return m_turnNumber;
}

bool Game::hasWinner() const noexcept
{
	return m_hasWinner;
}

const std::shared_ptr<HumanPlayer>& Game::getWinner() const noexcept
{
	return m_winner;
}
