
#ifndef CATAN_CORE_EXPORTS_H
#define CATAN_CORE_EXPORTS_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define CATAN_CORE_EXPORTS
#  define CATAN_CORE_NO_EXPORT
#else
#  ifndef CATAN_CORE_EXPORTS
#    ifdef Catan_Core_EXPORTS
        /* We are building this library */
#      define CATAN_CORE_EXPORTS __declspec(dllexport)
#    else
        /* We are using this library */
#      define CATAN_CORE_EXPORTS __declspec(dllimport)
#    endif
#  endif

#  ifndef CATAN_CORE_NO_EXPORT
#    define CATAN_CORE_NO_EXPORT 
#  endif
#endif

#ifndef CATAN_CORE_DEPRECATED
#  define CATAN_CORE_DEPRECATED __declspec(deprecated)
#endif

#ifndef CATAN_CORE_DEPRECATED_EXPORT
#  define CATAN_CORE_DEPRECATED_EXPORT CATAN_CORE_EXPORTS CATAN_CORE_DEPRECATED
#endif

#ifndef CATAN_CORE_DEPRECATED_NO_EXPORT
#  define CATAN_CORE_DEPRECATED_NO_EXPORT CATAN_CORE_NO_EXPORT CATAN_CORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef CATAN_CORE_NO_DEPRECATED
#    define CATAN_CORE_NO_DEPRECATED
#  endif
#endif

#endif /* CATAN_CORE_EXPORTS_H */
