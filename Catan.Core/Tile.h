#pragma once


#include "catan_core_exports.h"

#include <variant>

#include <Catan.Core/Hex.h>
#include <Catan.Core/Harbor.h>
#include <Catan.Core/Resource.h>

class CATAN_CORE_EXPORTS Tile
{
public:
	explicit constexpr Tile(Resource resource) noexcept;
	
	explicit constexpr Tile(Harbor harbor) noexcept;

	Hex hex;

	const Resource resource() const;

	const Harbor harbor() const;

private:
	std::variant<Resource, Harbor> m_tileType;
};
