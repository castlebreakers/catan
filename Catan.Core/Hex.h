#pragma once

#include <unordered_map>

#include <Catan.Core/Road.h>
#include <Catan.Core/Piece.h>
#include <Catan.Core/Settlement.h>
#include <Catan.Core/City.h>

#include "catan_core_exports.h"

#include <memory>

class CATAN_CORE_EXPORTS Hex
{
public:
	enum class Edge : uint8_t
	{
		UpperLeft,
		UpperRight,
		Right,
		BottomRight,
		BottomLeft,
		Left
	};

	enum class Corner : uint8_t
	{
		Upper,
		UpperRight,
		BottomRight,
		Bottom,
		BottomLeft,
		UpperLeft,

		NotCorner
	};

	using CornerIterator = std::unordered_map<Corner, std::shared_ptr<Piece>>::const_iterator;
	using EdgeIterator = std::unordered_map<Edge, std::shared_ptr<Road>>::const_iterator;

	struct CATAN_CORE_EXPORTS Data
	{
		struct CATAN_CORE_EXPORTS Corners
		{
			using AdjacentTilesPair = std::pair<std::pair<int8_t, int8_t>, std::pair<int8_t, int8_t>>;

			static const std::unordered_map<Hex::Corner, AdjacentTilesPair> adjacentTiles;

			static const std::unordered_map<Hex::Corner, AdjacentTilesPair> adjacentTiles_;

			using CornersPair = std::pair<Hex::Corner, Hex::Corner>;

			static const std::unordered_map<Hex::Corner, CornersPair> adjacentTileCorners;

			static const std::unordered_map<Hex::Corner, CornersPair> currentTileCorners;

			static const std::unordered_map<Hex::Corner, std::pair<Hex::Edge, Hex::Edge>> currentTileEdges;

			static const std::unordered_map<Hex::Corner, std::pair<Hex::Edge, Hex::Edge>> adjacentTileEdges;
			
			static const std::unordered_map<Hex::Corner, std::string> cornerToString;
		};

		struct CATAN_CORE_EXPORTS Edges
		{
			static const std::unordered_map<Hex::Edge, Hex::Edge> analogueEdge;

			static const std::unordered_map<Hex::Edge, std::pair<int8_t, int8_t>> adjacentTile;

			using AdjacentRoadsAndPieces =
				std::pair<std::pair<Hex::Edge, Hex::Edge>, std::pair<Hex::Corner, Hex::Corner>>;

			static const std::unordered_map<Hex::Edge, AdjacentRoadsAndPieces> currentHexAdjacentEdgesAndPieces;

			static const std::unordered_map<Hex::Edge, std::pair<Hex::Edge, Hex::Edge>> adjacentTileEdges;

			static const std::unordered_map<Hex::Edge, std::string> edgeToString;
		};
	};


public:
	const std::shared_ptr<Road> operator [](Edge edge) const noexcept;

	const std::shared_ptr<Piece> operator[](Corner corner) const noexcept;

	void addEdgeRoad(Edge edge, std::shared_ptr<Road> road);

	void addCornerPiece(Corner corner, std::shared_ptr<Settlement> settlement);

	std::shared_ptr<Piece> addCornerPiece(Corner corner, std::shared_ptr<City> city);

	std::pair<CornerIterator, CornerIterator> getCornerPieces() const noexcept;

	std::pair<EdgeIterator, EdgeIterator> getEdgePieces() const noexcept;

	bool operator==(const Hex& other) const noexcept;

private:
	std::unordered_map<Edge, std::shared_ptr<Road>> m_edgeRoads;

	std::unordered_map<Corner, std::shared_ptr<Piece>> m_cornerPieces;
};
