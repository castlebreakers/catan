#include "Board.h"

#include <Catan.Core/Tile.h>

Board::Board() :
	m_container()
{}

std::optional<Tile>& Board::getTile(uint8_t row, uint8_t column)
{
	return m_container.getTile(row, column);
}

const std::optional<Tile>& Board::getTile(uint8_t row, uint8_t column) const
{
	return m_container.getTile(row, column);
}

const BoardContainer & Board::getBoardContainer() const noexcept
{
	return m_container;
}

const std::vector<std::reference_wrapper<Tile>> Board::getTiles(uint16_t number) noexcept
{
	return m_container.getTilesByNumber(number);
}

const std::vector<std::reference_wrapper<Tile>> Board::getTiles() noexcept
{
	return m_container.getTilesByNumber();
}

BoardContainer::pair Board::getIndices(const Tile & tile) const noexcept
{
	return m_container.getIndices(tile);
}

BoardContainer::pair Board::getDesertTile() const noexcept
{
	return m_container.getDesertTile();
}
