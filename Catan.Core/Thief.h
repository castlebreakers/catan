#pragma once
#include <unordered_set>

#include "catan_core_exports.h"
#include <Catan.Core/Tile.h>
#include <Catan.Core/Board.h>
#include <Catan.Core/BoardContainer.h>

class HumanPlayer;

class  CATAN_CORE_EXPORTS Thief
{
public:
	Thief(Board& gameBoard);

	void move(uint8_t newRow, uint8_t newColumn, Board& board, uint8_t lowerBound = kLowerBound, uint8_t upperBound = kUpperBound);

	constexpr uint8_t getRow() const noexcept;

	constexpr uint8_t getColumn() const noexcept;

	Resource::Type applySpecialEffect(Board& board, std::vector<std::shared_ptr<HumanPlayer>>& players);

	static void getPlayersWithAdjacentPiecesToTile(Tile & tile, std::unordered_set<Piece::Color>& playersColor);

private:
	uint8_t m_row;
	uint8_t m_column;

	static constexpr uint8_t kLowerBound = 1;
	static constexpr uint8_t kUpperBound = 5;
};

