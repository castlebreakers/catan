#pragma once

#include <cstdint>
#include "catan_core_exports.h"

class CATAN_CORE_EXPORTS Piece
{
public:
	enum class Color : uint8_t
	{
		Blue, 
		White, 
		Red,
		Orange
	};

public:
	Piece() = default;

	explicit constexpr Piece(Color color) noexcept;

	virtual ~Piece() = default;

	constexpr Color getColor() const noexcept;
	virtual uint8_t getNumberOfPoints() const noexcept = 0;

protected:
	Color m_color;
};

