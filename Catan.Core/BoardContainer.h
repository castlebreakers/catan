#pragma once

#include <vector>
#include <optional>
#include <array>
#include <unordered_map>
#include "catan_core_exports.h"

#include <Catan.Core/Tile.h>

class Thief;

class CATAN_CORE_EXPORTS BoardContainer
{
	friend class Thief;
public:
	static constexpr uint8_t kBoardSize = 7;

	using pair = std::pair<uint8_t, uint8_t>;

	struct CATAN_CORE_EXPORTS Hash
	{
		std::size_t operator()(const pair& indexes) const noexcept
		{
			return std::hash<uint8_t>{}(indexes.first) + std::hash<uint8_t>{}(indexes.second);
		}
	};

	struct CATAN_CORE_EXPORTS EqualTo
	{
		constexpr bool operator()(const pair& lhs, const pair& rhs) const noexcept
		{
			return lhs.first == rhs.first && lhs.second == rhs.second;
		}
	};

	static constexpr auto kIteratorOffsets = std::array
	{
		pair{3, 6},
		pair{2, 6},
		pair{1, 6},
		pair{1, 5},
		pair{1, 4}
	};

	static constexpr auto kHarborPositions = std::array
	{
		pair{0, 3},
		pair{0, 5},
		pair{1, 6},
		pair{2 ,1},
		pair{3, 6},
		pair{4, 0},
		pair{5, 4},
		pair{6, 0},
		pair{6, 2}
	};

	static constexpr auto kWaterPositions = std::array
	{
		pair{0, 4},
		pair{0, 6},
		pair{1, 2},
		pair{2, 6},
		pair{3, 0},
		pair{4, 5},
		pair{5, 0},
		pair{6, 1},
		pair{6, 3}
	};

	static const std::unordered_map<pair, Hex::Edge, Hash, EqualTo> harborPositionToEdge;

public:
	BoardContainer();

	std::optional<Tile>& getTile(uint8_t row, uint8_t column);
	
	const std::optional<Tile>& getTile(uint8_t row, uint8_t column) const;

	pair getIndices(const Tile& tile) const noexcept;

	const std::vector <std::vector<std::optional<Tile>>>& getContainer() const noexcept;

	/**
	*	If it's the first turn , then we assume that the dice input numbers were sum to 754 ( a santinel value ). 
	*   \param number dice input
	*/
	const std::vector<std::reference_wrapper<Tile>> getTilesByNumber(uint16_t number = kDefaultNumberOfResources) noexcept;

	const BoardContainer::pair getDesertTile() const noexcept; 

private:
	void prepare();

	void populate();

	static constexpr uint8_t kDefaultNumberOfResources = 754;

	std::vector<std::vector<std::optional<Tile>>> m_container;
};

