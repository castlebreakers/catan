#pragma once

#include "Piece.h"

class CATAN_CORE_EXPORTS City : public Piece
{
public:
	explicit constexpr City(Piece::Color color) noexcept;

	uint8_t getNumberOfPoints() const noexcept override;

private:
	static constexpr uint8_t kNumberOfPoints = 2; //number of points associated with a city
};

