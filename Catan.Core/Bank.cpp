#include "Bank.h"

#include <algorithm>

#include <Catan.Utils/Random.hpp>
#include <Catan.Core/Exceptions.h>

//To be implemented

//14 Soldiers 
//5 Victory Points
//2 Road Building Cards
//2 Monopoly Cards
//2 Year of Plenty Cards

Bank::Bank()
{
	populate();
}

void Bank::populate() noexcept
{
	static constexpr uint8_t kSoldierNumber = 14;
	static constexpr uint8_t kVictoryPointsNumber = 5;
	static constexpr uint8_t kRoadMonopolyYearOfPlentyNumber = 2;

	for (auto index = 0 ; index < kSoldierNumber ;++index)
	{
		m_developmentCards.emplace_back(DevelopmentCard::Type::Soldier);

		if (index < kVictoryPointsNumber)
			m_developmentCards.emplace_back(DevelopmentCard::Type::VictoryPoint);

		if (index < kRoadMonopolyYearOfPlentyNumber)
		{
			m_developmentCards.emplace_back(DevelopmentCard::Type::Monopoly);
			m_developmentCards.emplace_back(DevelopmentCard::Type::RoadBuilding);
			m_developmentCards.emplace_back(DevelopmentCard::Type::YearOfPlenty);
		}
	}

	static Random<> random;
	std::shuffle(m_developmentCards.begin(), m_developmentCards.end(), random.getGenerator());
}

DevelopmentCard Bank::drawCard() 
{
	if (m_developmentCards.empty())
		throw Exceptions::BankOutOfCardsException("The bank has no remaining development cards.");

	auto card = m_developmentCards.back();
	m_developmentCards.pop_back();

	return card;
}

uint8_t Bank::remainingDevelopmentCards() const noexcept
{
	return static_cast<uint8_t>(m_developmentCards.size());
}
