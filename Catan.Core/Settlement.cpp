#include "Settlement.h"

constexpr Settlement::Settlement(Piece::Color color) noexcept :
	Piece(color)
{}

 uint8_t Settlement::getNumberOfPoints() const noexcept
{
	 return kNumberOfPoints;
}
