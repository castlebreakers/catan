#pragma once

#include "catan_core_exports.h"

#include <Catan.Core/Bank.h>
#include <Catan.Core/Board.h>
#include <Catan.Core/HumanPlayer.h>
#include <Catan.Core/Thief.h>

#include <Catan.Core/IPlayerObserver.h>

#include <algorithm>

class CATAN_CORE_EXPORTS Game : public IPlayerObserver, public std::enable_shared_from_this<Game>
{
public:
	using RequirementsIterator = std::unordered_map<std::shared_ptr<HumanPlayer>, std::pair<Harbor::Type, uint8_t>>::const_iterator;

public:
	/**
	* Factory method that creates a shared_ptr instance of the game. 
	* This would call the constructor that takes the same parameters 
	* and ensures that the object is properly created.
	* \param first Iterator to the beginning of the data structure currently holding the player objects.
	* \param last Iterator pointing to the end of the data structure currently holding the player objects.
	*/
	template <typename InputIterator>
	static std::shared_ptr<Game> create(InputIterator first, InputIterator last);

	const std::shared_ptr<Board> board;

	const std::shared_ptr<Thief> thief;

	const std::shared_ptr<Bank> bank;

	const std::shared_ptr<HumanPlayer> getCurrentPlayer() const noexcept;

	void nextPlayer();

	void onDevelopmentCardPlay(const std::shared_ptr<HumanPlayer>& player, DevelopmentCard::Type type) override;

	void onNumberOfPointsChange(const std::shared_ptr<HumanPlayer>& player, uint8_t numberOfPoints) override;
	
	void onPiecePlace(const std::shared_ptr<HumanPlayer>& player, HumanPlayer::CornerPosition position) override;

	const std::shared_ptr<std::vector<std::shared_ptr<HumanPlayer>>> players;
						
	void placeSettlementFirstTurn(HumanPlayer::CornerPosition settlementPosition);

	void placeRoadFirstTurn(HumanPlayer::EdgePosition roadPosition);

	void receiveResource(HumanPlayer::DiceRoll roll);

	void receiveResource(HumanPlayer::CornerPosition corner);

	void tradeResourcesWithHarbor(std::pair<uint8_t, uint8_t> position, Resource::Type givenType, Resource::Type requestedType, HumanPlayer& player);

	void diceInputWasSeven(HumanPlayer& player, uint8_t row, uint8_t column);

	std::vector<HumanPlayer::EdgePosition> getAvailableEdges();

	std::vector<HumanPlayer::CornerPosition> getAvailableCornersSettlement(bool firstTurn = false);

	std::vector<HumanPlayer::CornerPosition> getAvailableCornersCity();

	const std::unordered_map<std::shared_ptr<HumanPlayer>, std::vector<std::pair<Harbor::Type, uint8_t>>>& getPlayerTradeRequirements() const noexcept;

	constexpr uint16_t getTurnNumber() const noexcept;

	bool hasWinner() const noexcept;

	const std::shared_ptr<HumanPlayer>& getWinner() const noexcept;

protected:
	template <typename InputIterator>
	Game(InputIterator first, InputIterator last);
private:
	uint16_t m_turnNumber = 1;

	const HumanPlayer initialFirst;

	const HumanPlayer initialLast;

	std::unordered_map<std::shared_ptr<HumanPlayer>, uint16_t> m_soldierCardPoints;

	std::unordered_map<std::shared_ptr<HumanPlayer>, std::vector<Harbor::Type>> m_playerHarborOpening;

	std::unordered_map<std::shared_ptr<HumanPlayer>, std::vector<std::pair<Harbor::Type, uint8_t>>> m_playerTradeRequirements;

	bool m_hasWinner = false;

	std::shared_ptr<HumanPlayer> m_winner = nullptr;

	std::shared_ptr<HumanPlayer> m_currentPlayer;
};

template<typename InputIterator>
inline std::shared_ptr<Game> Game::create(InputIterator first, InputIterator last)
{
	auto& game = std::shared_ptr<Game>(new Game(first, last));

	std::for_each(std::begin(*game->players), std::end(*game->players),
		[&game](std::shared_ptr<HumanPlayer> player)
	{
		player->addObserver(game);
	});

	return game;
}

template<typename InputIterator>
inline Game::Game(InputIterator first, InputIterator last) :
	players(std::make_shared<std::vector<std::shared_ptr<HumanPlayer>>>(first, last)),
	board(std::make_shared<Board>()),
	thief(std::make_shared<Thief>(*board)),
	bank(std::make_shared<Bank>()),
	initialFirst(**first),
	initialLast(**std::prev(last))
{
	m_currentPlayer = *first;

	for (auto& player : *players)
	{
		static constexpr uint8_t kNumberOfHarborTypes = 6;
		static constexpr uint8_t kBankNumberOfResourcesRequirement = 4;

		for (uint8_t index = 0; index < kNumberOfHarborTypes; ++index)
			m_playerTradeRequirements[player].push_back(std::make_pair(static_cast<Harbor::Type>(index), kBankNumberOfResourcesRequirement));
	}
	/*std::for_each(std::begin(*players), std::end(*players), [this](const std::shared_ptr<HumanPlayer>& player)
	{
		static constexpr uint8_t kNumberOfHarborTypes = 6;
		static constexpr uint8_t kBankNumberOfResourcesRequirement = 4;

		for(uint8_t index = 0; index < kNumberOfHarborTypes; ++index)
			this->m_
	}*/
}
