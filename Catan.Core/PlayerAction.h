#pragma once


#include <cstdint>

#include "catan_core_exports.h"

enum class CATAN_CORE_EXPORTS PlayerActionType : uint8_t
{
	PlaceRoad,
	PlaceSettlement,
	PlaceCity,
	PlayDevelopmentCard,
	BuyDevelopmentCard,
	TradeWithBank,

	NotAnAction
};
