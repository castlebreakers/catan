#include "Thief.h"

#include <algorithm>

#include <Catan.Core/Exceptions.h>
#include <Catan.Utils/Random.hpp>
#include <Catan.Core/HumanPlayer.h>


Thief::Thief(Board& gameBoard)
{
	const auto size = gameBoard.m_container.m_container.size();
	for (auto index = 0; index < size; ++index)
	{
			auto result = std::find_if(std::begin(gameBoard.m_container.m_container[index]), 
				std::end(gameBoard.m_container.m_container[index]),
				[](std::optional<Tile>& tile)
			{
				return tile ? tile->resource().getType() == Resource::Type::Nothing : false;
			});

		if (result != std::end(gameBoard.m_container.m_container[index]))
		{
			m_row = index;
			m_column = result - std::begin(gameBoard.m_container.m_container[index]);
			break;
		}
	}
}

void Thief::move(uint8_t newRow, uint8_t newColumn, Board& board, uint8_t lowerBound, uint8_t upperBound)
{
	if (newRow >= lowerBound && newRow <= upperBound)
	{
		if (newColumn >= lowerBound && newColumn <= upperBound)
		{
			m_row = newRow;
			m_column = newColumn;
		}
		else
			throw Exceptions::InvalidThiefPositionException("You can't put the Thief on a non-resource Tile.");
	}
	else 
		throw Exceptions::InvalidThiefPositionException("You can't put the Thief on a non-resource Tile.");
}

constexpr uint8_t Thief::getRow() const noexcept
{
	return m_row;
}

constexpr uint8_t Thief::getColumn() const noexcept
{
	return m_column;
}

Resource::Type Thief::applySpecialEffect(Board& board, std::vector<std::shared_ptr<HumanPlayer>>& players)
{
	auto& tile = board.getTile(m_row, m_column);
	std::unordered_set<Piece::Color> playersColor;

	getPlayersWithAdjacentPiecesToTile(*tile, playersColor);

	std::vector<std::shared_ptr<HumanPlayer>> adjacentPlayers;
	std::for_each(std::begin(players), std::end(players),
		[&adjacentPlayers, &playersColor](std::shared_ptr<HumanPlayer>& player)
	{
		if (auto it = playersColor.find(player->getColor()); it != std::end(playersColor))
			adjacentPlayers.push_back(player);
	});

	auto& thePlayerWithTheMostResources = std::max_element(std::begin(adjacentPlayers), std::end(adjacentPlayers),
		[](const std::shared_ptr<HumanPlayer>& first, const std::shared_ptr<HumanPlayer>& second)
	{
		return first->hand.totalNumberOfResourceCards() < second->hand.totalNumberOfResourceCards();
	});

	std::vector<uint8_t> resourceTypes;

	if (thePlayerWithTheMostResources == std::end(adjacentPlayers))
		return Resource::Type::NotAType;

	for (const auto&[type, numberOfCards] : (*thePlayerWithTheMostResources)->hand.getResources())
	{
		resourceTypes.push_back(static_cast<uint8_t>(type));
	}

	Random<> randomEngine;
	auto randomResourceType = randomEngine.getElement(resourceTypes);

	(*thePlayerWithTheMostResources)->hand.discard(static_cast<Resource::Type>(randomResourceType));

	return static_cast<Resource::Type>(randomResourceType);
}

void Thief::getPlayersWithAdjacentPiecesToTile(Tile & tile, std::unordered_set<Piece::Color>& playersColor)
{
	auto[firstCornerPiecesIt, lastCornerPiecesIt] = tile.hex.getCornerPieces();

	while (firstCornerPiecesIt != lastCornerPiecesIt)
	{
		auto corner = firstCornerPiecesIt->first;

		switch (corner)
		{
		case Hex::Corner::Upper:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		case Hex::Corner::UpperRight:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		case Hex::Corner::BottomRight:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		case Hex::Corner::Bottom:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		case Hex::Corner::BottomLeft:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		case Hex::Corner::UpperLeft:
		{
			if (tile.hex[corner])
				playersColor.insert(tile.hex[corner]->getColor());

			break;
		}
		default:
			break;
		}

		++firstCornerPiecesIt;
	}
}
