#include "BoardContainer.h"

#include <Catan.Core/TileGenerator.h>
#include <Catan.Core/Exceptions.h>

#include <array>
#include <algorithm>

BoardContainer::BoardContainer()
{
	prepare();
	populate();
}

std::optional<Tile>& BoardContainer::getTile(uint8_t row, uint8_t column)
{
	return m_container[row][column];
}

const std::optional<Tile>& BoardContainer::getTile(uint8_t row, uint8_t column) const
{
	return m_container[row][column];
}

BoardContainer::pair BoardContainer::getIndices(const Tile & tile) const noexcept
{
	for (std::size_t row = 0; row < m_container.size(); ++row)
	{
		for (std::size_t column = 0; column < m_container[row].size(); ++column)
		{
			if (!m_container[row][column])
				continue;

			if (m_container[row][column]->resource().getType() == Resource::Type::NotAType ||
				m_container[row][column]->resource().getType() == Resource::Type::Water)
				continue;

			if (m_container[row][column]->resource() == tile.resource() &&
				m_container[row][column]->hex == tile.hex)
				return pair(row, column);
		}
	}
	return pair();
}

const std::vector<std::vector<std::optional<Tile>>>& BoardContainer::getContainer() const noexcept
{
	return m_container;
}

const std::vector<std::reference_wrapper<Tile>> BoardContainer::getTilesByNumber(uint16_t number) noexcept
{
	std::vector<std::reference_wrapper<Tile>> tiles;

	for (uint8_t i = 0; i < m_container.size(); ++i)
	{
		for (uint8_t j = 0; j < m_container[i].size(); ++j)
		{
			if (m_container[i][j])
			{
				if (auto resource = m_container[i][j]->resource();
					(resource.getType() != Resource::Type::NotAType) && (resource.getType() != Resource::Type::Water))
				{
					if(number== kDefaultNumberOfResources)
						tiles.push_back(*m_container[i][j]);
					else if (resource.getTileNumber() == number)
						tiles.push_back(*m_container[i][j]);

				}
			}
		}
	}

	return tiles;
}

const BoardContainer::pair BoardContainer::getDesertTile() const noexcept
{
	for (uint8_t i = 0; i < m_container.size(); ++i)
	{
		for (uint8_t j = 0; j < m_container[i].size(); ++j)
		{
			if (m_container[i][j])
			{
				if (auto resource = m_container[i][j]->resource();
					(resource.getType() == Resource::Type::Nothing))
					return { i, j };
			}
		}
	}
}


void BoardContainer::prepare()
{
	m_container.resize(kBoardSize);

	std::for_each(std::begin(m_container), std::end(m_container),
		[size = kBoardSize](auto& column)
	{
		column.resize(size);
	});
}

void BoardContainer::populate()
{
	static constexpr uint8_t kNumberOfTilePopulatedRows = 6;
	static constexpr uint8_t kIteratorOffsetsOffset = 1;
	static constexpr uint8_t kIndexStopPopulatingSpecifier = 5;

	auto resources = std::move(TileGenerator::createResources());
	auto harbors = std::move(TileGenerator::createHarbors());
	auto waters = std::move(TileGenerator::createWaters());

	auto getHarbor = [&harbors]()
	{
		Harbor harbor = harbors.back();
		harbors.pop_back();
		return harbor;
	};

	auto getWater = [&waters]()
	{
		Resource waterResource = waters.back();
		waters.pop_back();
		return waterResource;
	};

	for (auto[row, column] : kWaterPositions)
		m_container[row][column] = Tile(getWater());

	for (auto[row, column] : kHarborPositions)
		m_container[row][column] = Tile(getHarbor());

	for (uint8_t index = 1; index < kNumberOfTilePopulatedRows; ++index)
	{
		std::generate(std::next(std::begin(m_container[index]), kIteratorOffsets[index - kIteratorOffsetsOffset].first),
			std::next(std::begin(m_container[index]), kIteratorOffsets[index - kIteratorOffsetsOffset].second),
			[&resources]()
		{
			Resource resource = resources.back();
			resources.pop_back();
			return Tile(resource);
		});
	}
}
const std::unordered_map<BoardContainer::pair, Hex::Edge, BoardContainer::Hash, BoardContainer::EqualTo> 
BoardContainer::harborPositionToEdge
{
	{{0,3}, Hex::Edge::BottomRight},
	{{0,5}, Hex::Edge::BottomLeft},
	{{1,6}, Hex::Edge::BottomLeft},
	{{3,6}, Hex::Edge::Left},
	{{5,4}, Hex::Edge::UpperLeft},
	{{6,2}, Hex::Edge::UpperLeft},
	{{6,0}, Hex::Edge::UpperRight},
	{{7,0}, Hex::Edge::Right},
	{{2,1}, Hex::Edge::Right}
};

