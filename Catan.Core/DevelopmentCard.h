#pragma once

#include "catan_core_exports.h"

#include <cstdint>

class CATAN_CORE_EXPORTS DevelopmentCard
{
public:
	enum class Type : uint8_t
	{
		Soldier, //move the robber; steal one resource from the owner of the addiacent settlement/city
		Monopoly, //get one type of resource from all other players
		RoadBuilding, //place two roads
		YearOfPlenty, //take any two resources from the bank
		VictoryPoint, //one extra point
		None
	};


public:
	DevelopmentCard();

	explicit constexpr DevelopmentCard(Type type) noexcept;

	constexpr Type getType() const noexcept;

	constexpr bool isApplied() const noexcept;

	void apply() noexcept;

private:
	Type m_type;

	bool m_isApplied;
};