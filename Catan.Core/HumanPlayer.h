#pragma once

#include <string>
#include <vector>
#include <cstdint>

#include "catan_core_exports.h"

#include <Catan.Core/City.h>
#include <Catan.Core/Settlement.h>
#include <Catan.Core/Road.h>
#include <Catan.Core/Hand.h>
#include <Catan.Core/Board.h>
#include <Catan.Core/Thief.h>
#include <Catan.Core/IPlayerObserver.h>
#include <Catan.Core/IPlayer.h>

#include <Catan.Utils/Observable.h>
#include <Catan.Utils/StringUtils.h>


class CATAN_CORE_EXPORTS HumanPlayer : public IPlayer, public Observable<IPlayerObserver>, public std::enable_shared_from_this<HumanPlayer>
{
public:
	using EdgePosition   = std::tuple<uint8_t, uint8_t, Hex::Edge  >;
	using CornerPosition = std::tuple<uint8_t, uint8_t, Hex::Corner>;

public:
	HumanPlayer(std::string_view name, Piece::Color color);

	DiceRoll requestDiceInput() override;

	bool requestBuildingDecisionInput() override;

	bool requestTradeDecisionInput() override;

	void placeRoad(Board& board, const EdgePosition& position , bool isFirstTurn = false);

	void placeSettlement(Board& board, const CornerPosition& position, bool isFirstTurn = false);

	void placeCity(Board& board, const CornerPosition& position);

	constexpr uint8_t getNumberOfPoints() const noexcept;

	constexpr Piece::Color getColor() const noexcept;

	constexpr const std::string& getName() const noexcept;

	bool operator == (const HumanPlayer& other) const noexcept;

	template <typename T>
	bool hasEnoughResources() const noexcept;

	bool hasAtLeastOneRoad() const noexcept;

	bool hasAtLeastOneSettlement() const noexcept;

	bool hasAtLeastOneCity() const noexcept;

	Hand hand;

	//Victory Point card
	void playDevelopmentCard();
	
	//YearOfPlenty card
	void playDevelopmentCard(Resource::Type firstResourceType, Resource::Type secondResourceType);

	//roadBuilding card
	void playDevelopmentCard(Board& board, EdgePosition firstPosition, EdgePosition secondPosition);

	//monopoly card
	void playDevelopmentCard(std::vector<std::shared_ptr<HumanPlayer>>& players, Resource::Type type);

	//soldierCard
	void playDevelopmentCard(uint8_t newRow, uint8_t newColumn, Board& board, Thief & thief, std::vector<std::shared_ptr<HumanPlayer>>& players);

	void buy(DevelopmentCard card);

	void tradeResourcesWithBank(Resource::Type givenType, Resource::Type requestedType);

	bool canPlaceRoad(Board& board, const Tile& tile, const EdgePosition& position) const;

	static bool doAdjacentIntersectionsContainNoPieces(const Board& board, const Tile& tile, CornerPosition position);

	static bool doAdjacentIntersectionsContainNoPieces(const Board& board, const Tile& tile, CornerPosition position, Resource::Type& firstResourceType, Resource::Type& secondResourceType);

	bool hasAdjacentRoad(const Board& board, const Tile& tile, CornerPosition position) const;

private:

	void notifyObservers(DevelopmentCard::Type cardType);

	void notifyObservers(uint8_t numberOfPoints);

	void notifyObservers(CornerPosition position);
	

	void markAdjacentRoadTiles(Board& board, std::shared_ptr<Road> roadToPlace, EdgePosition position) const;

	template <typename PieceType>
	void markAdjacentPieceTiles(Board& board, std::shared_ptr<PieceType> pieceToPlace, const CornerPosition& position) const;

	void markAdjacentSettlementTiles(Board& board, std::shared_ptr<Settlement> settlementToPlace, CornerPosition position) const;

	void markAdjacentCityTiles(Board& board, std::shared_ptr<City> cityToPlace, CornerPosition position) const;


	void checkIfTheRequestedCardExists(DevelopmentCard::Type type);

	template <typename NeighbourPositionType>
	bool hasNeighbour(const Tile& tile, NeighbourPositionType position) const;

	std::string m_name;

	Piece::Color m_color;

	uint8_t m_numberOfPoints = 0;

	std::vector<std::shared_ptr<Road>> m_roadPool;

	std::vector<std::shared_ptr<Settlement>> m_settlementPool;

	std::vector<std::shared_ptr<City>> m_cityPool;
};

namespace std
{
	template <> struct CATAN_CORE_EXPORTS hash<std::shared_ptr<HumanPlayer>>
	{
		std::size_t operator()(const std::shared_ptr<HumanPlayer>& player) const noexcept
		{
			return std::hash<uint8_t>{}(player->getNumberOfPoints()) + std::hash<std::string>{}(player->getName());
		}
	};
}

bool CATAN_CORE_EXPORTS operator == (const std::shared_ptr<HumanPlayer>& lhs, const std::shared_ptr<HumanPlayer>& rhs);

template <typename T>
bool HumanPlayer::hasEnoughResources() const noexcept
{
	static const std::regex extractRegex("class ([A-Za-z]+)");
	const std::string typeName = StringUtils::extractSingleRegex(typeid(T).name(), extractRegex);

	if(typeName == "Road")
	{
		static constexpr uint8_t kNumberOfResourceNeeded = 1;
		return (hand.numberOfCards(Resource::Type::Brick) >= kNumberOfResourceNeeded
			&& hand.numberOfCards(Resource::Type::Lumber) >= kNumberOfResourceNeeded);
	}
	else if (typeName == "Settlement")
	{
		static constexpr uint8_t kNumberOfResourceNeeded = 1;
		return (hand.numberOfCards(Resource::Type::Brick) >= kNumberOfResourceNeeded
			&& hand.numberOfCards(Resource::Type::Lumber) >= kNumberOfResourceNeeded
			&& hand.numberOfCards(Resource::Type::Wool) >= kNumberOfResourceNeeded
			&& hand.numberOfCards(Resource::Type::Grain) >= kNumberOfResourceNeeded);
	}
	else if (typeName == "City")
	{
		static constexpr uint8_t kNumberOfOreNeeded = 3;
		static constexpr uint8_t kNumberOfGrainNeeded = 2;

		return (hand.numberOfCards(Resource::Type::Grain) >= kNumberOfGrainNeeded
			&& hand.numberOfCards(Resource::Type::Ore) >= kNumberOfOreNeeded);
	}
	else if (typeName == "DevelopmentCard")
	{
		static constexpr uint8_t kNumberOfNeededResource = 1;

		return hand.numberOfCards(Resource::Type::Ore) >= kNumberOfNeededResource &&
			hand.numberOfCards(Resource::Type::Wool) >= kNumberOfNeededResource &&
			hand.numberOfCards(Resource::Type::Grain) >= kNumberOfNeededResource;
	}
	else
	{
		return false;
	}
}

template<typename PieceType>
inline void HumanPlayer::markAdjacentPieceTiles(Board & board, std::shared_ptr<PieceType> pieceToPlace, const CornerPosition & position) const
{
	const auto&[row, column, corner] = position;

	const auto&[firstTilePair, secondTilePair] = Hex::Data::Corners::adjacentTiles.at(corner);
	const auto&[firstCorner, secondCorner] = Hex::Data::Corners::adjacentTileCorners.at(corner);

	auto addCornerPieceFunction = [&](const auto& tilePair, Hex::Corner corner)
	{
		auto&[firstOffset, secondOffset] = tilePair;
		auto& tile = board.getTile(row + firstOffset, column + secondOffset);

		if (tile)
			tile->hex.addCornerPiece(corner, pieceToPlace);
	};

	addCornerPieceFunction(firstTilePair, firstCorner);
	addCornerPieceFunction(secondTilePair, secondCorner);
}

template<typename NeighbourPositionType>
inline bool HumanPlayer::hasNeighbour(const Tile & tile, NeighbourPositionType position) const
{
	const auto& neighbour = tile.hex[position];

	return neighbour && neighbour->getColor() == m_color;
}

