#include "catan_core_exports.h"

#include <vector>

#include <Catan.Core/Tile.h>
#include <Catan.Core/Harbor.h>

#include <Catan.Utils/Random.hpp>


class CATAN_CORE_EXPORTS TileGenerator
{
public:
	static 	std::vector<Resource> createResources()  noexcept;
	static  std::vector<Harbor> createHarbors()  noexcept;
	static  std::vector<Resource> createWaters() noexcept;

private:
	static uint8_t extractRandomElement(std::vector<uint8_t>& container, Random<>& random);
};