#include "Harbor.h"

constexpr Harbor::Harbor() noexcept : 
	m_type(Harbor::Type::NotAType)
{
}

constexpr Harbor::Harbor(Type type) noexcept :
	m_type(type)
{
}

constexpr Harbor::Type Harbor::getType() const noexcept
{
	return m_type;
}
