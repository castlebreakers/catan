#include "TileGenerator.h"

namespace
{
	static Random randomGeneratorNumber;
}


uint8_t TileGenerator::extractRandomElement(std::vector<uint8_t>& container, Random<>& random)
{
	static constexpr uint8_t kDefaultKey = 0;
	static constexpr uint8_t kSizeOffset = 1;

	auto elementIndex = random.getIntegerInRange<size_t>(kDefaultKey, container.size() - kSizeOffset);
	auto element = container[elementIndex];

	container.erase(container.begin() + elementIndex);

	return element;
}

std::vector<Resource> TileGenerator::createResources() noexcept
{
	constexpr uint8_t keyDefaultDesert = 0;
	std::vector<Resource> resources;
	std::vector<uint8_t> tileNumbers{ 2,3,3,4,4,5,5,6,6,8,8,9,9,10,10,11,11,12 };


	const uint8_t kNumberOfFieldForestPastureResources = 4;
	const uint8_t kNumberOfMountainHillResources = 3;

	for (auto index = 0; index < kNumberOfFieldForestPastureResources; ++index)
	{
		resources.emplace_back(Resource::Type::Grain, extractRandomElement(tileNumbers, randomGeneratorNumber));
		resources.emplace_back(Resource::Type::Lumber, extractRandomElement(tileNumbers, randomGeneratorNumber));
		resources.emplace_back(Resource::Type::Wool, extractRandomElement(tileNumbers, randomGeneratorNumber));

		if (index < kNumberOfMountainHillResources)
		{
			resources.emplace_back(Resource::Type::Ore, extractRandomElement(tileNumbers, randomGeneratorNumber));
			resources.emplace_back(Resource::Type::Brick, extractRandomElement(tileNumbers, randomGeneratorNumber));
		}

		if (index == keyDefaultDesert)
			resources.emplace_back(Resource::Type::Nothing, 0);
	}

	std::shuffle(resources.begin(), resources.end(), randomGeneratorNumber.getGenerator());

	return resources;
}

std::vector<Harbor> TileGenerator::createHarbors() noexcept
{
	std::vector<Harbor> harbors 
	{ 
		Harbor(Harbor::Type::Lumber), Harbor(Harbor::Type::Generic), Harbor(Harbor::Type::Brick),
		Harbor(Harbor::Type::Grain), Harbor(Harbor::Type::Generic), Harbor(Harbor::Type::Ore),
		Harbor(Harbor::Type::Generic), Harbor(Harbor::Type::Wool), Harbor(Harbor::Type::Generic)
	};

	return harbors;
}

std::vector<Resource> TileGenerator::createWaters() noexcept
{
	std::vector<Resource> waters;
	static constexpr uint8_t kDefaultNumberOfWaterFields = 9;

	waters.resize(kDefaultNumberOfWaterFields);

	static constexpr uint8_t kDefaultWaterNumber = 0;

	std::fill(std::begin(waters), std::end(waters), Resource(Resource::Type::Water, kDefaultWaterNumber));

	return waters;
}
