#include "Piece.h"

constexpr Piece::Piece(Color color) noexcept:
	m_color(color)
{
}

constexpr Piece::Color Piece::getColor() const noexcept
{
	return m_color;
}
