#pragma once

#include "Piece.h"

class CATAN_CORE_EXPORTS Road : public Piece
{
public:
	explicit constexpr Road(Piece::Color color) noexcept;

	uint8_t getNumberOfPoints() const noexcept override;

private:
	static constexpr uint8_t kNumberOfPoints = 0;
};

