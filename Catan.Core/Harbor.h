#pragma once

#include <cstdint>

#include "catan_core_exports.h"

class CATAN_CORE_EXPORTS Harbor
{
public:
	enum class Type : uint8_t
	{
		Brick,
		Wool,
		Ore,
		Grain,
		Lumber,
		Generic,
		NotAType
	};

public:
	constexpr Harbor() noexcept;

	explicit constexpr Harbor(Type type) noexcept;

	constexpr Type getType() const noexcept;

private:
	Type m_type;
};

