#include "City.h"

constexpr City::City(Piece::Color color) noexcept:
	Piece(color)
{}

uint8_t City::getNumberOfPoints() const noexcept
{
	return City::kNumberOfPoints;
}
