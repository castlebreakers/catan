#pragma once

#include <stdexcept>
#include <string_view>

#include "catan_core_exports.h"

#include <Catan.Core/DevelopmentCard.h>

namespace Exceptions
{
	struct CATAN_CORE_EXPORTS ResourceNotAvailableException : public std::logic_error
	{
		explicit ResourceNotAvailableException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS ResourceNotEnoughException : public std::logic_error
	{
		explicit ResourceNotEnoughException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS BankOutOfCardsException : public std::logic_error
	{
		explicit BankOutOfCardsException(std::string_view errorMessage) noexcept;
	};

	//
	struct CATAN_CORE_EXPORTS NoPieceLeftToPlaceException : public std::logic_error
	{
		explicit NoPieceLeftToPlaceException(std::string_view errorMesage) noexcept;
	};


	//

	//
	struct CATAN_CORE_EXPORTS  PieceAlreadyExistsException : public std::runtime_error
	{
		explicit PieceAlreadyExistsException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS RoadAlreadyExistsException : public PieceAlreadyExistsException
	{
		explicit RoadAlreadyExistsException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS SettlementAlreadyExistsException : public PieceAlreadyExistsException
	{
		explicit SettlementAlreadyExistsException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS CityAlreadyExistsException : public PieceAlreadyExistsException
	{
		explicit CityAlreadyExistsException(std::string_view errorMessage) noexcept;
	};
	//

	struct CATAN_CORE_EXPORTS TileNotAccessibleException : public std::invalid_argument
	{
		explicit TileNotAccessibleException(std::string_view errorMessage) noexcept;
	};


	struct CATAN_CORE_EXPORTS InvalidThiefPositionException : public std::domain_error
	{
		explicit InvalidThiefPositionException(std::string_view errorMessage) noexcept;
	};


	//optional: pass the desired position as paramter for more useful error messages
	struct CATAN_CORE_EXPORTS NoNeighbourPieceFoundException : public std::logic_error
	{
		explicit NoNeighbourPieceFoundException(std::string_view errorMessage) noexcept;
	};



	////
	struct CATAN_CORE_EXPORTS CouldNotPlacePieceException : public std::logic_error
	{
		explicit CouldNotPlacePieceException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS CouldNotPlaceSettlementException : public CouldNotPlacePieceException
	{
		explicit CouldNotPlaceSettlementException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS CouldNotPlaceCityException : public CouldNotPlacePieceException
	{
		explicit CouldNotPlaceCityException(std::string_view errorMessage) noexcept;
	};
	////


	////
	struct CATAN_CORE_EXPORTS AdjacentIntersectionsNotNullException : public CouldNotPlaceSettlementException
	{
		explicit AdjacentIntersectionsNotNullException(std::string_view errorMessage) noexcept;
	};

	struct CATAN_CORE_EXPORTS AdjacentRoadNotExistingException : public CouldNotPlaceSettlementException
	{
		explicit AdjacentRoadNotExistingException(std::string_view errorMessage) noexcept;
	};
	////

	struct CATAN_CORE_EXPORTS RequestedCardNotFoundException : public std::logic_error
	{
		explicit RequestedCardNotFoundException(std::string_view errorMessage, DevelopmentCard::Type requestedType) noexcept;

	private:
		std::string composeCustomMessage(DevelopmentCard::Type requestedType);
	};


}