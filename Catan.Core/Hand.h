#pragma once

#include <Catan.Core/DevelopmentCard.h>
#include <Catan.Core/Tile.h>

#include <unordered_map>
#include <unordered_set>
#include <memory>

#include "catan_core_exports.h"

class CATAN_CORE_EXPORTS Hand
{
public:
	Hand() = default;

	void add(Resource::Type type, uint16_t numberOfResources = kDefaultNumberOfResourcesToRecieve) noexcept;

	void discard(Resource::Type type, uint16_t numberOfResources = kDefaultNumberOfResourcesToRecieve);

	uint16_t numberOfCards(Resource::Type type) const;

	uint16_t totalNumberOfResourceCards() const noexcept;

	uint16_t totalNumberOfDevelopmentCards() const noexcept;

	uint16_t totalNumberOfUnusedDevelopmentCards() const noexcept;

	void add(DevelopmentCard developmentCard) noexcept;

	const std::unordered_map<Resource::Type, uint16_t>& getResources() const noexcept;

	std::vector<DevelopmentCard>& getDevelopmentCards() noexcept;

	DevelopmentCard& operator[](DevelopmentCard::Type type);

	void markAsApplied(DevelopmentCard& card);

	void markAsApplied(DevelopmentCard::Type type);

	void discardHalf();

private:

	void discardUntilCounterIsEqualToTheNeededQuantity(uint16_t requestedNumber);

	std::unordered_map<Resource::Type, uint16_t> m_resources;

	std::vector<DevelopmentCard> m_developmentCards;

	static constexpr uint8_t kDefaultNumberOfResourcesToRecieve = 1;
};
