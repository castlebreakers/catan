#include "Tile.h"

#include <utility>

constexpr Tile::Tile(Resource resource) noexcept :
	m_tileType(resource)
{
}

constexpr Tile::Tile(Harbor harbor) noexcept :
	m_tileType(harbor)
{
}

const Resource Tile::resource() const
{
	try
	{
		return std::get<Resource>(m_tileType);
	}
	catch (const std::bad_variant_access& e)
	{
		return Resource();
	}
}

const Harbor Tile::harbor() const
{
	try
	{
		return std::get<Harbor>(m_tileType);
	}
	catch (const std::bad_variant_access& e)
	{
		return Harbor();
	}
}
