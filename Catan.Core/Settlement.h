#pragma once

#include "Piece.h"

class CATAN_CORE_EXPORTS Settlement : public Piece
{
public:
	explicit constexpr Settlement(Piece::Color color) noexcept;

	uint8_t getNumberOfPoints() const noexcept override;

private:
	static constexpr uint8_t kNumberOfPoints = 1; //number of points assoiciated with a settlement
};

