#include "Exceptions.h"

#include <Catan.Core/HumanPlayer.h>

#include <unordered_map>

namespace
{
	using str = std::string_view;
	inline const auto developmentTypeToString = std::unordered_map<DevelopmentCard::Type, std::string_view>
	{
		{DevelopmentCard::Type::Monopoly, str{"Monopoly card."}},
		{DevelopmentCard::Type::RoadBuilding, str{"RoadBuilding card."}},
		{DevelopmentCard::Type::Soldier, str{"Soldier card."}},
		{DevelopmentCard::Type::VictoryPoint, str{"VictoryPoint card."}},
		{DevelopmentCard::Type::YearOfPlenty, str{"YearOfPlenty card."}},
	};
}

namespace Exceptions
{
	ResourceNotAvailableException::ResourceNotAvailableException(std::string_view errorMessage) noexcept :
		std::logic_error(errorMessage.data())
	{}

	ResourceNotEnoughException::ResourceNotEnoughException(std::string_view errorMessage) noexcept :
		std::logic_error(errorMessage.data())
	{}

	RoadAlreadyExistsException::RoadAlreadyExistsException(std::string_view errorMessage) noexcept :
		PieceAlreadyExistsException(errorMessage)
	{}

	PieceAlreadyExistsException::PieceAlreadyExistsException(std::string_view errorMessage) noexcept :
		std::runtime_error(errorMessage.data())
	{}

	SettlementAlreadyExistsException::SettlementAlreadyExistsException(std::string_view errorMessage) noexcept :
		PieceAlreadyExistsException(errorMessage.data())
	{}

	CityAlreadyExistsException::CityAlreadyExistsException(std::string_view errorMessage) noexcept :
		PieceAlreadyExistsException(errorMessage.data())
	{}
	
	InvalidThiefPositionException::InvalidThiefPositionException(std::string_view errorMessage) noexcept : 
		std::domain_error(errorMessage.data())
	{}

	NoNeighbourPieceFoundException::NoNeighbourPieceFoundException(std::string_view errorMessage) noexcept:
		std::logic_error(errorMessage.data())
	{}

	AdjacentIntersectionsNotNullException::AdjacentIntersectionsNotNullException(std::string_view errorMessage) noexcept :
		CouldNotPlaceSettlementException(errorMessage)
	{
	}

	CouldNotPlacePieceException::CouldNotPlacePieceException(std::string_view errorMessage) noexcept :
		std::logic_error(errorMessage.data())
	{
	}

	CouldNotPlaceSettlementException::CouldNotPlaceSettlementException(std::string_view errorMessage) noexcept :
		CouldNotPlacePieceException(errorMessage)
	{
	}

	AdjacentRoadNotExistingException::AdjacentRoadNotExistingException(std::string_view errorMessage) noexcept :
		CouldNotPlaceSettlementException(errorMessage)
	{
	}

	BankOutOfCardsException::BankOutOfCardsException(std::string_view errorMessage) noexcept : 
		std::logic_error(errorMessage.data())
	{
	}

	TileNotAccessibleException::TileNotAccessibleException(std::string_view errorMessage) noexcept :
		std::invalid_argument(errorMessage.data())
	{
	}

	CouldNotPlaceCityException::CouldNotPlaceCityException(std::string_view errorMessage) noexcept :
		CouldNotPlacePieceException(errorMessage)
	{
	}

	RequestedCardNotFoundException::RequestedCardNotFoundException(std::string_view errorMessage, DevelopmentCard::Type requestedType) noexcept :
		std::logic_error(errorMessage.data() + composeCustomMessage(requestedType))
	{}

	std::string RequestedCardNotFoundException::composeCustomMessage(DevelopmentCard::Type requestedType)
	{
		return developmentTypeToString.at(requestedType).data();
	}

	//

	//
	NoPieceLeftToPlaceException::NoPieceLeftToPlaceException(std::string_view errorMessage) noexcept :
		std::logic_error(errorMessage.data())
	{}

	//
}