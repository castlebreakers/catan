#include "HumanPlayer.h"

#include <Catan.Core/Exceptions.h>
#include <Catan.Utils/Random.hpp>
#include <Catan.Utils/polymorphics.hpp>

#include <iostream>
#include <algorithm>
#include <unordered_set>

static constexpr uint8_t kLine = 0;
static constexpr uint8_t kColumn = 1;
static constexpr uint8_t kEdgeCorner = 2;

HumanPlayer::HumanPlayer(std::string_view name, Piece::Color color) :
	m_name(name),
	m_color(color)
{
	static constexpr uint8_t kNumberOfRoads = 15;
	static constexpr uint8_t kNumberOfSettlements = 5;
	static constexpr uint8_t kNumberOfCities = 4;

	for (auto index = 0; index < kNumberOfRoads; ++index)
	{
		auto road = std::make_shared<Road>(color);

		m_roadPool.emplace_back(std::move(road));
	}

	for (auto index = 0; index < kNumberOfSettlements; ++index)
	{
		auto settlement = std::make_shared<Settlement>(color);

		m_settlementPool.emplace_back(std::move(settlement));

	}

	for (auto index = 0; index < kNumberOfCities; ++index)
	{
		auto city = std::make_shared<City>(color);

		m_cityPool.emplace_back(std::move(city));
	}
}

HumanPlayer::DiceRoll HumanPlayer::requestDiceInput()
{
	Random<> random;
	static constexpr size_t kLowerBound = 1;
	static constexpr size_t kUpperBound = 6;

	return HumanPlayer::DiceRoll(std::make_pair(
		random.getIntegerInRange<size_t>(kLowerBound, kUpperBound),
		random.getIntegerInRange<size_t>(kLowerBound, kUpperBound)));
}

bool HumanPlayer::requestBuildingDecisionInput()
{
	return false;
}

bool HumanPlayer::requestTradeDecisionInput()
{
	return false;
}

void HumanPlayer::placeRoad(Board& board, const EdgePosition& position , bool isFirstTurn)
{
	auto[linePosition, rowPosition, edge] = position;

	auto& tileToPlace = board.getTile(linePosition, rowPosition);

	if (!tileToPlace)
		throw Exceptions::TileNotAccessibleException("There is nothing there.");

	if (tileToPlace->resource().getType() == Resource::Type::Water)
		throw Exceptions::TileNotAccessibleException("You could drown there!");

	std::shared_ptr<Road> roadToPlace ;

	if (!m_roadPool.empty())
		roadToPlace = m_roadPool.back();
	else
		throw Exceptions::NoPieceLeftToPlaceException("You don't have any roads left to place !");

	if (isFirstTurn)
	{
		if (canPlaceRoad(board, *tileToPlace, position))
		{
			tileToPlace->hex.addEdgeRoad(std::get<kEdgeCorner>(position), roadToPlace);
			m_roadPool.pop_back();
		}
		else
			throw Exceptions::NoNeighbourPieceFoundException("You can't place a road there.");
	}
	else
	{
		auto hasEnoughRoadResources = [this]()
		{
			static constexpr uint8_t kNumberOfResourceNeeded = 1;
			return (hand.numberOfCards(Resource::Type::Brick) >= kNumberOfResourceNeeded
				&& hand.numberOfCards(Resource::Type::Lumber) >= kNumberOfResourceNeeded);
		};

		if (hasEnoughRoadResources())
		{
			if (canPlaceRoad(board, *tileToPlace, position))
			{
				hand.discard(Resource::Type::Brick);
				hand.discard(Resource::Type::Lumber);
				tileToPlace->hex.addEdgeRoad(std::get<kEdgeCorner>(position), roadToPlace);
				m_roadPool.pop_back();

			}
			else
				throw Exceptions::NoNeighbourPieceFoundException("You can't place a road there.");
		}
		else
			throw Exceptions::ResourceNotAvailableException("You don't have enough resources");
	}

	markAdjacentRoadTiles(board, roadToPlace, position);
}

void HumanPlayer::placeSettlement(Board& board, const CornerPosition& position, bool isFirstTurn)
{
	auto[linePosition, rowPosition, corner] = position;

	auto& tileToPlace = board.getTile(linePosition, rowPosition);

	if (!tileToPlace)
		throw Exceptions::TileNotAccessibleException("There is nothing there.");

	if (tileToPlace->resource().getType() == Resource::Type::Water)
		throw Exceptions::TileNotAccessibleException("You could drown there!");

	std::shared_ptr<Settlement> settlementToPlace;

	if (!m_settlementPool.empty())
		settlementToPlace = m_settlementPool.back();
	else
		throw Exceptions::NoPieceLeftToPlaceException("You don't have any settlement left to place !");

	if (isFirstTurn)
	{
		if (doAdjacentIntersectionsContainNoPieces(board, *tileToPlace, position))
		{
			tileToPlace->hex.addCornerPiece(std::get<kEdgeCorner>(position), settlementToPlace);
			m_numberOfPoints += settlementToPlace->getNumberOfPoints();

			notifyObservers(m_numberOfPoints);
			notifyObservers(position);

			m_settlementPool.pop_back();
		}
		else
			throw Exceptions::CouldNotPlacePieceException("At least one adjacent intersection already contains a piece.");

		markAdjacentSettlementTiles(board, settlementToPlace, position);
	}
	else
	{
		const auto hasEnoughRoadResources = [this]()
		{
			static constexpr uint8_t kNumberOfResourceNeeded = 1;
			return (hand.numberOfCards(Resource::Type::Brick) >= kNumberOfResourceNeeded
				&& hand.numberOfCards(Resource::Type::Lumber) >= kNumberOfResourceNeeded
				&& hand.numberOfCards(Resource::Type::Wool) >= kNumberOfResourceNeeded
				&& hand.numberOfCards(Resource::Type::Grain) >= kNumberOfResourceNeeded);
		};

		if (hasEnoughRoadResources())
		{
			if (doAdjacentIntersectionsContainNoPieces(board, *tileToPlace, position))
			{
				if (hasAdjacentRoad(board, *tileToPlace, position))
				{
					hand.discard(Resource::Type::Brick);
					hand.discard(Resource::Type::Lumber);
					hand.discard(Resource::Type::Wool);
					hand.discard(Resource::Type::Grain);

					tileToPlace->hex.addCornerPiece(std::get<kEdgeCorner>(position), settlementToPlace);
					m_numberOfPoints += settlementToPlace->getNumberOfPoints();

					notifyObservers(m_numberOfPoints);
					notifyObservers(position);

					m_settlementPool.pop_back();
				}
				else
					throw Exceptions::AdjacentRoadNotExistingException("You must connect the settlement to an existing road");
			}
			else
				throw Exceptions::CouldNotPlacePieceException("At least one adjacent intersection already contains a piece.");
		}
		else
			throw Exceptions::ResourceNotEnoughException("You don't have enough resources !");

		markAdjacentSettlementTiles(board, settlementToPlace, position);
	}
}

void HumanPlayer::placeCity(Board & board, const CornerPosition& position)
{
	auto[rowPosition, columnPosition, corner] = position;

	auto& tile = board.getTile(rowPosition, columnPosition);

	if(!tile)
		throw Exceptions::TileNotAccessibleException("There is nothing there.");

	if(tile->resource().getType() == Resource::Type::Water)
		throw Exceptions::TileNotAccessibleException("You could drown there!");

	std::shared_ptr<City> cityToPlace;

	if (!m_cityPool.empty())
		cityToPlace = m_cityPool.back();
	else
		throw Exceptions::NoPieceLeftToPlaceException("You don't have any city left to place !");

	auto settlementPicked = tile->hex.addCornerPiece(corner, cityToPlace);

	static constexpr uint8_t kNumberOfOreNeeded = 3;
	static constexpr uint8_t kNumberOfGrainNeeded = 2;
	                                                                                                                              
	auto hasEnoughRoadResources = [ore = kNumberOfOreNeeded, grain = kNumberOfGrainNeeded, this]()
	{
		return (hand.numberOfCards(Resource::Type::Grain) >= grain
			&& hand.numberOfCards(Resource::Type::Ore) >= ore);
	};

	if (hasEnoughRoadResources())
	{
		if (settlementPicked)
		{
			hand.discard(Resource::Type::Grain, kNumberOfGrainNeeded);
			hand.discard(Resource::Type::Ore, kNumberOfOreNeeded);

			m_numberOfPoints -= settlementPicked->getNumberOfPoints();
			m_numberOfPoints += m_cityPool.back()->getNumberOfPoints();

			notifyObservers(m_numberOfPoints);
			notifyObservers(position);

			m_cityPool.pop_back();
			m_settlementPool.emplace_back(std::dynamic_pointer_cast<Settlement>(settlementPicked));

			markAdjacentCityTiles(board, cityToPlace, position);
		}
		else
			throw Exceptions::CouldNotPlaceCityException("You need to have a settlement there.");
	}
	else
		throw Exceptions::ResourceNotEnoughException("You don't have enough resources!");
}

constexpr uint8_t HumanPlayer::getNumberOfPoints() const noexcept
{
	return m_numberOfPoints;
}

constexpr Piece::Color HumanPlayer::getColor() const noexcept
{
	return m_color;
}

constexpr const std::string & HumanPlayer::getName() const noexcept
{
	return m_name;
}

bool HumanPlayer::operator==(const HumanPlayer & other) const noexcept
{
	return m_name == other.getName() && m_color == other.getColor();
}

void HumanPlayer::playDevelopmentCard()
{
	static constexpr DevelopmentCard::Type requestedType = DevelopmentCard::Type::VictoryPoint;
	checkIfTheRequestedCardExists(requestedType);

	++m_numberOfPoints;

	notifyObservers(m_numberOfPoints);
	hand.markAsApplied(requestedType);
}

bool HumanPlayer::hasAtLeastOneRoad() const noexcept
{
	return !m_roadPool.size();
}

bool HumanPlayer::hasAtLeastOneSettlement() const noexcept
{
	return !m_settlementPool.empty();
}

bool HumanPlayer::hasAtLeastOneCity() const noexcept
{
	return !m_cityPool.empty();
}

void HumanPlayer::playDevelopmentCard(Resource::Type firstResourceType, Resource::Type secondResourceType)
{
	static constexpr DevelopmentCard::Type requestedType = DevelopmentCard::Type::YearOfPlenty;
	checkIfTheRequestedCardExists(requestedType);

	hand.add(firstResourceType);
	hand.add(secondResourceType);

	hand.markAsApplied(requestedType);
}

void HumanPlayer::playDevelopmentCard(Board & board, EdgePosition firstPosition, EdgePosition secondPosition)
{
	static constexpr DevelopmentCard::Type requestedType = DevelopmentCard::Type::RoadBuilding;
	checkIfTheRequestedCardExists(requestedType);

	//don't forget to catch the exceptions in the Game class
	placeRoad(board, firstPosition , true);
	placeRoad(board, secondPosition , true);

	hand.markAsApplied(requestedType);
}

void HumanPlayer::playDevelopmentCard(std::vector<std::shared_ptr<HumanPlayer>>& players, Resource::Type type)
{
	static constexpr DevelopmentCard::Type requestedType = DevelopmentCard::Type::Monopoly;
	checkIfTheRequestedCardExists(requestedType);

	for (auto& player : players)
	{
		if (const auto numberOfCards = player->hand.numberOfCards(type);
			numberOfCards)
		{
			hand.add(type, numberOfCards);
			player->hand.discard(type, numberOfCards);
		}
	}

	hand.markAsApplied(requestedType);
}

void HumanPlayer::playDevelopmentCard(uint8_t newRow, uint8_t newColumn, Board& board, Thief & thief, std::vector<std::shared_ptr<HumanPlayer>>& players)
{
	static constexpr DevelopmentCard::Type requestedType = DevelopmentCard::Type::Soldier;
	checkIfTheRequestedCardExists(requestedType);

	thief.move(newRow, newColumn, board);
	auto resourceType = thief.applySpecialEffect(board, players);

	this->hand.add(static_cast<Resource::Type>(resourceType));
	hand.markAsApplied(requestedType);

	notifyObservers(requestedType);
}

void HumanPlayer::buy(DevelopmentCard card)
{
	const auto hasEnoughResources = [this]()
	{
		static constexpr uint8_t kNumberOfNeededResource = 1;

		return hand.numberOfCards(Resource::Type::Ore) >= kNumberOfNeededResource &&
			hand.numberOfCards(Resource::Type::Wool) >= kNumberOfNeededResource &&
			hand.numberOfCards(Resource::Type::Grain) >= kNumberOfNeededResource;
	};

	if (hasEnoughResources())
	{
		hand.discard(Resource::Type::Ore);
		hand.discard(Resource::Type::Wool);
		hand.discard(Resource::Type::Grain);

		hand.add(card);
	}
	else
		throw Exceptions::ResourceNotEnoughException("You don't have enough resources!");
}

void HumanPlayer::tradeResourcesWithBank(Resource::Type givenType, Resource::Type requestedType)
{
	static constexpr uint8_t kMinimumNumberOfNeededResources = 4;

	if (hand.numberOfCards(givenType) >= kMinimumNumberOfNeededResources)
	{
		hand.discard(givenType, kMinimumNumberOfNeededResources);
		hand.add(requestedType);
	}
	else
		throw Exceptions::ResourceNotEnoughException("You don't have enough resources of that type!");
}

void HumanPlayer::notifyObservers(DevelopmentCard::Type cardType)
{
	std::for_each(std::begin(m_observers), std::end(m_observers),
		[cardType, this](std::shared_ptr<IPlayerObserver> observer)
	{
		observer->onDevelopmentCardPlay(this->shared_from_this(), cardType);
	});
}

void HumanPlayer::notifyObservers(uint8_t numberOfPoints)
{
	std::for_each(std::begin(m_observers), std::end(m_observers),
		[numberOfPoints, this](std::shared_ptr<IPlayerObserver> observer)
	{
		observer->onNumberOfPointsChange(this->shared_from_this(), numberOfPoints);
	});
}

void HumanPlayer::notifyObservers(CornerPosition position)
{
	std::for_each(std::begin(m_observers), std::end(m_observers),
		[position, this](std::shared_ptr<IPlayerObserver> observer)
	{
		observer->onPiecePlace(this->shared_from_this(), position);
	});
}

bool HumanPlayer::canPlaceRoad(Board & board, const Tile& tile, const EdgePosition& position) const
{
	auto[row, column, edge] = position;

	const auto&[neighbourEdges, neighbourCorners] = Hex::Data::Edges::currentHexAdjacentEdgesAndPieces.at(edge);
	const auto&[firstEdge, secondEdge] = neighbourEdges;
	const auto&[firstCorner, secondCorner] = neighbourCorners;

	if (hasNeighbour(tile, firstEdge))
		return true;

	if (hasNeighbour(tile, secondEdge))
		return true;

	if(hasNeighbour(tile, firstCorner))
		return true;

	if(hasNeighbour(tile, secondCorner))
		return true;

	const auto&[rowOffset, columnOffset] = Hex::Data::Edges::adjacentTile.at(edge);

	const auto& neighbourTile = board.getTile(row + rowOffset, column + columnOffset);

	if (neighbourTile)
	{
		const auto&[edge1, edge2] = Hex::Data::Edges::adjacentTileEdges.at(edge);

		if (hasNeighbour(*neighbourTile, edge1))
			return true;

		if (hasNeighbour(*neighbourTile, edge2))
			return true;
	}

	return false;
}

bool HumanPlayer::doAdjacentIntersectionsContainNoPieces(const Board & board, const Tile & tile, CornerPosition position)
{
	static Resource::Type firstResourceType;
	static Resource::Type secondResourceType;
	return doAdjacentIntersectionsContainNoPieces(board, tile, position, firstResourceType, secondResourceType);
}

bool HumanPlayer::doAdjacentIntersectionsContainNoPieces(const Board& board, const Tile& tile, CornerPosition position,
	Resource::Type& firstResourceType, Resource::Type& secondResourceType)
{
	const auto&[row, column, corner] = position;

	if (tile.hex[corner])
		return false;

	const auto&[firstCorner, secondCorner] = Hex::Data::Corners::currentTileCorners.at(corner);

	if (tile.hex[firstCorner])
		return false;

	if (tile.hex[secondCorner])
		return false;

	auto checkNeighboursFunction = [&](const auto& firstOffsets, const auto& secondOffsets, Hex::Corner firstCorner, Hex::Corner secondCorner, Resource::Type& first, Resource::Type& second)
	{
		const auto& firstNeighbour = board.getTile(row + firstOffsets.first, column + firstOffsets.second);
		if (firstNeighbour)
		{
			if (firstNeighbour->hex[firstCorner])
				return false;
		}

		const auto& secondNeighbour = board.getTile(row + secondOffsets.first, column + secondOffsets.second);
		if (secondNeighbour)
		{
			if (secondNeighbour->hex[secondCorner])
				return false;
		}


		first = firstNeighbour->resource().getType();
		second = secondNeighbour->resource().getType();

		return true;
	};

	const auto&[firstNeighbourOffsets, secondNeighbourOffsets] = Hex::Data::Corners::adjacentTiles.at(corner);

	return checkNeighboursFunction(firstNeighbourOffsets, secondNeighbourOffsets, firstCorner, secondCorner, firstResourceType, secondResourceType);
}

void HumanPlayer::markAdjacentRoadTiles(Board & board, std::shared_ptr<Road> roadToPlace, EdgePosition position) const
{
	const auto&[row, column, edge] = position;
	const auto&[rowOffset, columnOffset] = Hex::Data::Edges::adjacentTile.at(edge);

	auto& neighbourTile = board.getTile(row + rowOffset, column + columnOffset);

	if (neighbourTile)
		neighbourTile->hex.addEdgeRoad(Hex::Data::Edges::analogueEdge.at(edge), roadToPlace);
}

void HumanPlayer::markAdjacentSettlementTiles(Board & board, std::shared_ptr<Settlement> settlementToPlace, CornerPosition position) const
{
	markAdjacentPieceTiles(board, settlementToPlace, position);
}

void HumanPlayer::markAdjacentCityTiles(Board & board, std::shared_ptr<City> cityToPlace, CornerPosition position) const
{
	markAdjacentPieceTiles(board, cityToPlace, position);
}

bool HumanPlayer::hasAdjacentRoad(const Board & board, const Tile & tile, CornerPosition position) const
{
	auto[row, column, corner] = position;

	const auto&[firstEdge, secondEdge] = Hex::Data::Corners::currentTileEdges.at(corner);

	if (hasNeighbour(tile, firstEdge))
		return true;

	if (hasNeighbour(tile, secondEdge))
		return true;

	auto checkNeighbourFunction = [&board, row, column, *this](const auto& offsets, Hex::Edge edge)
	{
		const auto&[rowOffset, columnOffset] = offsets;

		const auto& neighbourTile = board.getTile(row + rowOffset, column + columnOffset);
		
		if (neighbourTile)
		{
			const auto& road = neighbourTile->hex[edge];

			if (road)
				if (road->getColor() == m_color)
						return true;
		}

		return false;
	};

	const auto&[firstNeighbourOffsets, secondNeighbourOffsets] = Hex::Data::Corners::adjacentTiles_.at(corner);
	const auto&[firstNeighbourEdge, secondNeighbourEdge] = Hex::Data::Corners::adjacentTileEdges.at(corner);

	if (checkNeighbourFunction(firstNeighbourOffsets, firstNeighbourEdge))
		return true;

	return checkNeighbourFunction(secondNeighbourOffsets, secondNeighbourEdge);
}

void HumanPlayer::checkIfTheRequestedCardExists(DevelopmentCard::Type type)
{
	if (hand[type].getType() == DevelopmentCard::Type::None)
		throw Exceptions::RequestedCardNotFoundException("That card is not available. You wanted a ", type);
}

bool operator==(const std::shared_ptr<HumanPlayer>& lhs, const std::shared_ptr<HumanPlayer>& rhs)
{
	return lhs->getColor() == rhs->getColor() && lhs->getName() == rhs->getName();
}