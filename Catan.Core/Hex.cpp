#include "Hex.h"

#include <Catan.Core/Exceptions.h>
#include <Catan.Utils/polymorphics.hpp>

const std::shared_ptr<Road> Hex::operator[](Edge edge) const noexcept
{
	auto it = m_edgeRoads.find(edge);
	return it != m_edgeRoads.end() ? it->second : nullptr;
}

const std::shared_ptr<Piece> Hex::operator[](Corner corner) const noexcept
{
	auto it = m_cornerPieces.find(corner);
	return it != m_cornerPieces.end() ? it->second : nullptr;
}

void Hex::addEdgeRoad(Edge edge, std::shared_ptr<Road> road)
{
	if (m_edgeRoads.find(edge) == m_edgeRoads.end())
		m_edgeRoads[edge] = road;
	else
		throw Exceptions::RoadAlreadyExistsException("This edge already contains a road");
}

void Hex::addCornerPiece(Corner corner, std::shared_ptr<Settlement> settlement)
{
	if (m_cornerPieces.find(corner) == m_cornerPieces.end())
		m_cornerPieces[corner] = settlement;
	else
	{
		if (m_cornerPieces[corner])
			throw Exceptions::PieceAlreadyExistsException("There is already a piece over there.");
	}
}

std::shared_ptr<Piece> Hex::addCornerPiece(Corner corner, std::shared_ptr<City> city)
{
	//Inspection needed!
	//m_cornerPieces[corner] = city;
	if (instanceof<Settlement>(m_cornerPieces[corner]))
	{
		if (m_cornerPieces[corner]->getColor() == city->getColor())
		{
			auto settlement = m_cornerPieces[corner];
			m_cornerPieces[corner] = city;
			return settlement;
		}
		else
			throw Exceptions::PieceAlreadyExistsException("There is already a piece over there.");
	}
	else
		return nullptr;
}

std::pair<Hex::CornerIterator, Hex::CornerIterator> Hex::getCornerPieces() const noexcept
{
	return std::make_pair(std::cbegin(m_cornerPieces), std::cend(m_cornerPieces));
}

std::pair<Hex::EdgeIterator, Hex::EdgeIterator> Hex::getEdgePieces() const noexcept
{
	return std::make_pair(std::cbegin(m_edgeRoads), std::cend(m_edgeRoads));
}

bool Hex::operator==(const Hex & other) const noexcept
{
	return m_edgeRoads == other.m_edgeRoads && m_cornerPieces == other.m_cornerPieces;
}


const std::unordered_map<Hex::Edge, std::string> Hex::Data::Edges::edgeToString
{
	{ Hex::Edge::BottomLeft , "bottom_left"  },
	{ Hex::Edge::BottomRight, "bottom_right" },
	{ Hex::Edge::Left       , "left"         },
	{ Hex::Edge::Right      , "right"        },
	{ Hex::Edge::UpperLeft  , "upper_left"   },
	{ Hex::Edge::UpperRight , "upper_right"  },
};

const std::unordered_map<Hex::Corner, std::string> Hex::Data::Corners::cornerToString
{
	{ Hex::Corner::Bottom     , "bottom"       },
	{ Hex::Corner::BottomLeft , "bottom_left"  },
	{ Hex::Corner::BottomRight, "bottom_right" },
	{ Hex::Corner::Upper      , "upper"        },
	{ Hex::Corner::UpperLeft  , "upper_left"   },
	{ Hex::Corner::UpperRight , "upper_right"  },
};

using TilesPair = Hex::Data::Corners::AdjacentTilesPair;

const std::unordered_map<Hex::Corner, TilesPair> Hex::Data::Corners::adjacentTiles
{
	{ Hex::Corner::Bottom     , TilesPair({1,-1}, {1,0})	},
	{ Hex::Corner::BottomLeft , TilesPair({0, -1}, {1,-1})	},
	{ Hex::Corner::BottomRight, TilesPair({0,1},  {1,0})	},
	{ Hex::Corner::Upper      , TilesPair({-1,0}, {-1,1})	},
	{ Hex::Corner::UpperLeft  , TilesPair({-1,0}, {0,-1})	},
	{ Hex::Corner::UpperRight , TilesPair({-1,1}, {0,1})	},
};

const std::unordered_map<Hex::Corner, TilesPair> Hex::Data::Corners::adjacentTiles_
{
	{ Hex::Corner::Bottom     , TilesPair({1,-1}, {1,0})	},
	{ Hex::Corner::BottomLeft , TilesPair({0, -1}, {1,-1})	},
	{ Hex::Corner::BottomRight, TilesPair({0,1},  {1,0})	},
	{ Hex::Corner::Upper      , TilesPair({-1,0}, {-1,1})	},
	{ Hex::Corner::UpperLeft  , TilesPair({0,-1}, {-1, 0})	},
	{ Hex::Corner::UpperRight , TilesPair({-1,1}, {0,1})	},
};

using TilesCornersPair = Hex::Data::Corners::CornersPair;

const std::unordered_map<Hex::Corner, TilesCornersPair> Hex::Data::Corners::adjacentTileCorners
{
	{ Hex::Corner::Bottom     , TilesCornersPair(Hex::Corner::UpperRight, Hex::Corner::UpperLeft)},
	{ Hex::Corner::BottomLeft , TilesCornersPair(Hex::Corner::BottomRight, Hex::Corner::Upper)},
	{ Hex::Corner::BottomRight, TilesCornersPair(Hex::Corner::BottomLeft, Hex::Corner::Upper)}	,
	{ Hex::Corner::Upper      , TilesCornersPair(Hex::Corner::BottomRight, Hex::Corner::BottomLeft)}	,
	{ Hex::Corner::UpperLeft  , TilesCornersPair(Hex::Corner::Bottom, Hex::Corner::UpperRight)}	,
	{ Hex::Corner::UpperRight , TilesCornersPair(Hex::Corner::Bottom, Hex::Corner::UpperLeft)}	,
};
	
const std::unordered_map<Hex::Edge, std::pair<int8_t, int8_t>> Hex::Data::Edges::adjacentTile
{
	{ Hex::Edge::BottomLeft , {1, -1} },
	{ Hex::Edge::BottomRight, {1,  0} },
	{ Hex::Edge::Left       , {0, -1} },
	{ Hex::Edge::Right      , {0,  1} },
	{ Hex::Edge::UpperLeft  , {-1, 0} },
	{ Hex::Edge::UpperRight , {-1, 1} },
};

const std::unordered_map<Hex::Edge, Hex::Edge> Hex::Data::Edges::analogueEdge
{
	{ Hex::Edge::BottomLeft , Hex::Edge::UpperRight  },
	{ Hex::Edge::BottomRight, Hex::Edge::UpperLeft   },
	{ Hex::Edge::Left       , Hex::Edge::Right       },
	{ Hex::Edge::Right      , Hex::Edge::Left        },
	{ Hex::Edge::UpperLeft  , Hex::Edge::BottomRight },
	{ Hex::Edge::UpperRight , Hex::Edge::BottomLeft  },
};

using RoadsAndPieces = Hex::Data::Edges::AdjacentRoadsAndPieces;

const std::unordered_map<Hex::Edge, RoadsAndPieces> Hex::Data::Edges::currentHexAdjacentEdgesAndPieces
{
	{ Hex::Edge::BottomLeft , RoadsAndPieces({Hex::Edge::BottomRight, Hex::Edge::Left},
											{Hex::Corner::BottomLeft, Hex::Corner::Bottom}) },
	{ Hex::Edge::BottomRight, RoadsAndPieces({Hex::Edge::Right, Hex::Edge::BottomLeft},
											{Hex::Corner::BottomRight, Hex::Corner::Bottom})  },
	{ Hex::Edge::Left       , RoadsAndPieces({Hex::Edge::BottomLeft, Hex::Edge::UpperLeft},
											{Hex::Corner::BottomLeft, Hex::Corner::UpperLeft})  },
	{ Hex::Edge::Right      , RoadsAndPieces({Hex::Edge::UpperRight, Hex::Edge::BottomRight},
											{Hex::Corner::UpperRight, Hex::Corner::BottomRight}) },
	{ Hex::Edge::UpperLeft  , RoadsAndPieces({Hex::Edge::Left, Hex::Edge::UpperRight}, 
											{Hex::Corner::UpperLeft, Hex::Corner::Upper})	},
	{ Hex::Edge::UpperRight , RoadsAndPieces({Hex::Edge::UpperLeft, Hex::Edge::Right},
											{Hex::Corner::Upper, Hex::Corner::UpperRight})	},
};

const std::unordered_map<Hex::Edge, std::pair<Hex::Edge, Hex::Edge>> Hex::Data::Edges::adjacentTileEdges
{
	{ Hex::Edge::BottomLeft , {Hex::Edge::UpperRight, Hex::Edge::Right}		},
	{ Hex::Edge::BottomRight, {Hex::Edge::Left, Hex::Edge::UpperRight}		},
	{ Hex::Edge::Left       , {Hex::Edge::UpperRight, Hex::Edge::BottomRight}},
	{ Hex::Edge::Right      , {Hex::Edge::UpperLeft, Hex::Edge::BottomLeft} },
	{ Hex::Edge::UpperLeft  , {Hex::Edge::BottomLeft, Hex::Edge::Right}		},
	{ Hex::Edge::UpperRight , {Hex::Edge::BottomRight, Hex::Edge::Left}		},
};

const std::unordered_map<Hex::Corner, TilesCornersPair> Hex::Data::Corners::currentTileCorners
{
	{ Hex::Corner::Bottom     , TilesCornersPair(Hex::Corner::BottomRight, Hex::Corner::BottomLeft)},
	{ Hex::Corner::BottomLeft , TilesCornersPair(Hex::Corner::Bottom, Hex::Corner::UpperLeft)},
	{ Hex::Corner::BottomRight, TilesCornersPair(Hex::Corner::Bottom, Hex::Corner::UpperRight)}	,
	{ Hex::Corner::Upper      , TilesCornersPair(Hex::Corner::UpperRight, Hex::Corner::UpperLeft)}	,
	{ Hex::Corner::UpperLeft  , TilesCornersPair(Hex::Corner::BottomLeft, Hex::Corner::Upper)}	,
	{ Hex::Corner::UpperRight , TilesCornersPair(Hex::Corner::BottomRight, Hex::Corner::Upper)}	,
};

const std::unordered_map<Hex::Corner, std::pair<Hex::Edge, Hex::Edge>> Hex::Data::Corners::currentTileEdges
{
	{ Hex::Corner::Bottom     , {Hex::Edge::BottomRight, Hex::Edge::BottomLeft}},
	{ Hex::Corner::BottomLeft , {Hex::Edge::Left, Hex::Edge::BottomLeft}},
	{ Hex::Corner::BottomRight, {Hex::Edge::BottomRight, Hex::Edge::Right} }	,
	{ Hex::Corner::Upper      , {Hex::Edge::UpperLeft, Hex::Edge::UpperRight}	},
	{ Hex::Corner::UpperLeft  ,	{Hex::Edge::Left, Hex::Edge::UpperLeft}	}	,
	{ Hex::Corner::UpperRight , {Hex::Edge::UpperRight, Hex::Edge::Right} }	
};

const std::unordered_map<Hex::Corner, std::pair<Hex::Edge, Hex::Edge>> Hex::Data::Corners::adjacentTileEdges
{
	{ Hex::Corner::Bottom     , {Hex::Edge::Right, Hex::Edge::Left}},
	{ Hex::Corner::BottomLeft , {Hex::Edge::BottomRight, Hex::Edge::UpperLeft}},
	{ Hex::Corner::BottomRight, {Hex::Edge::BottomLeft, Hex::Edge::UpperRight} }	,
	{ Hex::Corner::Upper      , {Hex::Edge::Right, Hex::Edge::Left}	},
	{ Hex::Corner::UpperLeft  ,	{Hex::Edge::UpperRight, Hex::Edge::BottomLeft}	}	,
	{ Hex::Corner::UpperRight , {Hex::Edge::BottomRight, Hex::Edge::UpperLeft} }
};