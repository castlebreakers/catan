#pragma once

#include <utility>
#include <cstdint>

class IPlayer
{
public:
	using DiceRoll = std::pair<size_t, size_t>;

public:
	virtual ~IPlayer() = default;

	virtual DiceRoll requestDiceInput() = 0;

	virtual bool requestBuildingDecisionInput() = 0;

	virtual bool requestTradeDecisionInput() = 0;
};