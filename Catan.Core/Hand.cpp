#include "Hand.h"

#include <Catan.Core/Exceptions.h>

#include <algorithm>

void Hand::add(Resource::Type type, uint16_t numberOfResources) noexcept
{
	auto it = m_resources.find(type);
	if (it == std::end(m_resources))
	{
		m_resources[type] = numberOfResources;
	}
	else
	{
		m_resources[type] += numberOfResources;
	}
}

void Hand::discard(Resource::Type type, uint16_t numberOfResources)
{
	if (auto it = m_resources.find(type); it != m_resources.end())
	{
		if (numberOfResources <= m_resources[type])
			m_resources[type] -= numberOfResources;
		else
			throw Exceptions::ResourceNotEnoughException("Not enough resources.");

		////If you have 0 cards of that resource, we erase that entire resource type from the container
		//if (!m_resources[type])
		//	m_resources.erase(type);
	}
	else
		throw Exceptions::ResourceNotAvailableException("That resource type doesn't exist");
}

uint16_t Hand::numberOfCards(Resource::Type type) const
{
	if (auto it = m_resources.find(type); it != std::end(m_resources))
	{
		return it->second;
	}
	else
	{
		static constexpr uint16_t kZeroCards = 0;
		return kZeroCards;
	}
}

uint16_t Hand::totalNumberOfResourceCards() const noexcept
{
	uint16_t totalNumberOfResourceCards = 0;
	std::for_each(std::begin(m_resources), std::end(m_resources), 
		[&totalNumberOfResourceCards](const std::pair<Resource::Type, uint16_t>& pair)
	{
		totalNumberOfResourceCards += pair.second;

	});

	return totalNumberOfResourceCards;
}

uint16_t Hand::totalNumberOfDevelopmentCards() const noexcept
{
	return m_developmentCards.size();
}

uint16_t Hand::totalNumberOfUnusedDevelopmentCards() const noexcept
{
	return std::count_if(std::begin(m_developmentCards), std::end(m_developmentCards),
		[](const DevelopmentCard& card)
	{
		return !card.isApplied();
	});
}

void Hand::add(DevelopmentCard developmentCard) noexcept
{	
	m_developmentCards.push_back(std::move(developmentCard));
}

const std::unordered_map<Resource::Type, uint16_t>& Hand::getResources() const noexcept
{
	return m_resources;
}

std::vector<DevelopmentCard>& Hand::getDevelopmentCards() noexcept
{
	return m_developmentCards;
}

DevelopmentCard& Hand::operator[](DevelopmentCard::Type type)
{
	for (auto& card : m_developmentCards)
	{
		if (card.getType() == type && !card.isApplied())
			return card;
	}

	return DevelopmentCard();
}

void Hand::markAsApplied(DevelopmentCard & card)
{
	card.apply();
}

void Hand::markAsApplied(DevelopmentCard::Type type)
{
	auto& card = (*this)[type];
	
	if (card.getType() != DevelopmentCard::Type::None)
		card.apply();
}

void Hand::discardHalf()
{
	uint16_t numberOfDiscardedCards = 0;
	auto allResourcesCounter = totalNumberOfResourceCards();

	auto neededToDiscardCounter = allResourcesCounter / 2;

	discardUntilCounterIsEqualToTheNeededQuantity(neededToDiscardCounter);
}

void Hand::discardUntilCounterIsEqualToTheNeededQuantity(uint16_t requestedNumber)
{
	uint16_t numberOfDiscardedCards = 0;

	while (numberOfDiscardedCards != requestedNumber)
	{
		for (auto[resourceType, count] : m_resources)
		{
			if (static constexpr uint8_t kNoCardsSpecifier = 0;
				count == kNoCardsSpecifier)
				continue;
	

			discard(resourceType);
			++numberOfDiscardedCards;

			if (numberOfDiscardedCards == requestedNumber)
				break;
		}
	}
}



