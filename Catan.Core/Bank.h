#pragma once

#include <vector>

#include <Catan.Core/DevelopmentCard.h>

#include <Catan.Utils/NonCopyable.hpp>
#include <Catan.Utils/NonMovable.hpp>

#include "catan_core_exports.h"

class CATAN_CORE_EXPORTS Bank : public NonCopyable, public NonMovable
{
public:
	Bank();

	DevelopmentCard drawCard();

	uint8_t remainingDevelopmentCards() const noexcept;

private:
	void populate() noexcept;

	std::vector<DevelopmentCard> m_developmentCards;
};