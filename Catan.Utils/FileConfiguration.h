#pragma once
#include "catan_utils_exports.h"
#include <fstream>
#include <string_view>
#include <regex>
#include <unordered_map>
#include <string>
#include <sstream>
#include <Catan.Utils/exceptions.h>

class CATAN_UTILS_EXPORTS FileConfiguration
{
public:
	FileConfiguration(std::string_view filePath);

	template <typename Iterator>
	FileConfiguration(std::string_view filePath, Iterator begin, Iterator end);

	/**
	 * Gets the value of a property from the config
	 * The returned type must have the >> operator overloaded and have default constructor.
	 * \param key The property name
	 * \return The property value
	 */
	template <typename T>
	T get(const std::string& key) const;;

	/**
	 * Puts the value of a property in the config
	 * The property type must have the << operator overloaded and NOT output new line in it.
	 * Also, this function will not write to the file.
	 * Please call the <code>save()</code> method if you want to write the changes to the file.
	 * \param key The property name
	 * \param object The value to insert
	 */
	template <typename T>
	void set(const std::string& key, const T& object);

	void save();
private:
	std::unordered_map<std::string, std::string>::const_iterator find(const std::string& key) const;
	void init();
	void read();
	std::fstream m_file;
	std::string m_filePath;
	std::unordered_map<std::string, std::string> m_propertyMap;

	static const std::regex kPropertyRegex;
};


template <typename Iterator>
FileConfiguration::FileConfiguration(std::string_view filePath, Iterator begin, Iterator end) :
	m_filePath(filePath)
{
	try
	{
		init();
	} 
	catch(Exceptions::CouldNotOpenFileException)
	{
		m_propertyMap.insert(begin, end);
		save();
	}
}

template<typename T>
inline T FileConfiguration::get(const std::string& key) const
{
	auto propertyIterator = find(key);
	std::istringstream stringStream(propertyIterator->second);
	T value;
	stringStream >> std::boolalpha >> value;
	if (stringStream.fail())
		throw Exceptions::WrongPropertyTypeException(key);
	return value;
}

template<typename T>
inline void FileConfiguration::set(const std::string& key, const T& object)
{
	std::ostringstream stringStream;
	stringStream << std::boolalpha << object;
	m_propertyMap[key] = stringStream.str();
}
