#pragma once
#include <random>

#include "catan_utils_exports.h"

template <typename RandomEngine = std::mt19937, typename SeedGenerator = std::random_device>
class Random
{
public:
	Random(uint32_t seed = SeedGenerator{}()) noexcept;

	template <typename Type = float>
	Type getRealInRange(Type lowerBound, Type upperBound) noexcept;

	template <typename Type = int>
	Type getIntegerInRange(Type lowerBound, Type upperBound) noexcept;

	template <typename Type = int, typename InputIterator>
	Type getElement(InputIterator first, InputIterator last);

	template <typename Type = int>
	Type getElement(std::initializer_list<Type> list);

	template <typename Type = int, typename Container>
	Type getElement(const Container& container);

	//template <typename Type = int, typename Container>
	//Type& getElement(Container& container);

	RandomEngine& getGenerator() noexcept;

private:
	RandomEngine m_generator;

	static constexpr uint8_t kFirstElementInListIndex = 0;

	static constexpr uint8_t kLastElementOffset = 1;

};

template<typename RandomEngine, typename SeedGenerator>
inline Random<RandomEngine, SeedGenerator>::Random(uint32_t seed) noexcept :
	m_generator(seed)
{
}

template<typename RandomEngine, typename SeedGenerator>
inline RandomEngine & Random<RandomEngine, SeedGenerator>::getGenerator() noexcept
{
	return m_generator;
}

template<typename RandomEngine, typename SeedGenerator>
template<typename Type>
inline Type Random<RandomEngine, SeedGenerator>::getRealInRange(Type lowerBound, Type upperBound) noexcept
{
	std::uniform_real_distribution<Type> distribution(lowerBound, upperBound);
	return distribution(m_generator);
}

template<typename RandomEngine, typename SeedGenerator>
template<typename Type>
inline Type Random<RandomEngine, SeedGenerator>::getIntegerInRange(Type lowerBound, Type upperBound) noexcept
{
	std::uniform_int_distribution<Type> distribution(lowerBound, upperBound);
	return distribution(m_generator);
}

template<typename RandomEngine, typename SeedGenerator>
template<typename Type, typename InputIterator>
inline Type Random<RandomEngine, SeedGenerator>::getElement(InputIterator first, InputIterator last)
{
	auto randomIndex = getIntegerInRange<Type>(kFirstElementInListIndex,
		std::distance(first, last) - kLastElementOffset);

	return *std::next(first, randomIndex);
}

template<typename RandomEngine, typename SeedGenerator>
template<typename Type>
inline Type Random<RandomEngine, SeedGenerator>::getElement(std::initializer_list<Type> list)
{
	return getElement(std::begin(list), std::end(list));
}

template<typename RandomEngine, typename SeedGenerator>
template<typename Type, typename Container>
inline Type Random<RandomEngine, SeedGenerator>::getElement(const Container&  container)
{
	return getElement(std::cbegin(container), std::cend(container));
}

//template <typename RandomEngine, typename SeedGenerator>
//template <typename Type, typename Container>
//inline Type& Random<RandomEngine, SeedGenerator>::getElement(Container& container)
//{
//	auto randomIndex = getIntegerInRange<Type>(kFirstElementInListIndex, std::distance(std::begin(container), std::end(container) - kLastElementOffset));
//	return *std::next(std::begin(container), randomIndex);
//}

