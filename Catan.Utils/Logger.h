#pragma once
#include "catan_utils_exports.h"
#include <ostream>
#include <unordered_map>
#include <string_view>

class CATAN_UTILS_EXPORTS Logger
{

public:
	enum class Level
	{
		Debug = 0,
		Info,
		Warning,
		Error
	};
	Logger(std::ostream& outputStream, Level minimumLevel = Level::Info, bool printTimestamp = false);
	virtual ~Logger() = default;

	virtual void log(std::string_view message, Level level);

	void setMinimumLogLevel(Level level);
	constexpr Level getMinimumLogLevel() const;

	void setPrintTimestamp(bool printTimestamp);
	constexpr bool getPrintTimestamp() const;

private:
	static const std::string_view levelToString(Level level);
	static const std::unordered_map<Level, std::string_view> m_levelToStringMap;
	std::ostream& m_outputStream;
	Level m_minimumLevel:2;
	bool m_printTimestamp:1;
};