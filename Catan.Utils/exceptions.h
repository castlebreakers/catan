#include "catan_utils_exports.h"
#include <stdexcept>

namespace Exceptions
{
	struct CATAN_UTILS_EXPORTS ConfigurationException : public std::runtime_error
	{
		ConfigurationException(std::string_view message);
	};

	struct CATAN_UTILS_EXPORTS CouldNotOpenFileException : public ConfigurationException
	{
		CouldNotOpenFileException(std::string_view filePath);
		const std::string filePath;
	};

	struct CATAN_UTILS_EXPORTS PropertyException : public ConfigurationException
	{
		PropertyException(std::string_view message, std::string_view propertyName);
		const std::string propertyName;
	};

	struct CATAN_UTILS_EXPORTS PropertyNotFoundException : public PropertyException
	{
		PropertyNotFoundException(std::string_view propertyName);
	};

	struct CATAN_UTILS_EXPORTS WrongPropertyTypeException : public PropertyException
	{
		WrongPropertyTypeException(std::string_view propertyName);
	};
}