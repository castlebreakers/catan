#pragma once

#include "catan_utils_exports.h"

#include <memory>
#include <vector>

template <typename ObserverType>
class Observable
{
public:
	void addObserver(std::shared_ptr<ObserverType> observer) noexcept;

	void removeObserver(std::shared_ptr<ObserverType> observer);

protected:
	std::vector<std::shared_ptr<ObserverType>> m_observers;
};

template<typename ObserverType>
inline void Observable<ObserverType>::addObserver(std::shared_ptr<ObserverType> observer) noexcept
{
	m_observers.push_back(observer);
}

template<typename ObserverType>
inline void Observable<ObserverType>::removeObserver(std::shared_ptr<ObserverType> observer)
{
	auto listenerIterator = std::find(std::begin(m_observers), std::end(m_observers), observer);
	if (listenerIterator == m_observers.end())
		throw std::runtime_error("Listener not attached.");

	m_observers.erase(listenerIterator);
}