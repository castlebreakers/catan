#include "StringUtils.h"

std::string StringUtils::extractSingleRegex(const std::string& string, const std::regex& regex)
{
	std::smatch match;
	if (!std::regex_match(string, match, regex) || match.size() < 2) return match[0];
	return match[1];
}
