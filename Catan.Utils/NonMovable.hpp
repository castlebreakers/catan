#pragma once

#include "catan_utils_exports.h"

struct CATAN_UTILS_EXPORTS NonMovable
{
	NonMovable() = default;
	NonMovable(NonMovable&& other) = delete;
	NonMovable& operator = (NonMovable&& other) = delete;
};