#pragma once

template <typename T = int>
struct Vector2
{
	constexpr Vector2() noexcept = default;
	constexpr Vector2(T x, T y) noexcept :
		x(x), y(y) 
	{}
	bool operator==(const Vector2& other) const noexcept;
	bool operator!=(const Vector2& other) const noexcept;
	T x = 0;
	T y = 0;
};

template <typename T = int>
struct Vector3 : public Vector2<T>
{
	constexpr Vector3() noexcept = default;
	constexpr Vector3(T x, T y, T z) noexcept :
		Vector2<T>(x, y), z(z)
	{}
	bool operator==(const Vector3& other) const noexcept;
	bool operator!=(const Vector3& other) const noexcept;
	T z = 0;
};

template <typename T = int>
struct Vector4 : public Vector3<T>
{
	constexpr Vector4() noexcept = default;
	constexpr Vector4(T x, T y, T z, T w) noexcept :
		Vector3<T>(x, y, z), w(w)
	{}
	bool operator==(const Vector4& other) const noexcept;
	bool operator!=(const Vector4& other) const noexcept;
	T w = 0;
};

template <typename T = unsigned int>
struct Size
{
	constexpr Size() noexcept = default;
	constexpr Size(T width, T height) noexcept :
		width(width), height(height)
	{}
	bool operator==(const Size& other) const noexcept;
	bool operator!=(const Size& other) const noexcept;
	T width = 0;
	T height = 0;
};

template<typename T>
inline bool Vector2<T>::operator==(const Vector2 & other) const noexcept
{
	return this->x == other.x && this->y == other.y;
}

template<typename T>
inline bool Vector2<T>::operator!=(const Vector2 & other) const noexcept
{
	return !(*this == other);
}

template<typename T>
inline bool Vector3<T>::operator==(const Vector3 & other) const noexcept
{
	return this->x == other.x && this->y == other.y && this->z == other.z;
}

template<typename T>
inline bool Vector3<T>::operator!=(const Vector3 & other) const noexcept
{
	return !(*this == other);
}

template<typename T>
inline bool Vector4<T>::operator==(const Vector4 & other) const noexcept
{
	return this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w;
}

template<typename T>
inline bool Vector4<T>::operator!=(const Vector4 & other) const noexcept
{
	return !(*this == other);
}

template<typename T>
inline bool Size<T>::operator==(const Size & other) const noexcept
{
	return this->width == other.width && this->height == other.height;
}

template<typename T>
inline bool Size<T>::operator!=(const Size & other) const noexcept
{
	return !(*this == other);
}