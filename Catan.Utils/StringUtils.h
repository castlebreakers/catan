#pragma once
#include <regex>
#include <sstream>
#include "catan_utils_exports.h"

class CATAN_UTILS_EXPORTS StringUtils
{
public:
	static std::string extractSingleRegex(const std::string& string, const std::regex& regex);

	template <typename T>
	static std::string getClassName(T object);

	template <typename T>
	static std::string getClassName(std::shared_ptr<T> pointer);

	template <typename T>
	static T stringTo(const std::string& input);
};

template <typename T>
std::string StringUtils::getClassName(T object)
{
	static const std::regex extractRegex("class ([A-Za-z]+)");
	return extractSingleRegex(typeid(T).name(), extractRegex);
}

template <typename T>
std::string StringUtils::getClassName(std::shared_ptr<T> pointer)
{
	static const std::regex extractRegex("class std\:\:shared_ptr([A-Za-z]+)");
	return extractSingleRegex(typeid(T).name(), extractRegex);
}

template <typename T>
inline T StringUtils::stringTo(const std::string& input)
{
	std::istringstream stringStream(input);
	T result;
	stringStream >> std::boolalpha >> result;
	return result;
}
