#pragma once

template<typename Derived, typename Base>
bool instanceof(Base* base)
{
	return (dynamic_cast<Derived*>(base) != nullptr);
}

template <typename Derived, typename Base>
bool instanceof(std::shared_ptr<Base> base)
{
	return (std::dynamic_pointer_cast<Derived>(base) != nullptr);
}