#include "Logger.h"
#include <chrono>
#include <ctime>
#include <iomanip>

Logger::Logger(std::ostream & outputStream, Level minimumLevel, bool printTimestamp) :
	m_outputStream(outputStream),
	m_minimumLevel(minimumLevel),
	m_printTimestamp(printTimestamp)
{
}

void Logger::log(std::string_view message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(m_minimumLevel))
		return;

	if (m_printTimestamp)
	{
		std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		m_outputStream << "[" << std::put_time(std::localtime(&time), "%Y-%m-%d %X") << "] ";
	}

	m_outputStream << "[" << levelToString(level) << "]\t" << message << std::endl;
}

void Logger::setMinimumLogLevel(Level level)
{
	m_minimumLevel = level;
}

constexpr Logger::Level Logger::getMinimumLogLevel() const
{
	return m_minimumLevel;
}

void Logger::setPrintTimestamp(bool printTimestamp)
{
	m_printTimestamp = printTimestamp;
}

constexpr bool Logger::getPrintTimestamp() const
{
	return m_printTimestamp;
}

const std::string_view Logger::levelToString(Logger::Level level)
{
	auto iterator = m_levelToStringMap.find(level);
	if (iterator == m_levelToStringMap.cend())
		return "";
	return iterator->second;
}

const std::unordered_map<Logger::Level, std::string_view> Logger::m_levelToStringMap 
{
	{ Logger::Level::Debug	,	"Debug"   },
	{ Logger::Level::Info	,	"Info"	  },
	{ Logger::Level::Warning,	"Warning" },
	{ Logger::Level::Error	,	"Error"   },
};