#pragma once

#include "catan_utils_exports.h"

struct CATAN_UTILS_EXPORTS NonCopyable
{
	NonCopyable() = default;
	NonCopyable(const NonCopyable& other) = delete;
	NonCopyable& operator = (const NonCopyable& other) = delete;
};