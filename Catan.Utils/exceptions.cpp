#include "exceptions.h"

Exceptions::ConfigurationException::ConfigurationException(std::string_view message) :
	std::runtime_error(message.data())
{
}

Exceptions::CouldNotOpenFileException::CouldNotOpenFileException(std::string_view filePath) :
	ConfigurationException(std::string("Could not open file ") + filePath.data() + "."),
	filePath(filePath)
{

}

Exceptions::PropertyException::PropertyException(std::string_view message, std::string_view propertyName) :
	ConfigurationException(message),
	propertyName(propertyName)
{
}

Exceptions::PropertyNotFoundException::PropertyNotFoundException(std::string_view propertyName) :
	PropertyException(std::string("Property ") + propertyName.data() + " not found.", propertyName)
{
}

Exceptions::WrongPropertyTypeException::WrongPropertyTypeException(std::string_view propertyName) :
	PropertyException(std::string("Property ") + propertyName.data() + " has a different type than the one provided.", propertyName)
{
}
