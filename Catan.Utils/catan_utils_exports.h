
#ifndef CATAN_UTILS_EXPORTS_H
#define CATAN_UTILS_EXPORTS_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define CATAN_UTILS_EXPORTS
#  define CATAN_UTILS_NO_EXPORT
#else
#  ifndef CATAN_UTILS_EXPORTS
#    ifdef Catan_Utils_EXPORTS
        /* We are building this library */
#      define CATAN_UTILS_EXPORTS __declspec(dllexport)
#    else
        /* We are using this library */
#      define CATAN_UTILS_EXPORTS __declspec(dllimport)
#    endif
#  endif

#  ifndef CATAN_UTILS_NO_EXPORT
#    define CATAN_UTILS_NO_EXPORT 
#  endif
#endif

#ifndef CATAN_UTILS_DEPRECATED
#  define CATAN_UTILS_DEPRECATED __declspec(deprecated)
#endif

#ifndef CATAN_UTILS_DEPRECATED_EXPORT
#  define CATAN_UTILS_DEPRECATED_EXPORT CATAN_UTILS_EXPORTS CATAN_UTILS_DEPRECATED
#endif

#ifndef CATAN_UTILS_DEPRECATED_NO_EXPORT
#  define CATAN_UTILS_DEPRECATED_NO_EXPORT CATAN_UTILS_NO_EXPORT CATAN_UTILS_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef CATAN_UTILS_NO_DEPRECATED
#    define CATAN_UTILS_NO_DEPRECATED
#  endif
#endif

#endif /* CATAN_UTILS_EXPORTS_H */
