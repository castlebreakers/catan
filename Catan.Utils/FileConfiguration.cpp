#include "FileConfiguration.h"

const std::regex FileConfiguration::kPropertyRegex("^([^= ]+)\\s*=\\s*(.+)$");

FileConfiguration::FileConfiguration(std::string_view filePath) :
	m_filePath(filePath)
{
	init();
}

void FileConfiguration::save()
{
	m_file.open(m_filePath, std::fstream::out | std::fstream::trunc);
	if (m_file.fail())
		throw Exceptions::CouldNotOpenFileException(m_filePath);
	for (const auto&[name, value] : m_propertyMap)
		m_file << name << " = " << value << std::endl;
	m_file.close();
}

std::unordered_map<std::string, std::string>::const_iterator FileConfiguration::find(const std::string & key) const
{
	auto propertyIterator = m_propertyMap.find(key);
	if (propertyIterator == m_propertyMap.end())
		throw Exceptions::PropertyNotFoundException(key);
	return propertyIterator;
}

void FileConfiguration::init()
{
	m_file.open(m_filePath, std::fstream::in);
	if (m_file.fail())
		throw Exceptions::CouldNotOpenFileException(m_filePath);
	read();
	m_file.close();
}

void FileConfiguration::read()
{
	std::string currentLine;
	while (std::getline(m_file, currentLine))
	{
		std::smatch propertyMatch;
		if (!std::regex_match(currentLine, propertyMatch, kPropertyRegex)) continue;
		if (propertyMatch.size() < 3) continue;
		auto& propertyName = propertyMatch[1];
		auto& propertyValue = propertyMatch[2];
		m_propertyMap.emplace(propertyName, propertyValue);
	}
}

template<>
std::string FileConfiguration::get<std::string>(const std::string& key) const
{
	return find(key)->second;
}