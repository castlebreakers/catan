# The Settlers of Catan

## Team members

-   Gagiu George-Daniel
-   Hanganu Bogdan
-   Ifrim Silviu-Marian
-   Man Ștefania

## Our mission

Our team was created because we felt the need for better games to exist.  
And now the real reason: We had to do an university project.

## Our project

A C++ implementation of The Settlers of Catan (4th edition).

## Game Components

 - 19 terrain hexes (tiles) 
 - 9 sea harbors
 - 18 circular number tokens (chits)
 - Resource Cards
	 - ore 	
	 - grain 
	 - lumber 
	 - wool 	
	 - brick
 - 25 Development Cards
	 - 14 Knight/Soldier Cards 	
    - 6 Progress Cards 	
    - 5 Victory Point Cards  	
    - 4 “Building Costs” Cards  	
    - 2 Special Cards: “Longest Road” & “Largest Army”
 - 16 cities (4 of each color)  
 - 20 settlements (5 of each color)  
 - 60 roads (15 of each color)  
 - 2 dice  
 - 1 robber

## Gameplay Overview

During the setup of the game (the Zero Decision State), each player places four Roads and two Settlements and receives the starting resources. Once the game begins, each student takes turn rolling the dice to determine which tiles produce resources. After rolling, the current player has the opportunity to place a Road, a Settlement/City or to buy a Development Card by using his/her resources. The goal of the game is to be the first of the other players to obtain 10 victory points.

## Game Design

### Board 

We use a two dimensional array for the hexagonal grid and we access the tiles by row and column. 

We set up the game board by randomly generating tiles (terrain hexes)  and hard-coding the Harbors and Water positions. Moreover, when we generate the tiles, we also randomly generate the numbers associated with them.  

Tiles are represented by hexagons. Players can place Roads on their edges, and Settlements/Cities on their corners. Appropriate verification is performed for valid positions. 

## Game Presentation Video

### Link: https://www.youtube.com/watch?v=bb4hDd98Lz4&feature=youtu.be

## AI 

### Hint Provider

AI is a big part of games. So we had to integrate it in our project.

We created an intelligent hint provider that evaluates the most advantageous action you can take in order to obtain a maximum score. The possible actions are:
 - Place a road
 - Place a Settlement
 - Palce a City
 - Buy Development Card
 - Play Development Card
 - Trade resources with the Bank
When it's the first turn, the hint provider evaluates the most fit positions where you can place a Settlement/City and recommends you the best one. The criteria we took into consideration is the following:
 - the three adjacent tiles of the corner position must have different resources
 - the dice roll odds, concerning the numbers on the adjacent tiles
 During the game, the hint provider recommends an action every turn.

### Link: https://youtu.be/kMtURqRI4dM