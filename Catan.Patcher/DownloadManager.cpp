#include "DownloadManager.h"
#include <SFML/Network.hpp>
#include <fstream>

DownloadManager::DownloadManager(std::string_view host, std::string_view remoteDirectory) :
	m_host(host),
	m_remoteDirectory(remoteDirectory)
{ }

void DownloadManager::downloadFile(std::string_view remotePath, std::string_view filePath) const
{
	auto body = download(m_host, m_remoteDirectory + "/" + remotePath.data());
	std::ofstream file(filePath.data(), std::ios::out | std::ios::binary);
	file.write(body.c_str(), body.size());
	file.close();
}

std::string DownloadManager::download(std::string_view host, std::string_view remotePath)
{
	sf::Http http(host.data());
	sf::Http::Request request;
	request.setMethod(sf::Http::Request::Get);
	request.setUri(remotePath.data());
	auto response = http.sendRequest(request);
	return response.getBody();
}