#pragma once
#include "catan_patcher_exports.h"
#include <unordered_map>
#include <regex>

class CATAN_PATCHER_EXPORTS FileList
{
public:
	using Container = std::vector<std::string>;
	FileList(std::string_view localDirectoryPath, std::string_view remoteHost, std::string_view remoteFileListPath, bool checkFileSizes = true);
	Container::const_iterator begin() const;
	Container::const_iterator end() const;
private:
	void addLocalFiles(std::string_view path);
	void addRemoteFiles(std::string_view host, std::string_view path);
	void addDiffFiles();

	std::unordered_map<std::string, uint32_t> m_localFiles;
	std::unordered_map<std::string, uint32_t> m_remoteFiles;
	Container m_diffFiles;
	bool m_checkFileSizes;

	static const std::regex kFileLineRegex;
};
