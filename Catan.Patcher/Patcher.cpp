#include "Patcher.h"
#include <filesystem>

Patcher::Patcher(const DownloadManager& downloadManager, const FileList& fileList, std::string_view assetFolder) :
	m_downloadManager(downloadManager),
	m_fileList(fileList),
	m_assetFolderPath(assetFolder)
{
}

void Patcher::run() const
{
	std::vector<std::thread> downloadThreads;
	for(const auto& filePath : m_fileList)
	{
		std::filesystem::path localPath(m_assetFolderPath + "/" + filePath);
		if (localPath.has_parent_path() && !std::filesystem::exists(localPath.parent_path()))
			std::filesystem::create_directories(localPath.parent_path());

		downloadThreads.emplace_back(&DownloadManager::downloadFile, m_downloadManager, filePath, localPath.generic_string());
	}
	for (auto& thread : downloadThreads)
		if (thread.joinable())
			thread.join();
}
