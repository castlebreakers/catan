#include "FileList.h"
#include <filesystem>
#include <Catan.Patcher/DownloadManager.h>
#include <Catan.Utils/StringUtils.h>

FileList::FileList(std::string_view localDirectoryPath, std::string_view remoteHost,
	std::string_view remoteFileListPath, bool checkFileSizes) :
	m_checkFileSizes(checkFileSizes)
{
	addLocalFiles(localDirectoryPath);
	addRemoteFiles(remoteHost, remoteFileListPath);
	addDiffFiles();
}

FileList::Container::const_iterator FileList::begin() const
{
	return m_diffFiles.cbegin();
}

FileList::Container::const_iterator FileList::end() const
{
	return m_diffFiles.cend();
}

void FileList::addLocalFiles(std::string_view path)
{
	std::filesystem::create_directories(path);
	for (const auto & entry : std::filesystem::recursive_directory_iterator(path))
		if (entry.is_regular_file())
			m_localFiles.emplace(entry.path().generic_string().substr(path.length()), entry.file_size());
}

void FileList::addRemoteFiles(std::string_view host, std::string_view path)
{
	std::string remoteFilesString = DownloadManager::download(host, path);
	const std::sregex_iterator  endIt;
	for(std::sregex_iterator  it(remoteFilesString.begin(), remoteFilesString.end(), kFileLineRegex); it != endIt; ++it)
	{
		auto filePath = (*it)[1];
		auto size = StringUtils::stringTo<uint32_t>((*it)[2]);
		m_remoteFiles.emplace(filePath, size);
	}
}

void FileList::addDiffFiles()
{
	for(const auto& [file, size] : m_remoteFiles)
	{
		auto localIt = m_localFiles.find(file);
		if (localIt == m_localFiles.end() || (m_checkFileSizes && localIt->second != size))
			m_diffFiles.emplace_back(file);
	}
}

const std::regex FileList::kFileLineRegex(R"(([^\|]+)\|([0-9]+)[\s]*)");
