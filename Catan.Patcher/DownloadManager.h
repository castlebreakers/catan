#pragma once
#include "catan_patcher_exports.h"
#include <string_view>

class CATAN_PATCHER_EXPORTS DownloadManager
{
public:
	DownloadManager(std::string_view host, std::string_view remoteDirectory);
	void downloadFile(std::string_view remotePath, std::string_view filePath) const;
	static std::string download(std::string_view host, std::string_view remotePath);
private:
	const std::string m_host;
	const std::string m_remoteDirectory;
};

