
#ifndef CATAN_PATCHER_EXPORTS_H
#define CATAN_PATCHER_EXPORTS_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define CATAN_PATCHER_EXPORTS
#  define CATAN_PATCHER_NO_EXPORT
#else
#  ifndef CATAN_PATCHER_EXPORTS
#    ifdef Catan_Patcher_EXPORTS
        /* We are building this library */
#      define CATAN_PATCHER_EXPORTS __declspec(dllexport)
#    else
        /* We are using this library */
#      define CATAN_PATCHER_EXPORTS __declspec(dllimport)
#    endif
#  endif

#  ifndef CATAN_PATCHER_NO_EXPORT
#    define CATAN_PATCHER_NO_EXPORT 
#  endif
#endif

#ifndef CATAN_PATCHER_DEPRECATED
#  define CATAN_PATCHER_DEPRECATED __declspec(deprecated)
#endif

#ifndef CATAN_PATCHER_DEPRECATED_EXPORT
#  define CATAN_PATCHER_DEPRECATED_EXPORT CATAN_PATCHER_EXPORTS CATAN_PATCHER_DEPRECATED
#endif

#ifndef CATAN_PATCHER_DEPRECATED_NO_EXPORT
#  define CATAN_PATCHER_DEPRECATED_NO_EXPORT CATAN_PATCHER_NO_EXPORT CATAN_PATCHER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef CATAN_PATCHER_NO_DEPRECATED
#    define CATAN_PATCHER_NO_DEPRECATED
#  endif
#endif

#endif /* CATAN_PATCHER_EXPORTS_H */
