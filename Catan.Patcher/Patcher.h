#pragma once
#include "catan_patcher_exports.h"
#include <Catan.Patcher/DownloadManager.h>
#include <Catan.Patcher/FileList.h>
#include <thread>

class CATAN_PATCHER_EXPORTS Patcher
{
public:
	Patcher(const DownloadManager& downloadManager, const FileList& fileList, std::string_view assetFolder);
	void run() const;
private:
	std::string m_assetFolderPath;
	FileList m_fileList;
	DownloadManager m_downloadManager;
};
