#pragma once
#include "catan_graphics_exports.h"
#include <Catan.Graphics/Drawable.h>
#include <SFML/Graphics/RenderWindow.hpp>

class Application;
class CATAN_GRAPHICS_EXPORTS Renderer
{
public:
	Renderer(std::shared_ptr<Application> application);
	void draw(std::shared_ptr<Drawable> drawable);
private:
	sf::RenderWindow* m_window;
};