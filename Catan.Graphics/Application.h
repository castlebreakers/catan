#pragma once
#include "catan_graphics_exports.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <Catan.Utils/vector.hpp>
#include <Catan.Graphics/Color.h>
#include <string_view>

class EventListener;

class CATAN_GRAPHICS_EXPORTS Application
{
	friend class Renderer;
public:
	Application(const Size<>& size, std::string_view windowName, const Color& backgroundColor = { 255, 255, 255 }, bool fullscreen = false);
	virtual ~Application() = default;
	void run();
	void setEventListener(const std::shared_ptr<EventListener>& eventListener);
	Vector2<> getMousePosition() const;
	void setMousePosition(const Vector2<>& point);
	Size<> getWindowSize() const;
	void setWindowSize(const Size<>& size);
	static Size<> getScreenResolution();
	void setBackgroundColor(const Color& backgroundColor);
	void close();
protected:
	virtual void start() = 0;
	virtual void update(float deltaTime) = 0;
	virtual void draw(float deltaTime) = 0;
private:
	static const uint8_t kDesiredFPS = 60;
	void handleEvent(const sf::Event& event);
	void pollEvents();
	std::shared_ptr<EventListener> m_eventListener = nullptr;
	sf::RenderWindow m_window;
	Color m_backgroundColor;
};