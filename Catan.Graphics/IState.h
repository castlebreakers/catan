#pragma once
class IState
{
public:
	virtual ~IState() = default;
	virtual void onEnter() = 0;
	virtual void onExit() = 0;
};