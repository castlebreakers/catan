#include "DrawableText.h"

DrawableText::DrawableText(const std::shared_ptr<Font>& font, const Vector2<float>& position, std::string_view text, const Color & color, uint32_t characterSize)
{
	m_text.setFont(font->m_sfObject);
	setPosition(position);
	setText(text);
	m_text.setCharacterSize(characterSize);
	m_text.setFillColor({ color.r, color.g, color.b, color.a });
}

Color DrawableText::getColor() const
{
	return m_color;
}

void DrawableText::setColor(const Color & color)
{
	m_color = color;
	m_text.setFillColor({ color.r, color.g, color.b, color.a });
}

void DrawableText::setText(std::string_view text)
{
	m_text.setString(text.data());
}

std::string DrawableText::getText() const
{
	return m_text.getString().toAnsiString();
}

void DrawableText::setCharacterSize(uint32_t characterSize)
{
	m_text.setCharacterSize(characterSize);
}

uint32_t DrawableText::getCharacterSize() const
{
	return m_text.getCharacterSize();
}

Vector2<float> DrawableText::getPosition() const
{
	auto sfPosition = m_text.getPosition();
	return { sfPosition.x, sfPosition.y };
}

bool DrawableText::isClicked(const Vector2<>& mousePosition) const
{
	return false;
}

void DrawableText::setPosition(const Vector2<float>& newPosition)
{
	m_text.setPosition(newPosition.x, newPosition.y);
}

void DrawableText::requestDraw(sf::RenderWindow* window) const
{
	window->draw(m_text);
}
