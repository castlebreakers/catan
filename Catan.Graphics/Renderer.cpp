#include "Renderer.h"
#include <Catan.Graphics/Application.h>

Renderer::Renderer(std::shared_ptr<Application> application) :
	m_window(&application->m_window)
{
}

void Renderer::draw(std::shared_ptr<Drawable> drawable)
{
	drawable->requestDraw(m_window);
}
