#pragma once
#include "catan_graphics_exports.h"
#include <cstdint>

struct CATAN_GRAPHICS_EXPORTS Color
{
	Color() = default;
	constexpr Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255) noexcept;
	bool operator==(const Color& other) const noexcept;
	bool operator!=(const Color& other) const noexcept;
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	uint8_t a = 255;
};