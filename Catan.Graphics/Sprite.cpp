#include "Sprite.h"

Sprite::Sprite(const std::shared_ptr<Texture> & texture) :
	m_sprite(texture->m_sfObject)
{
}

Vector2<float> Sprite::getScale() const
{
	auto scale = m_sprite.getScale();
	return { scale.x, scale.y };
}

void Sprite::setScale(const Vector2<float>& scale)
{
	m_sprite.setScale(scale.x, scale.y);
}

Vector2<float> Sprite::getPosition() const
{
	auto position = m_sprite.getPosition();
	return { position.x, position.y };
}

void Sprite::setPosition(const Vector2<float>& position)
{
	m_sprite.setPosition(position.x, position.y);
}

Vector2<float> Sprite::getOrigin() const
{
	auto origin = m_sprite.getOrigin();
	return { origin.x, origin.y };
}

void Sprite::setOrigin(const Vector2<float>& origin)
{
	m_sprite.setOrigin(origin.x, origin.y);
}

float Sprite::getRotation() const
{
	return m_sprite.getRotation();
}

void Sprite::setRotation(float angle)
{
	m_sprite.setRotation(angle);
}

void Sprite::setTexture(const std::shared_ptr<Texture>& texture)
{
	m_sprite.setTexture(texture->m_sfObject);
}

void Sprite::requestDraw(sf::RenderWindow* window) const
{
	window->draw(m_sprite);
}

Size<float> Sprite::getSize() const
{
	auto bounds = m_sprite.getGlobalBounds();
	return { bounds.width, bounds.height };
}

bool Sprite::isTransparentPixel(const Vector2<unsigned int>& coordinates) const
{
	auto size = getSize();
	auto scale = getScale();
	if (coordinates.x >= size.width || coordinates.y >= size.height) return false;
	return m_sprite.getTexture()->copyToImage().getPixel(coordinates.x / scale.x, coordinates.y / scale.y).a == 0;
}

bool Sprite::isClicked(const Vector2<>& mousePosition) const
{
	auto position = getPosition();
	auto size = getSize();
	if (!(mousePosition.x > position.x && mousePosition.x < position.x + size.width &&
		mousePosition.y > position.y && mousePosition.y < position.y + size.height)) return false;

	return !isTransparentPixel({
		static_cast<unsigned int>(mousePosition.x - static_cast<int>(position.x)),
		static_cast<unsigned int>(mousePosition.y - static_cast<int>(position.y))
	});
}
