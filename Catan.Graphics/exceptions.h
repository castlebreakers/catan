#pragma once
#include "catan_graphics_exports.h"
#include <stdexcept>
#include <string_view>

namespace Exceptions
{	
	struct CATAN_GRAPHICS_EXPORTS SpriteNotFoundException :
		public std::runtime_error
	{
	public:
		SpriteNotFoundException(std::string_view spriteName);
	private:
		const std::string m_spriteName;
	};

	struct CATAN_GRAPHICS_EXPORTS AssetException :
		public std::runtime_error
	{
		AssetException(std::string_view message, std::string_view assetName, std::string_view assetTypeName);
		const std::string m_assetName;
		const std::string m_assetTypeName;
	};

	struct CATAN_GRAPHICS_EXPORTS AssetFileNotFoundException :
		public AssetException
	{
		AssetFileNotFoundException(std::string_view assetName, std::string_view assetTypeName);
	};

	struct CATAN_GRAPHICS_EXPORTS AssetNotLoadedException :
		public AssetException
	{
		AssetNotLoadedException(std::string_view assetName, std::string_view assetTypeName);
	};

	struct CATAN_GRAPHICS_EXPORTS WrongAssetTypeException :
		public AssetException
	{
		WrongAssetTypeException(std::string_view assetName, std::string_view assetTypeName);
	};
}