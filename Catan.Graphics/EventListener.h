#pragma once
#include "catan_graphics_exports.h"
#include <Catan.Graphics/Application.h>
#include <Catan.Utils/vector.hpp>

class CATAN_GRAPHICS_EXPORTS EventListener
{
public:
	using Keycode = sf::Keyboard::Key;
	using MouseWheelDirection = sf::Mouse::Wheel;
	using MouseButton = sf::Mouse::Button;
	using JoystickAxis = sf::Joystick::Axis;
	using SensorType = sf::Sensor::Type;

	inline virtual void onWindowClose() {};
	inline virtual void onWindowFocus() {};
	inline virtual void onWindowUnfocus() {};
	inline virtual void onWindowResize(const Size<>& newSize) {};
	inline virtual void onTextInput(uint32_t unicodeCharValue) {};
	inline virtual void onKeyPress(const Keycode& keyCode, bool isAltPressed, bool isControlPressed, bool isShiftPressed, bool isSystemPressed) {};
	inline virtual void onKeyRelease(const Keycode& keyCode, bool isAltPressed, bool isControlPressed, bool isShiftPressed, bool isSystemPressed) {};
	inline virtual void onMouseWheelScroll(const MouseWheelDirection& wheelDirection, float movingTicks, const Vector2<>& mousePosition) {};
	inline virtual void onMouseButtonPress(const MouseButton& mouseButton, const Vector2<>& mousePosition) {};
	inline virtual void onMouseButtonRelease(const MouseButton& mouseButton, const Vector2<>& mousePosition) {};
	inline virtual void onMouseMove(const Vector2<>& mousePosition) {};
	inline virtual void onMouseEnter() {};
	inline virtual void onMouseLeave() {};
	inline virtual void onJoystickButtonPress(uint8_t joystickIndex, uint8_t buttonIndex) {};
	inline virtual void onJoystickButtonRelease(uint8_t joystickIndex, uint8_t buttonIndex) {};
	inline virtual void onJoystickMove(uint8_t joystickIndex, const JoystickAxis& joystickAxis, float axisValue) {};
	inline virtual void onJoystickConnect(uint8_t joystickIndex) {};
	inline virtual void onJoystickDisconnect(uint8_t joystickIndex) {};
	inline virtual void onTouchBegin(uint8_t fingerIndex, const Vector2<>& touchPosition) {};
	inline virtual void onTouchMove(uint8_t fingerIndex, const Vector2<>& touchPosition) {};
	inline virtual void onTouchEnd(uint8_t fingerIndex, const Vector2<>& touchPosition) {};
	inline virtual void onSensorChange(const SensorType& sensorType, const Vector3<float>& sensorValues) {};
};