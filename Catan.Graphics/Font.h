#pragma once
#include "catan_graphics_exports.h"
#include <SFML/Graphics/Font.hpp>

class DrawableText;
class AssetLoader;
class CATAN_GRAPHICS_EXPORTS Font
{
	friend class DrawableText;
	friend class AssetLoader;
private:
	sf::Font m_sfObject;
};