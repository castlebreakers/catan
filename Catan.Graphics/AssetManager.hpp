#pragma once
#include "catan_graphics_exports.h"
#include <unordered_map>
#include <Catan.Graphics/Texture.h>
#include <Catan.Graphics/Font.h>
#include <Catan.Graphics/AssetLoader.h>
#include <variant>
#include <Catan.Graphics/exceptions.h>

class AssetManager
{
public:
	template <typename AssetType>
	void save(std::string_view assetId, const std::shared_ptr<AssetType>& asset);

	template <typename AssetType>
	std::shared_ptr<AssetType> get(std::string_view assetId) const;

	template <typename AssetType>
	void remove(std::string_view assetId);
private:
	std::unordered_map<std::string, std::variant<std::shared_ptr<Texture>, std::shared_ptr<Font>>> m_assets;
};

template<typename AssetType>
inline void AssetManager::save(std::string_view assetId, const std::shared_ptr<AssetType>& asset)
{
	m_assets[assetId.data()] = asset;
}

template<typename AssetType>
inline std::shared_ptr<AssetType> AssetManager::get(std::string_view assetId) const
{
	auto assetIterator = m_assets.find(assetId.data());
	if (assetIterator == m_assets.end())
	{
		throw Exceptions::AssetNotLoadedException(assetId, typeid(AssetType).name() + 6);
	}
	try 
	{
		return std::get<std::shared_ptr<AssetType>>(assetIterator->second);
	}
	catch (const std::bad_variant_access& exception)
	{
		throw Exceptions::WrongAssetTypeException(assetId, typeid(AssetType).name() + 6);
	}
}

template<typename AssetType>
inline void AssetManager::remove(std::string_view assetId)
{
	auto assetIterator = m_assets.find(assetId.data());
	if (assetIterator == m_assets.end())
	{
		throw Exceptions::AssetNotFoundException(assetId, typeid(AssetType).name() + 6);
	}
	m_assets.erase(assetIterator);
}
