#include "Drawable.h"

float Drawable::getZOrder() const
{
	return m_zOrder;
}

void Drawable::setZOrder(float zOrder)
{
	m_zOrder = zOrder;
}
