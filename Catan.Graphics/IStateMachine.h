#pragma once
#include <Catan.Graphics/IState.h>
#include <memory>

class IStateMachine
{
public:
	virtual ~IStateMachine() = default;

	virtual void push(std::shared_ptr<IState> state) = 0;

	virtual void pop() = 0;

	virtual const std::shared_ptr<IState> current() const noexcept = 0;
};
