#include "exceptions.h"

namespace Exceptions
{
	AssetException::AssetException(std::string_view message, std::string_view assetName, std::string_view assetTypeName) :
		std::runtime_error(std::string("Error: ") + message.data()),
		m_assetName(assetName),
		m_assetTypeName(assetTypeName)
	{
	}

	AssetFileNotFoundException::AssetFileNotFoundException(std::string_view assetName, std::string_view assetTypeName) :
		AssetException((std::string("Could not load ") + assetTypeName.data() + " asset file with path: " + assetName.data() + ".").c_str(), assetName, assetTypeName)
	{
	}

	AssetNotLoadedException::AssetNotLoadedException(std::string_view assetName, std::string_view assetTypeName) :
		AssetException((std::string(assetTypeName) + " with id: " + assetName.data() + " does not exist.").c_str(), assetName, assetTypeName)
	{
	}

	WrongAssetTypeException::WrongAssetTypeException(std::string_view assetName, std::string_view assetTypeName) :
		AssetException((std::string("Asset with id: ") + assetName.data() + "is not a " + assetTypeName.data() + ".").c_str(), assetName, assetTypeName)
	{
	}

	SpriteNotFoundException::SpriteNotFoundException(std::string_view spriteName) :
		std::runtime_error(std::string("Error: Sprite ") + spriteName.data() + " not found."),
		m_spriteName(spriteName)
	{
	}
}