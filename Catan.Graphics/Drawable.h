#pragma once
#include "catan_graphics_exports.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <Catan.Utils/vector.hpp>

class Renderer;
class CATAN_GRAPHICS_EXPORTS Drawable
{
	friend class Renderer;
public:
	virtual ~Drawable() = default;

	virtual Vector2<float> getPosition() const = 0;
	virtual void setPosition(const Vector2<float>& position) = 0;

	virtual float getZOrder() const;
	virtual void setZOrder(float zOrder);

	virtual bool isClicked(const Vector2<>& mousePosition) const = 0;
protected:
	virtual void requestDraw(sf::RenderWindow* window) const = 0;
private:
	float m_zOrder = 0;
};