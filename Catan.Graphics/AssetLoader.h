#pragma once
#include "catan_graphics_exports.h"
#include <Catan.Graphics/Font.h>
#include <Catan.Graphics/Texture.h>
#include <Catan.Graphics/exceptions.h>

class CATAN_GRAPHICS_EXPORTS AssetLoader
{
public:
	AssetLoader(std::string_view assetsFolder = ".");
	template <typename AssetType>
	std::shared_ptr<AssetType> loadFromFile(std::string_view fileName) const;
	std::string fileNameToPath(std::string_view fileName) const;
private:
	std::string m_assetsFolder;
};

template<typename AssetType>
inline std::shared_ptr<AssetType> AssetLoader::loadFromFile(std::string_view fileName) const
{
	auto asset = std::make_shared<AssetType>();
	if (!asset->m_sfObject.loadFromFile(fileNameToPath(fileName)))
		throw Exceptions::AssetFileNotFoundException(fileName, typeid(asset).name() + 6);
	return asset;
}
