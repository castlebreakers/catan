#include "DrawableHolder.h"

void DrawableHolder::add(const std::string& drawableName, const std::shared_ptr<Drawable> & drawable)
{
	m_drawablesMap[drawableName] = drawable;
}

void DrawableHolder::remove(const std::string& drawableName)
{
	auto sprite = get(drawableName);
	m_drawablesMap.erase(drawableName);
}

std::shared_ptr<Drawable> DrawableHolder::get(const std::string& drawableName)
{
	auto spriteIterator = m_drawablesMap.find(drawableName);
	if (spriteIterator == m_drawablesMap.end())
		throw Exceptions::SpriteNotFoundException(drawableName);
	return spriteIterator->second;
}

const std::vector<std::pair<std::string, std::shared_ptr<Drawable>>>& DrawableHolder::getAll()
{
	m_buffer.resize(m_drawablesMap.size());
	auto index = -1;
	for (const auto&[name, sprite] : m_drawablesMap)
		m_buffer[++index] = { name, sprite };
	std::sort(m_buffer.begin(), m_buffer.end(), [](const std::pair<std::string, std::shared_ptr<Drawable>> & firstElement, const std::pair<std::string, std::shared_ptr<Drawable>>& secondElement) {
		return firstElement.second->getZOrder() < secondElement.second->getZOrder();
	});
	return m_buffer;
}

void DrawableHolder::removeStartingWith(std::string_view drawableNamePrefix)
{
	for (auto iterator = std::begin(m_drawablesMap); iterator != std::end(m_drawablesMap);)
	{
		if (iterator->first.find(drawableNamePrefix) == 0)
			iterator = m_drawablesMap.erase(iterator);
		else
			++iterator;
	}
}

void DrawableHolder::clear()
{
	m_buffer.clear();
	m_drawablesMap.clear();
}
