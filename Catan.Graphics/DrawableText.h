#pragma once
#include "catan_graphics_exports.h"
#include <Catan.Graphics/Drawable.h>
#include <Catan.Graphics/Font.h>
#include <Catan.Utils/vector.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <Catan.Graphics/Color.h>

namespace GUI { class TextBox; class Button; class MessageBox;  }
class CATAN_GRAPHICS_EXPORTS DrawableText : public Drawable
{
	friend class GUI::TextBox;
	friend class GUI::Button;
	friend class GUI::MessageBox;
public:
	DrawableText(const std::shared_ptr<Font>& font, const Vector2<float>& position, std::string_view text = "", const Color& color = {}, uint32_t characterSize = 24);
	Color getColor() const;
	void setColor(const Color& color);
	void setText(std::string_view text);
	std::string getText() const;
	void setCharacterSize(uint32_t characterSize);
	uint32_t getCharacterSize() const;
	void setPosition(const Vector2<float>& newPosition) override;
	Vector2<float> getPosition() const override;
	bool isClicked(const Vector2<>& mousePosition) const override;
protected:
	void requestDraw(sf::RenderWindow* window) const override;
private:
	Color m_color;
	sf::Text m_text;
};