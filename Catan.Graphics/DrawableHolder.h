#pragma once
#include "catan_graphics_exports.h"
#include <unordered_map>
#include <set>
#include <memory>
#include <functional>
#include <Catan.Graphics/Drawable.h>
#include <Catan.Graphics/exceptions.h>
#include <Catan.Graphics/Renderer.h>

class CATAN_GRAPHICS_EXPORTS DrawableHolder
{
public:
	void add(const std::string& drawableName, const std::shared_ptr<Drawable>& drawable);
	void remove(const std::string& drawableName);
	std::shared_ptr<Drawable> get(const std::string& drawableName);
	const std::vector<std::pair<std::string, std::shared_ptr<Drawable>>>& getAll();
	void removeStartingWith(std::string_view drawableNamePrefix);
	void clear();
private:
	std::vector<std::pair<std::string, std::shared_ptr<Drawable>>> m_buffer;
	std::unordered_map<std::string, std::shared_ptr<Drawable>> m_drawablesMap;
};