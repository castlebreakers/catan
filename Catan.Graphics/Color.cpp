#include "Color.h"

constexpr Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) noexcept :
	r(r), g(g), b(b), a(a)
{}

bool Color::operator==(const Color & other) const noexcept
{
	return this->r == other.r && this->g == other.g && this->b == other.b && this->a == other.a;
}

bool Color::operator!=(const Color & other) const noexcept
{
	return !(*this == other);
}