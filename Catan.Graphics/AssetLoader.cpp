#include "AssetLoader.h"

AssetLoader::AssetLoader(std::string_view assetsFolder) :
	m_assetsFolder(assetsFolder)
{
}

std::string AssetLoader::fileNameToPath(std::string_view fileName) const
{
	return m_assetsFolder + "/" + fileName.data();
}
