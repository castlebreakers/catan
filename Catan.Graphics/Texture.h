#pragma once
#include "catan_graphics_exports.h"
#include <SFML/Graphics/Texture.hpp>

class CATAN_GRAPHICS_EXPORTS Texture
{
	friend class Sprite;
    friend class AssetLoader;
private:
    sf::Texture m_sfObject;
};