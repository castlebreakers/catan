#pragma once
#include "catan_graphics_exports.h"
#include <Catan.Graphics/Drawable.h>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <Catan.Utils/vector.hpp>
#include <Catan.Graphics/Texture.h>

class CATAN_GRAPHICS_EXPORTS Sprite : public Drawable
{
public:
	Sprite(const std::shared_ptr<Texture>& texture);

	Vector2<float> getScale() const;
	void setScale(const Vector2<float>& scale);
	
	Vector2<float> getPosition() const override;
	void setPosition(const Vector2<float>& position) override;

	Vector2<float> getOrigin() const;
	void setOrigin(const Vector2<float>& origin);

	float getRotation() const;
	void setRotation(float angle);

	void setTexture(const std::shared_ptr<Texture>& texture);

	Size<float> getSize() const;

	bool isTransparentPixel(const Vector2<unsigned int>& coordinates) const;

	bool isClicked(const Vector2<>& mousePosition) const override;
protected:
	void requestDraw(sf::RenderWindow* window) const override;

private:
	sf::Sprite m_sprite;
};