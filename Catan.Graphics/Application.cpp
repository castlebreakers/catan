#include "Application.h"
#include <Catan.Graphics/EventListener.h>

Application::Application(const Size<> & size, std::string_view windowName, const Color& backgroundColor, bool fullscreen) :
	m_window(sf::VideoMode(size.width, size.height), windowName.data(), fullscreen ? sf::Style::Fullscreen : (sf::Style::Titlebar | sf::Style::Close)),
	m_backgroundColor(backgroundColor)
{
	m_window.setKeyRepeatEnabled(false);
	m_window.setActive(false);
}

void Application::run()
{
	static sf::Clock updateClock, drawClock;
	m_window.setActive(true);
	m_window.setVerticalSyncEnabled(true);
	m_window.setFramerateLimit(kDesiredFPS);
	start();
	while (m_window.isOpen())
	{
		if (m_eventListener != nullptr)
			pollEvents();
		update(updateClock.getElapsedTime().asSeconds());
		updateClock.restart();
		m_window.clear({ m_backgroundColor.r, m_backgroundColor.g, m_backgroundColor.b, m_backgroundColor.a});
		draw(drawClock.getElapsedTime().asSeconds());
		drawClock.restart();
		m_window.display();
	}
}

void Application::setEventListener(const std::shared_ptr<EventListener>& eventListener)
{
	m_eventListener = eventListener;
}

Vector2<> Application::getMousePosition() const
{
	auto mousePositionVector = sf::Mouse::getPosition();
	return {mousePositionVector.x, mousePositionVector.y};
}

void Application::setMousePosition(const Vector2<> & point)
{
	sf::Mouse::setPosition({ point.x, point.y });
}

void Application::close()
{
	m_window.close();
}

void Application::handleEvent(const sf::Event & event)
{
	switch (event.type)
	{
	case sf::Event::EventType::Closed:
		m_eventListener->onWindowClose();
		m_window.close();
		break;
	case sf::Event::EventType::Resized:
		m_window.setView(sf::View({ 0.0f, 0.0f, static_cast<float>(event.size.width), static_cast<float>(event.size.height) }));
		m_eventListener->onWindowResize({ event.size.width, event.size.height });
		break;
	case sf::Event::EventType::LostFocus:
		m_eventListener->onWindowFocus();
		break;
	case sf::Event::EventType::GainedFocus:
		m_eventListener->onWindowUnfocus();
		break;
	case sf::Event::EventType::TextEntered:
		m_eventListener->onTextInput(event.text.unicode);
		break;
	case sf::Event::EventType::KeyPressed:
		m_eventListener->onKeyPress(event.key.code, event.key.alt, event.key.control, event.key.shift, event.key.system);
		break;
	case sf::Event::EventType::KeyReleased:
		m_eventListener->onKeyRelease(event.key.code, event.key.alt, event.key.control, event.key.shift, event.key.system);
		break;
	case sf::Event::EventType::MouseWheelScrolled:
		m_eventListener->onMouseWheelScroll(event.mouseWheelScroll.wheel, event.mouseWheelScroll.delta, { event.mouseWheelScroll.x, event.mouseWheelScroll.y });
		break;
	case sf::Event::EventType::MouseButtonPressed:
		m_eventListener->onMouseButtonPress(event.mouseButton.button, { event.mouseButton.x, event.mouseButton.y });
		break;
	case sf::Event::EventType::MouseButtonReleased:
		m_eventListener->onMouseButtonRelease(event.mouseButton.button, { event.mouseButton.x, event.mouseButton.y });
		break;
	case sf::Event::EventType::MouseMoved:
	{
		static auto lastMousePosition = Vector2<>(-1, -1);
		auto currentMousePosition = Vector2<>(event.mouseMove.x, event.mouseMove.y);
		if (currentMousePosition != lastMousePosition)
		{
			lastMousePosition = currentMousePosition;
			m_eventListener->onMouseMove(currentMousePosition);
		}
		break;
	}
	case sf::Event::EventType::MouseEntered:
		m_eventListener->onMouseEnter();
		break;
	case sf::Event::EventType::MouseLeft:
		m_eventListener->onMouseLeave();
		break;
	case sf::Event::EventType::JoystickButtonPressed:
		m_eventListener->onJoystickButtonPress(event.joystickButton.joystickId, event.joystickButton.button);
		break;
	case sf::Event::EventType::JoystickButtonReleased:
		m_eventListener->onJoystickButtonRelease(event.joystickButton.joystickId, event.joystickButton.button);
		break;
	case sf::Event::EventType::JoystickMoved:
		m_eventListener->onJoystickMove(event.joystickMove.joystickId, event.joystickMove.axis, event.joystickMove.position);
		break;
	case sf::Event::EventType::JoystickConnected:
		m_eventListener->onJoystickConnect(event.joystickConnect.joystickId);
		break;
	case sf::Event::EventType::JoystickDisconnected:
		m_eventListener->onJoystickDisconnect(event.joystickConnect.joystickId);
		break;
	case sf::Event::EventType::TouchBegan:
		m_eventListener->onTouchBegin(event.touch.finger, { event.touch.x, event.touch.y });
		break;
	case sf::Event::EventType::TouchMoved:
		m_eventListener->onTouchMove(event.touch.finger, { event.touch.x, event.touch.y });
		break;
	case sf::Event::EventType::TouchEnded:
		m_eventListener->onTouchEnd(event.touch.finger, { event.touch.x, event.touch.y });
		break;
	case sf::Event::EventType::SensorChanged:
		m_eventListener->onSensorChange(event.sensor.type, { event.sensor.x, event.sensor.y, event.sensor.z });
		break;
	}
}

void Application::pollEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		handleEvent(event);
	}
}

Size<> Application::getWindowSize() const
{
	auto sfSize = m_window.getSize();
	return { sfSize.x, sfSize.y };
}

void Application::setWindowSize(const Size<>& size)
{
	m_window.setSize({ size.width, size.height });
}

Size<> Application::getScreenResolution()
{
	auto screenResolution = sf::VideoMode::getDesktopMode();
	return { screenResolution.width, screenResolution.height };
}

void Application::setBackgroundColor(const Color & backgroundColor)
{
	m_backgroundColor = backgroundColor;
}
