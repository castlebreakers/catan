#pragma once
#include "../catan_graphics_exports.h"
#include <Catan.Graphics/Drawable.h>
#include <Catan.Graphics/DrawableText.h>
#include <Catan.Graphics/GUI/Button.h>

namespace GUI
{
	class CATAN_GRAPHICS_EXPORTS MessageBox : public Drawable
	{
	public:
		MessageBox(const Vector2<float>& position, const std::shared_ptr<Font>& font, std::string_view text = "");
		Vector2<float> getPosition() const override;
		void setPosition(const Vector2<float>& position) override;
		bool isClicked(const Vector2<>& mousePosition) const override;
		std::string getMessage() const;
		static const Size<> kSize;
	protected:
		void requestDraw(sf::RenderWindow* window) const override;
	private:
		static const Size<> kButtonSize;
		static const Vector2<float> kTextPosition;
		static const Vector2<float> kButtonPosition;
		static const Color kColor;
		static const Color kButtonColor;
		static const Color kTextColor;
		static const int kBorderThickness = 2;
		static const std::string kButtonText;
		Vector2<float> m_position;
		sf::RectangleShape m_rectangle;
		DrawableText m_drawableText;
		GUI::Button m_button;
	};
}