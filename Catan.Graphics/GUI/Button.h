#pragma once
#include "../catan_graphics_exports.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <Catan.Graphics/Drawable.h>
#include <Catan.Graphics/DrawableText.h>
#include <Catan.Graphics/Color.h>

namespace GUI
{
	class MessageBox;
	class CATAN_GRAPHICS_EXPORTS Button : public Drawable
	{
		friend class MessageBox;
	public:
		Button(
			const Vector2<float>& position, 
			const Size<>& size, 
			const std::shared_ptr<Font>& font, 
			std::string_view text = "", 
			const Color& backgroundColor = {}, 
			const Color& textColor = {}, 
			const Vector2<float>& offset = { 8, 8 }, 
			const Color& outlineColor = {},
			float outlineThickness = 1
		);
		Vector2<float> getPosition() const override;
		void setPosition(const Vector2<float>& position) override;
		virtual bool isClicked(const Vector2<>& mousePosition) const override;
		void setCharacterSize(uint32_t characterSize);
		uint32_t getCharacterSize() const;
		Size<> getSize() const noexcept;
		void setSize(const Size<>& size);
	protected:
		void requestDraw(sf::RenderWindow* window) const override;
	private:
		Size<> m_size;
		Vector2<float> m_position;
		Vector2<float> m_offset;
		sf::RectangleShape m_rectangle;
		DrawableText m_drawableText;
	};
}