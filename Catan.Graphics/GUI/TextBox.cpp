#include "TextBox.h"

GUI::TextBox::TextBox(const std::shared_ptr<Font>& font, const Vector2<float>& position, const Size<>& size, size_t length, Vector2<float> padding, std::string_view text) :
	m_drawableText(font, {}),
	m_length(length),
	m_padding(padding)
{
	m_box.setOutlineColor(sf::Color::Black);
	m_box.setOutlineThickness(1);
	setPosition(position);
	setSize(size);
	m_drawableText.setText(text);
}

void GUI::TextBox::input(char newCharacter)
{
	auto currentText = m_drawableText.getText();
	if (newCharacter == '\b')
	{
		if (currentText.length() > 0)
		{
			currentText.pop_back();
			setText(currentText);
		}
		return;
	}
	if (newCharacter < 32) return;
	if (currentText.length() >= m_length) return;
	m_drawableText.setText(m_drawableText.getText() + newCharacter);
}

void GUI::TextBox::setText(std::string_view text)
{
	if (text.length() > m_length)
		m_drawableText.setText(text.substr(0, m_length));
	else
		m_drawableText.setText(text);
}

std::string GUI::TextBox::getText() const
{
	return m_drawableText.getText();
}

Size<> GUI::TextBox::getSize() const noexcept
{
	return m_size;
}

void GUI::TextBox::setSize(const Size<>& size)
{
	m_size = size;
	m_box.setSize({ size.width + m_padding.x, size.height + m_padding.y });
}

Vector2<float> GUI::TextBox::getPosition() const
{
	return m_position;
}

void GUI::TextBox::setPosition(const Vector2<float>& position)
{
	m_position = position;
	m_box.setPosition({ position.x, position.y });
	m_drawableText.setPosition({ position.x + m_padding.x, position.y + m_padding.y });
}

void GUI::TextBox::focus()
{
	m_isFocused = true;
}

void GUI::TextBox::unfocus()
{
	m_isFocused = false;
}

bool GUI::TextBox::isFocused() const
{
	return m_isFocused;
}

bool GUI::TextBox::isClicked(const Vector2<>& mousePosition) const
{
	auto position = getPosition();
	auto size = getSize();
	size.width += m_padding.x * 2;
	size.height += m_padding.y * 2;
	return mousePosition.x > position.x && mousePosition.x < position.x + size.width &&
		mousePosition.y > position.y && mousePosition.y < position.y + size.height;
}

void GUI::TextBox::requestDraw(sf::RenderWindow * window) const
{
	window->draw(m_box);
	m_drawableText.requestDraw(window);
}