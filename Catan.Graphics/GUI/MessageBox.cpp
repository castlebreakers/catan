#include "MessageBox.h"

GUI::MessageBox::MessageBox(const Vector2<float>& position, const std::shared_ptr<Font>& font, std::string_view text) :
	m_drawableText(font, {}, text),
	m_button({}, kButtonSize, font, kButtonText, kButtonColor, kTextColor, { 2, -2 }),
	m_rectangle({ static_cast<float>(kSize.width), static_cast<float>(kSize.height) })
{
	m_rectangle.setOutlineThickness(kBorderThickness);
	m_rectangle.setOutlineColor(sf::Color::Black);
	m_rectangle.setFillColor({ kColor.r, kColor.g, kColor.b, kColor.a });
	m_drawableText.setColor(kTextColor);
	setPosition(position);
	setZOrder(INT_MAX);
}

Vector2<float> GUI::MessageBox::getPosition() const
{
	return m_position;
}

void GUI::MessageBox::setPosition(const Vector2<float>& position)
{
	m_position = position;
	m_rectangle.setPosition(position.x, position.y);
	m_drawableText.setPosition({ position.x + kTextPosition.x, position.y + kTextPosition.y });
	m_button.setPosition({ position.x + kButtonPosition.x, position.y + kButtonPosition.y });
}

bool GUI::MessageBox::isClicked(const Vector2<>& mousePosition) const
{
	return m_button.isClicked(mousePosition);
}

std::string GUI::MessageBox::getMessage() const
{
	return m_drawableText.getText();
}

void GUI::MessageBox::requestDraw(sf::RenderWindow * window) const
{
	window->draw(m_rectangle);
	m_drawableText.requestDraw(window);
	m_button.requestDraw(window);
}


const Size<> GUI::MessageBox::kSize = { 800, 200 };
const Size<> GUI::MessageBox::kButtonSize = { 40, 25 };
const Vector2<float> GUI::MessageBox::kTextPosition = { 20, 20 };
const Vector2<float> GUI::MessageBox::kButtonPosition = { 360, 150 };
const Color GUI::MessageBox::kColor = { 239, 228, 176 };
const Color GUI::MessageBox::kTextColor = { 0, 0, 0 };
const Color GUI::MessageBox::kButtonColor = { 181, 230, 29 };
const std::string GUI::MessageBox::kButtonText = "OK";