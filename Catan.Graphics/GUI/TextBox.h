#pragma once
#include "../catan_graphics_exports.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <Catan.Graphics/Drawable.h>
#include <Catan.Graphics/DrawableText.h>
#include <Catan.Graphics/IFocusable.h>
#include <Catan.Graphics/Font.h>
#include <Catan.Utils/vector.hpp>

namespace GUI
{
	class CATAN_GRAPHICS_EXPORTS TextBox : public Drawable, public IFocusable
	{
	public:
		TextBox(const std::shared_ptr<Font>& font, const Vector2<float>& position, const Size<>& size, size_t length, Vector2<float> padding = { 8, 8 }, std::string_view text = "");
		void input(char newCharacter);
		void setText(std::string_view text);
		std::string getText() const;
		Size<> getSize() const noexcept;
		void setSize(const Size<>& size);
		Vector2<float> getPosition() const override;
		void setPosition(const Vector2<float>& position) override;
		void focus() override;
		void unfocus() override;
		bool isFocused() const override;
		bool isClicked(const Vector2<>& mousePosition) const;
	protected:
		void requestDraw(sf::RenderWindow* window) const override;
	private:
		size_t m_length = 0;
		bool m_isFocused = false;
		Vector2<float> m_padding;
		Vector2<float> m_position;
		Size<> m_size;
		sf::RectangleShape m_box;
		DrawableText m_drawableText;
	};
}