#include "Button.h"

GUI::Button::Button(const Vector2<float>& position, const Size<>& size, const std::shared_ptr<Font>& font, std::string_view text, const Color& backgroundColor, const Color& textColor,
	const Vector2<float>& offset, const Color& outlineColor, float outlineThickness) :
	m_drawableText(font, {}),
	m_offset(offset)
{
	m_rectangle.setFillColor({ backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a });
	m_rectangle.setOutlineColor({ outlineColor.r, outlineColor.g, outlineColor.b, outlineColor.a });
	m_rectangle.setOutlineThickness(outlineThickness);
	m_drawableText.setColor(textColor);
	setPosition(position);
	setSize(size);
	m_drawableText.setText(text);
}

Vector2<float> GUI::Button::getPosition() const
{
	return m_position;
}

void GUI::Button::setPosition(const Vector2<float>& position)
{
	m_position = position;
	m_rectangle.setPosition({ position.x, position.y });
	m_drawableText.setPosition({ position.x + m_offset.x, position.y + m_offset.y });
}

bool GUI::Button::isClicked(const Vector2<>& mousePosition) const
{
	auto position = getPosition();
	auto size = getSize();
	return mousePosition.x > position.x && mousePosition.x < position.x + size.width &&
		mousePosition.y > position.y && mousePosition.y < position.y + size.height;
}

void GUI::Button::setCharacterSize(uint32_t characterSize)
{
	m_drawableText.setCharacterSize(characterSize);
}

uint32_t GUI::Button::getCharacterSize() const
{
	return m_drawableText.getCharacterSize();
}

Size<> GUI::Button::getSize() const noexcept
{
	return m_size;
}

void GUI::Button::setSize(const Size<>& size)
{
	m_size = size;
	m_rectangle.setSize(sf::Vector2f(size.width, size.height));
}

void GUI::Button::requestDraw(sf::RenderWindow * window) const
{
	window->draw(m_rectangle);
	m_drawableText.requestDraw(window);
}

