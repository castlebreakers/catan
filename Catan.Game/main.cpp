#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/CatanEventListener.h>

int main()
{
	static const std::unordered_map<std::string, std::string> defaultConfigValues {
		{ "assets_folder", "assets"  },
		{ "assets_remote_host", "catan.danybv.eu"},
		{ "assets_remote_filelist", "/assets/list.txt"},
		{ "assets_remote_directory", "/assets/files/"},
		{ "check_assets_size", "true"},
		{ "log_file", "game.log" }
	};
	auto applicationConfig = std::make_shared<FileConfiguration>("app.conf", defaultConfigValues.begin(), defaultConfigValues.end());
	auto application = CatanApplication::create({ 1600, 900 }, "Catan", applicationConfig, { 255, 255, 0 });
	auto eventListener = CatanEventListener::create(application);
	application->run();
	return 0;
}
