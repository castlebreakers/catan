#pragma once
#include <Catan.Graphics/Sprite.h>
#include <Catan.Core/HumanPlayer.h>

class CornerSprite :
	public Sprite
{
public:
	CornerSprite(const std::shared_ptr<Texture>& texture, HumanPlayer::CornerPosition& cornerPosition);
	HumanPlayer::CornerPosition getCornerPosition() const;
private:
	HumanPlayer::CornerPosition m_cornerPosition;
};
