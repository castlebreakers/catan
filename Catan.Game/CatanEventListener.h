#pragma once
#include <Catan.Game/CatanApplication.h>
#include <Catan.Graphics/EventListener.h>

class CatanEventListener :
	public EventListener, public std::enable_shared_from_this<CatanEventListener>
{
public:
	static std::shared_ptr<CatanEventListener> create(std::weak_ptr<CatanApplication> application);
	void onKeyPress(const Keycode& keyCode, bool isAltPressed, bool isControlPressed, bool isShiftPressed, bool isSystemPressed) override;
	void onTextInput(uint32_t unicodeCharValue);
	void onMouseButtonPress(const MouseButton& mouseButton, const Vector2<>& mousePosition) override;

protected:
	CatanEventListener(std::weak_ptr<CatanApplication> application);

private:
	std::shared_ptr<CatanApplication> getApplication() const;
	std::weak_ptr<CatanApplication> m_application;
};

