#include "CatanApplication.h"
#include <Catan.Game/TileSprite.h>
#include <Catan.Game/CornerSprite.h>
#include <Catan.Game/EdgeSprite.h>
#include <Catan.Game/DevelopmentCardSprite.h>
#include <Catan.Game/ResourceSprite.h>
#include <Catan.Game/States/states.h>
#include <Catan.Graphics/Sprite.h>
#include <Catan.Graphics/GUI/TextBox.h>
#include <Catan.Graphics/GUI/MessageBox.h>
#include <Catan.Patcher/Patcher.h>
#include <Catan.Utils/polymorphics.hpp>
#include <iostream>

std::shared_ptr<CatanApplication> CatanApplication::create(const Size<>& size, std::string_view windowName,
	const std::shared_ptr<FileConfiguration>& configurationFile, const Color& backgroundColor, bool fullscreen)
{
	return std::shared_ptr<CatanApplication>(new CatanApplication(size, windowName, configurationFile, backgroundColor, fullscreen));
}

void CatanApplication::push(std::shared_ptr<IState> state)
{
	if (!m_states.empty())
		m_states.top()->onExit();
	m_states.push(state);
	state->onEnter();
}

void CatanApplication::pop()
{
	m_states.top()->onExit();
	m_states.pop();
	if (!m_states.empty())
		m_states.top()->onEnter();
}

const std::shared_ptr<IState> CatanApplication::current() const noexcept
{
	if (m_states.empty()) return nullptr;
	return m_states.top();
}

void CatanApplication::start()
{
	downloadAssets();
	loadAssets();
	init();
}

void CatanApplication::update(float deltaTime)
{

}

void CatanApplication::draw(float deltaTime)
{
	for (const auto&[name, drawable] : m_drawableHolder->getAll())
		m_renderer->draw(drawable);
	if (m_messageBox != nullptr)
		m_renderer->draw(m_messageBox);
}

CatanApplication::CatanApplication(const Size<>& size, std::string_view windowName, const std::shared_ptr<FileConfiguration> &  configurationFile,
	const Color& backgroundColor, bool fullscreen) :
	Application(size, windowName, backgroundColor, fullscreen),
	m_applicationConfig(configurationFile)
{
	auto assetFolderPath = m_applicationConfig->get<std::string>("assets_folder");
	auto logFilePath = m_applicationConfig->get<std::string>("log_file");
	m_logFile.open(logFilePath);
	m_assetLoader = std::make_shared<AssetLoader>(assetFolderPath);
	m_assetManager = std::make_shared<AssetManager>();
	m_drawableHolder = std::make_shared<DrawableHolder>();
	//m_logger = std::make_shared<Logger>(m_logFile, Logger::Level::Debug, true);
	m_logger = std::make_shared<Logger>(std::cout, Logger::Level::Debug, true);
	m_logger->log("The application has been initialized.", Logger::Level::Debug);
}

void CatanApplication::init()
{
	const auto& hexSpecification = *m_hexSpecification;
	m_renderer = std::make_shared<Renderer>(this->shared_from_this());

	m_boardSize = Size<int>(
		BoardContainer::kBoardSize * (hexSpecification[Hex::Corner::UpperRight].x - hexSpecification[Hex::Corner::UpperLeft].x),
		BoardContainer::kBoardSize / 2 * (hexSpecification[Hex::Corner::Bottom].y - hexSpecification[Hex::Corner::Upper].y) + (BoardContainer::kBoardSize + 1) / 2 * (hexSpecification[Hex::Corner::Bottom].y - hexSpecification[Hex::Corner::UpperRight].y)
	);
	
	auto windowSize = getWindowSize();
	m_boardScreenOffset = Vector2<int>(
		(windowSize.width  - m_boardSize.width ) / 2,
		(windowSize.height - m_boardSize.height) / 2
	);
	this->push(std::make_shared<States::MainMenu>(*this));
}

void CatanApplication::downloadAssets()
{
	FileList fileList(
		m_applicationConfig->get<std::string>("assets_folder"), 
		m_applicationConfig->get<std::string>("assets_remote_host"),
		m_applicationConfig->get<std::string>("assets_remote_filelist"),
		m_applicationConfig->get<bool>("check_assets_size")
	);
	DownloadManager downloadManager(
		m_applicationConfig->get<std::string>("assets_remote_host"),
		m_applicationConfig->get<std::string>("assets_remote_directory")
	);
	Patcher patcher(
		downloadManager, 
		fileList, 
		m_applicationConfig->get<std::string>("assets_folder")
	);
	patcher.run();
}

void CatanApplication::loadAssets()
{
	m_hexSpecification = std::make_shared<HexSpecification>(FileConfiguration(m_assetLoader->fileNameToPath("hexes/hexes.conf")));
	m_assetManager->save("defaultFont", m_assetLoader->loadFromFile<Font>("fonts/chiller.ttf"));
	m_defaultFont = m_assetManager->get<Font>("defaultFont");
	m_assetManager->save("thief", m_assetLoader->loadFromFile<Texture>("helpers/thief.png"));
	m_assetManager->save("placeBuildingHelper", m_assetLoader->loadFromFile<Texture>("helpers/corner.png"));
	m_assetManager->save("settlementHint", m_assetLoader->loadFromFile<Texture>("hints/settlement.png"));
	m_assetManager->save("arial", m_assetLoader->loadFromFile<Font>("fonts/arial.ttf"));
	auto loadAssetsByNumber = [this](std::string_view idPrefix, std::string_view pathPrefix, int32_t lowerBound, int32_t upperBound) {
		for (int32_t index = lowerBound; index <= upperBound; ++index)
		{
			m_assetManager->save(std::string(idPrefix) + std::to_string(index),
				m_assetLoader->loadFromFile<Texture>(std::string(pathPrefix) + std::to_string(index) + ".png"));
		}
	};
	loadAssetsByNumber("dice_", "dices/", 1, 6);
	loadAssetsByNumber("number_", "numbers/", 2, 12);
}

void CatanApplication::messageBox(std::string_view message)
{
	const auto windowSize = getWindowSize();
	const auto messageBoxSize = GUI::MessageBox::kSize;
	auto position = Vector2<float>(
		(windowSize.width - messageBoxSize.width) / 2,
		(windowSize.height - messageBoxSize.height) / 2
		);
	m_messageBox = std::make_shared<GUI::MessageBox>(position, m_assetManager->get<Font>("arial"), message);
}

std::shared_ptr<GUI::Button> CatanApplication::createButton(std::string_view text, const Vector2<float>& position, const Size<>& size, int characterSize, const Vector2<float>& buttonOffset, std::shared_ptr<Font> font,
	const Color& textColor, const Color& backgroundColor, const Color& outlineColor)
{
	if (font == nullptr)
		font = m_defaultFont;

	auto button = std::make_shared<GUI::Button>(
		Vector2<float>(position.x, position.y),
		size,
		font,
		text,
		backgroundColor,
		textColor,
		buttonOffset,
		outlineColor
		);
	button->setCharacterSize(characterSize);
	return button;
}

void CatanApplication::addLowerButton(std::string_view id, std::string_view text,
	float positionX, uint32_t width, Color textColor, const Color& backgroundColor, const Color& outlineColor)
{
		m_drawableHolder->add(std::string(id) + "Button", createButton(text, Vector2<float>(positionX, kLowerButtonY), Size<>(width, kLowerButtonHeight), kLowerButtonCharacterSize, kLowerButtonTextOffset, m_defaultFont, textColor, backgroundColor, outlineColor));
}

void CatanApplication::showHint()
{
	if(instanceof<States::PlaceSettlement>(current()))
	{
		CornerPosition cornerPosition = m_hintProvider->settlementLocationAdvisor(isAtGameBeginning());
		auto[y, x, corner] = cornerPosition;
		if(corner != Hex::Corner::NotCorner)
		{
			addCornerSprite("corner_hint_", cornerPosition, m_assetManager->get<Texture>("settlementHint"), 3);
		}
		else
		{
			messageBox("No suitable location to place a settlement was found.");
		}
	}
	else if (instanceof<States::PlayerThinking>(current()))
	{
		messageBox(AI::HintProvider::actionTypeToMessage.at(m_hintProvider->requestAdvice()));
	}
}

Vector2<float> CatanApplication::hexBoardToScreenCoordinates(const Vector2<int>& boardCoordinates)
{
	const auto& hexSpecification = *m_hexSpecification;
	return Vector2<float>(
		m_boardScreenOffset.x + boardCoordinates.x * (hexSpecification[Hex::Corner::UpperRight].x - hexSpecification[Hex::Corner::UpperLeft].x) -
		(BoardContainer::kBoardSize / 2 - boardCoordinates.y) * (hexSpecification[Hex::Corner::Upper].x -
			hexSpecification[Hex::Corner::UpperLeft].x) - hexSpecification[Hex::Corner::UpperLeft].x,
		m_boardScreenOffset.y + boardCoordinates.y * (hexSpecification[Hex::Corner::Bottom].y - hexSpecification[Hex::Corner::UpperRight].y)
	);
}

void CatanApplication::focusElement(std::shared_ptr<IFocusable> element)
{
	if (m_focusedElement)
		m_focusedElement->unfocus();
	element->focus();
	m_focusedElement = element;
}

void CatanApplication::unfocusCurrentElement()
{
	if (m_focusedElement)
		m_focusedElement->unfocus();
	m_focusedElement.reset();
}

void CatanApplication::nextPlayer()
{
	if (!m_isFirstTurn)
		m_game->nextPlayer();
	if (m_isFirstTurn)
		m_isFirstTurn = false;
	auto diceRoll = m_game->getCurrentPlayer()->requestDiceInput();
	m_firstDice = diceRoll.first;
	m_secondDice = diceRoll.second;
	m_game->receiveResource(diceRoll);
	if (m_game->hasWinner())
		this->push(std::make_shared<States::GameEnd>(*this));
	if (!isAtGameBeginning() && m_firstDice + m_secondDice == 7)
		this->push(std::make_shared<States::PlaceThief>(*this));
}

void CatanApplication::redrawPlayerUI()
{
	if (isInNonActualGameState()) return;
	if (instanceof<States::GameEnd>(current())) return;
	auto playerTurnText = std::dynamic_pointer_cast<DrawableText>(m_drawableHolder->get("playerTurnText"));
	playerTurnText->setText("It's " + m_game->getCurrentPlayer()->getName() + "'s turn.");
	playerTurnText->setColor(pieceColorToColor.at(m_game->getCurrentPlayer()->getColor()));
	if (!isAtGameBeginning())
	{
		auto changeDiceTexture = [this](const std::string& diceId, int diceValue) {
			std::dynamic_pointer_cast<Sprite>(m_drawableHolder->get(diceId))->setTexture(m_assetManager->get<Texture>(std::string("dice_") + std::to_string(diceValue)));
		};
		auto& player = m_game->getCurrentPlayer();
		changeDiceTexture("firstDice", m_firstDice);
		changeDiceTexture("secondDice", m_secondDice);
		auto playerHand = player->hand.getResources();
		for (const auto&[type, id] : resourceTypeToResourceAssetString)
		{
			int number = 0;
			auto resourceIterator = playerHand.find(type);
			if (resourceIterator != playerHand.end())
				number = resourceIterator->second;
			std::dynamic_pointer_cast<DrawableText>(m_drawableHolder->get(std::string("resourceCounter_") + id))->setText(std::to_string(number));
		}
		auto playerTextPrefix = std::string("player_") + colorTypeToColorAssetString.at(player->getColor());
		std::dynamic_pointer_cast<DrawableText>(m_drawableHolder->get(playerTextPrefix + "_resourcesText"))->setText(std::to_string(player->hand.totalNumberOfResourceCards()));
		std::dynamic_pointer_cast<DrawableText>(m_drawableHolder->get(playerTextPrefix + "_developmentCardText"))->setText(std::to_string(player->hand.totalNumberOfDevelopmentCards()));
		std::dynamic_pointer_cast<DrawableText>(m_drawableHolder->get(playerTextPrefix + "_victoryPointText"))->setText(std::to_string(player->getNumberOfPoints()));

		redrawDevelopmentCards();
	}
}

void CatanApplication::placeSettlement(const HumanPlayer::CornerPosition & cornerPosition)
{
	m_game->getCurrentPlayer()->placeSettlement(*m_game->board, cornerPosition, isAtGameBeginning());
	auto textureId = std::string("settlement_") + colorTypeToColorAssetString.at(m_game->getCurrentPlayer()->getColor());
	addCornerSprite("settlement_", cornerPosition, m_assetManager->get<Texture>(textureId), 1);
	if (isAtGameBeginning())
	{
		if (m_game->getTurnNumber() == kFirstNormalTurnNumber - 1)
			m_game->receiveResource(cornerPosition);
		//this->pop();
		this->push(std::make_shared<States::PlaceRoad>(*this));
	}
	else
	{
		this->popUntilFound<States::PlayerThinking>();
	}
}

void CatanApplication::placeCity(const HumanPlayer::CornerPosition & cornerPosition)
{
	m_game->getCurrentPlayer()->placeCity(*m_game->board, cornerPosition);
	auto textureId = std::string("city_") + colorTypeToColorAssetString.at(m_game->getCurrentPlayer()->getColor());
	addCornerSprite("city_", cornerPosition, m_assetManager->get<Texture>(textureId), 2);
	this->pop();
}

void CatanApplication::placeRoad(const HumanPlayer::EdgePosition& edgePosition)
{
	auto[y, x, edge] = edgePosition;
	m_game->getCurrentPlayer()->placeRoad(*m_game->board, edgePosition, isAtGameBeginning());
	auto textureId = std::string("road_") + colorTypeToColorAssetString.at(m_game->getCurrentPlayer()->getColor()) + "_" + Hex::Data::Edges::edgeToString.at(edge);
	addEdgeSprite("road_", edgePosition, m_assetManager->get<Texture>(textureId));
	this->pop();
	if (isAtGameBeginning())
		this->pop();
}

void CatanApplication::moveThief(const Vector2<int>& boardCoordinates)
{
	pop();
	m_drawableHolder->get("thief")->setPosition(hexBoardToScreenCoordinates(boardCoordinates));
	if (m_usingSoldierCard)
	{
		m_game->getCurrentPlayer()->playDevelopmentCard(boardCoordinates.x, boardCoordinates.y, *m_game->board, *m_game->thief, *m_game->players);
		m_usingSoldierCard = false;
	} 
	else
	{
		m_game->thief->move(boardCoordinates.x, boardCoordinates.y, *m_game->board);
	}
}

void CatanApplication::buyDevelopmentCard()
{
	resourceCheck<DevelopmentCard>();
	m_game->getCurrentPlayer()->buy(m_game->bank->drawCard());
	redrawDevelopmentCards();
}

void CatanApplication::redrawDevelopmentCards()
{
	m_drawableHolder->removeStartingWith("card_");
	int columns = 5;
	Vector2<float> devCardTableOffset = { 1240, 200 };
	Vector2<int> spacing = { 4, 4 };
	auto developmentCardSize = std::make_shared<Sprite>(m_assetManager->get<Texture>("card_monopoly"))->getSize();
	auto& developmentCards = m_game->getCurrentPlayer()->hand.getDevelopmentCards();
	for (auto index = 0; index < developmentCards.size(); ++index)
	{
		auto& card = developmentCards[index];
		Vector2<float> developmentCardPosition{
			devCardTableOffset.x + index % columns * (developmentCardSize.width + spacing.x),
			devCardTableOffset.y + index / columns * (developmentCardSize.height + spacing.y),
		};
		auto sprite = std::make_shared<DevelopmentCardSprite>(m_assetManager->get<Texture>("card_" + developmentCardTypeToAssetString.at(card.getType())), card);
		sprite->setPosition(developmentCardPosition);
		m_drawableHolder->add("card_" + std::to_string(index), sprite);
		if (card.isApplied())
		{
			auto usedSprite = std::make_shared<Sprite>(m_assetManager->get<Texture>("developmentCardUsed"));
			usedSprite->setPosition(developmentCardPosition);
			usedSprite->setZOrder(15);
			m_drawableHolder->add("card_used_" + std::to_string(index), usedSprite);
		}
	}
}

void CatanApplication::playDevelopmentCard(DevelopmentCard & developmentCard)
{
	auto player = m_game->getCurrentPlayer();
	switch (developmentCard.getType())
	{
	case DevelopmentCard::Type::Monopoly:
	{
		this->push(std::make_shared<States::PlayMonopolyCard>(*this));
		break;
	}
	case DevelopmentCard::Type::RoadBuilding:
	{
		break;
	}
	case DevelopmentCard::Type::Soldier:
	{
		m_usingSoldierCard = true;
		this->push(std::make_shared<States::PlaceThief>(*this));
		break;
	}
	case DevelopmentCard::Type::VictoryPoint:
	{
		player->playDevelopmentCard();
		break;
	}
	case DevelopmentCard::Type::YearOfPlenty:
	{

		break;
	}
	}
	redrawDevelopmentCards();
}

bool CatanApplication::isAtGameBeginning() const
{
	return m_game->getTurnNumber() < kFirstNormalTurnNumber;
}

bool CatanApplication::isInNonActualGameState() const
{
	auto state = current();
	return instanceof<States::MainMenu>(state) || instanceof<States::GameConfiguration>(state) || instanceof<States::PlayerDetails>(state) || instanceof<States::GameEnd>(state) ||
		instanceof<States::DrawBase>(state) || instanceof<States::ZeroDecision>(state) || instanceof<States::StartedGame>(state);
}

void CatanApplication::addCornerSprite(std::string_view idPrefix, const CornerPosition& cornerPosition, std::shared_ptr<Texture> cornerTexture, int zOrderModifier)
{
	const auto& hexSpecification = *m_hexSpecification;
	auto[y, x, corner] = cornerPosition;
	auto spriteId = std::string(idPrefix) + std::to_string(x) + "_" + std::to_string(y) + "_" + std::to_string(static_cast<int>(corner));
	auto sprite = std::make_shared<CornerSprite>(cornerTexture, HumanPlayer::CornerPosition(cornerPosition));
	auto tilePosition = hexBoardToScreenCoordinates({ x, y });
	sprite->setPosition({
		tilePosition.x + hexSpecification[corner].x - sprite->getSize().width / 2,
		tilePosition.y + hexSpecification[corner].y - sprite->getSize().height / 2,
	});
	sprite->setZOrder(kCornerZOrder + zOrderModifier);
	m_drawableHolder->add(spriteId, sprite);
}

void CatanApplication::addEdgeSprite(std::string_view idPrefix, const HumanPlayer::EdgePosition& edgePosition, std::shared_ptr<Texture> edgeTexture, int zOrderModifier)
{
	const auto& hexSpecification = *m_hexSpecification;
	auto[y, x, edge] = edgePosition;
	auto spriteId = std::string(idPrefix) + std::to_string(x) + "_" + std::to_string(y) + "_" + std::to_string(static_cast<int>(edge));
	auto sprite = std::make_shared<EdgeSprite>(edgeTexture, HumanPlayer::EdgePosition(edgePosition));
	auto tilePosition = hexBoardToScreenCoordinates({ x, y });
	sprite->setPosition(tilePosition);
	sprite->setZOrder(kEdgeZOrder + zOrderModifier);
	m_drawableHolder->add(spriteId, sprite);
}

void CatanApplication::initializeGame(const std::vector<std::shared_ptr<HumanPlayer>>& players)
{
	m_game = Game::create(players.begin(), std::next(players.begin(), players.size()));
	
	std::ifstream statisticsInputStream("statistics.txt");

	auto[x, y] = m_game->board->getDesertTile();

	auto thiefSprite = std::make_shared<Sprite>(m_assetManager->get<Texture>("thief"));
	thiefSprite->setPosition(hexBoardToScreenCoordinates({ y, x }));
	thiefSprite->setZOrder(16);
	m_drawableHolder->add("thief", thiefSprite);

	m_hintProvider = std::make_shared<AI::HintProvider>(m_game, statisticsInputStream);

	m_isGameRunning = true;
	this->push(std::make_shared<States::DrawBase>(*this));
}

const Color CatanApplication::kLowerButtonTextColor{ 192, 192, 192 };

const Color CatanApplication::kButtonBackgroundColor{ 69, 136, 247 };
const Color CatanApplication::kButtonOutlineColor{ 34, 107, 226 };
const Color CatanApplication::kButtonTextColor{};

const Color CatanApplication::kHintButtonBackgroundColor{ 255, 0, 0 };

const std::unordered_map<Resource::Type, std::string> CatanApplication::hexTypeToHexAssetString{
	{ Resource::Type::Nothing , "desert"    },
	{ Resource::Type::Grain   , "fields"    },
	{ Resource::Type::Lumber  , "forest"    },
	{ Resource::Type::Brick   , "hills"     },
	{ Resource::Type::Ore     , "mountains" },
	{ Resource::Type::Wool    , "pasture"   },
	{ Resource::Type::Water   , "sea"       },
};

const std::unordered_map<Resource::Type, std::string> CatanApplication::resourceTypeToResourceAssetString{
	{ Resource::Type::Grain   , "wheat" },
	{ Resource::Type::Lumber  , "wood"  },
	{ Resource::Type::Brick   , "brick" },
	{ Resource::Type::Ore     , "ore"	},
	{ Resource::Type::Wool    , "sheep"	}
};

const std::unordered_map<Harbor::Type, std::string> CatanApplication::harborTypeToHarborAssetString{
	{ Harbor::Type::Brick	, "brick"	},
	{ Harbor::Type::Generic	, "generic"	},
	{ Harbor::Type::Grain	, "grain"	},
	{ Harbor::Type::Lumber	, "lumber"	},
	{ Harbor::Type::Ore		, "ore"		},
	{ Harbor::Type::Wool	, "sheep"	}
};

const std::unordered_map<Piece::Color, std::string> CatanApplication::colorTypeToColorAssetString{
	{ Piece::Color::Blue  , "blue"   },
	{ Piece::Color::Orange, "orange" },
	{ Piece::Color::Red   , "red"    },
	{ Piece::Color::White , "white"  },
};

const std::unordered_map<DevelopmentCard::Type, std::string> CatanApplication::developmentCardTypeToAssetString {
	{ DevelopmentCard::Type::Monopoly    , "monopoly"      },
    { DevelopmentCard::Type::RoadBuilding, "road_building" },
    { DevelopmentCard::Type::Soldier     , "knight"        },
    { DevelopmentCard::Type::VictoryPoint, "victory_point" },
    { DevelopmentCard::Type::YearOfPlenty, "year_of_plenty"}
};

const std::unordered_map<Piece::Color, Color> CatanApplication::pieceColorToColor{
	{ Piece::Color::Blue  ,	{   0,   0, 255 }},
	{ Piece::Color::Orange, { 255, 165,   0 }},
	{ Piece::Color::Red	  , { 255,   0,   0 }},
	{ Piece::Color::White , { 127, 127, 127 }}
};
