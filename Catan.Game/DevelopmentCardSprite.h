#pragma once
#include <Catan.Graphics/Sprite.h>
#include <Catan.Core/DevelopmentCard.h>

class DevelopmentCardSprite :
	public Sprite
{
public:
	DevelopmentCardSprite(const std::shared_ptr<Texture>& texture, DevelopmentCard& developmentCard);
	DevelopmentCard& getDevelopmentCard();
private:
	DevelopmentCard& m_developmentCard;
};
