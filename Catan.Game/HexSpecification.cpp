#include "HexSpecification.h"

HexSpecification::HexSpecification(const FileConfiguration & fileConfiguration) :
	m_hexTextureCornerCoordinates({
		{ Hex::Corner::Upper	  , fileConfiguration.get<Vector2<int>>("corner_upper")		   },
		{ Hex::Corner::UpperRight , fileConfiguration.get<Vector2<int>>("corner_upper_right")  },
		{ Hex::Corner::BottomRight, fileConfiguration.get<Vector2<int>>("corner_bottom_right") },
		{ Hex::Corner::Bottom	  , fileConfiguration.get<Vector2<int>>("corner_bottom")       },
		{ Hex::Corner::BottomLeft , fileConfiguration.get<Vector2<int>>("corner_bottom_left")  },
		{ Hex::Corner::UpperLeft  , fileConfiguration.get<Vector2<int>>("corner_upper_left")   },
	})
{
}

const Vector2<int>& HexSpecification::operator[](Hex::Corner corner) const
{
	return m_hexTextureCornerCoordinates.at(corner);
}

std::istream & operator>>(std::istream & inputStream, Vector2<int>& vector)
{
	inputStream >> vector.x >> vector.y;
	return inputStream;
}
