#include "EdgeSprite.h"

EdgeSprite::EdgeSprite(const std::shared_ptr<Texture>& texture, HumanPlayer::EdgePosition& edgePosition) :
	Sprite(texture),
	m_edgePosition(edgePosition)
{
}

HumanPlayer::EdgePosition EdgeSprite::getEdgePosition() const
{
	return m_edgePosition;
}
