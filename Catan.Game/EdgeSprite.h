#pragma once
#include <Catan.Graphics/Sprite.h>
#include <Catan.Core/HumanPlayer.h>

class EdgeSprite :
	public Sprite
{
public:
	EdgeSprite(const std::shared_ptr<Texture>& texture, HumanPlayer::EdgePosition& edgePosition);
	HumanPlayer::EdgePosition getEdgePosition() const;
private:
	HumanPlayer::EdgePosition m_edgePosition;
};
