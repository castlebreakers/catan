#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class PlaceSettlement : public CatanState
	{
	public:
		explicit PlaceSettlement(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}

