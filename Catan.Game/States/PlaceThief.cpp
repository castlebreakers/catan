#include "PlaceThief.h"
#include <Catan.Game/CatanApplication.h>

States::PlaceThief::PlaceThief(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::PlaceThief::onEnter()
{
	application.messageBox("Click on the tile where you want to place the thief.");
}

void States::PlaceThief::onExit()
{

}
