#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class GameConfiguration : public CatanState
	{
	public:
		explicit GameConfiguration(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}