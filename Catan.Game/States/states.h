#pragma once
#ifndef STATES_LIST
#define STATES_LIST
	#include "DrawBase.h"
	#include "GameConfiguration.h"
	#include "GameEnd.h"
	#include "MainMenu.h"
	#include "PlaceCity.h"
	#include "PlaceRoad.h"
	#include "PlaceSettlement.h"
	#include "PlaceThief.h"
	#include "PlayerDetails.h"
	#include "PlayerThinking.h"
	#include "PlayMonopolyCard.h"
	#include "StartedGame.h"
	#include "ZeroDecision.h"
#endif