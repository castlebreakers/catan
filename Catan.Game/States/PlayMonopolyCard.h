#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class PlayMonopolyCard : public CatanState
	{
	public:
		explicit PlayMonopolyCard(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
