#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class DrawBase : public CatanState
	{
	public:
		explicit DrawBase(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
