#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class MainMenu : public CatanState
	{
	public:
		explicit MainMenu(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
