#pragma once 
#include <Catan.Game/CatanState.h> 

namespace States
{
	class ZeroDecision : public CatanState
	{
	public:
		explicit ZeroDecision(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
