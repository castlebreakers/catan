#include "StartedGame.h" 
#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/States/PlayerThinking.h>
#include <Catan.Game/ResourceSprite.h>

States::StartedGame::StartedGame(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}


void States::StartedGame::onEnter()
{
	if (!application.m_isGameRunning) return;
	// Start Dice things
	{
		application.m_drawableHolder->add("firstDice", std::make_shared<Sprite>(application.m_assetManager->get<Texture>("dice_1")));
		application.m_drawableHolder->add("secondDice", std::make_shared<Sprite>(application.m_assetManager->get<Texture>("dice_1")));
		float diceY = 785.f;
		application.m_drawableHolder->get("firstDice")->setPosition({ 737.f, diceY });
		application.m_drawableHolder->get("secondDice")->setPosition({ 804.f, diceY });
	}
	// End Dice things

	// Start Resource Indicators Things
	{
		application.loadAssetsFromMap(CatanApplication::resourceTypeToResourceAssetString, "resource_", "resources/");

		float resourceY = 4;
		float resourceStartingX = 4;
		float resourceSpaceX = 4;
		float resourceScale = 1.0f / 8.f;
		int counter = 0;

		for (auto&[resourceType, assetName] : CatanApplication::resourceTypeToResourceAssetString)
		{
			auto resourceIconId = std::string("resource_") + assetName;
			auto resourceCounterId = std::string("resourceCounter_") + assetName;
			auto sprite = std::make_shared<ResourceSprite>(application.m_assetManager->get<Texture>(resourceIconId), resourceType);
			auto resourceX = resourceStartingX + (resourceScale * sprite->getSize().width + resourceSpaceX) * counter++;
			sprite->setPosition({ resourceX, resourceY });
			sprite->setScale({ resourceScale, resourceScale });
			application.m_drawableHolder->add(resourceIconId, sprite);
			application.m_drawableHolder->add(resourceCounterId, std::make_shared<DrawableText>(application.m_defaultFont,
				Vector2<float>(resourceX + resourceStartingX, resourceY + sprite->getSize().height)));
		}
	}
	// End Resource Indicators Things

	// Start Lower Bar Button Things
	{
		const int kCharacterSize = 24;
		auto arialFont = application.m_assetManager->get<Font>("arial");

		application.addLowerButton("placeSettlement", "Build settlement", 0.f, 140);
		application.addLowerButton("placeCity", "Build city", 140.f, 100);
		application.addLowerButton("placeRoad", "Build road", 240.f, 100);
		application.addLowerButton("buyDevelopmentCard", "Buy development card", 340.f, 180);

		application.addLowerButton("endTurn", "End turn", 1520.f, 80);

	}
	// End Lower Bar Button Things

	// Start Player Table
	{
		float startingY = 8;
		float startingX = 1200;
		float rowHeight = 50;
		int counter = 0;
		for (const auto& player : *application.m_game->players)
		{
			float currentY = startingY + rowHeight * counter++;
			auto colorIcon = std::make_shared<Sprite>(application.m_assetManager->get<Texture>(std::string("settlement_") + CatanApplication::colorTypeToColorAssetString.at(player->getColor())));
			colorIcon->setPosition({ startingX, currentY + 4 });
			auto playerName = std::make_shared<DrawableText>(application.m_defaultFont, Vector2<float>{ startingX + colorIcon->getSize().width + 8, currentY }, player->getName());
			auto playerResourcesIcon = std::make_shared<Sprite>(application.m_assetManager->get<Texture>("resourcesIcon"));
			playerResourcesIcon->setPosition({ playerName->getPosition().x + 100, currentY });
			playerResourcesIcon->setScale({ 1.0 / 8.f, 1.0 / 8.f });
			auto playerResourcesText = std::make_shared<DrawableText>(application.m_defaultFont, Vector2<float>{ playerResourcesIcon->getPosition().x + playerResourcesIcon->getSize().width + 16, currentY }, "0");
			auto playerDevelopmentCardIcon = std::make_shared<Sprite>(application.m_assetManager->get<Texture>("developmentCardIcon"));
			playerDevelopmentCardIcon->setPosition({ playerResourcesText->getPosition().x + 40, currentY });
			auto playerDevelopmentCardText = std::make_shared<DrawableText>(application.m_defaultFont, Vector2<float>{ playerDevelopmentCardIcon->getPosition().x + playerDevelopmentCardIcon->getSize().width + 16, currentY }, "0");
			auto playerVictoryPointIcon = std::make_shared<Sprite>(application.m_assetManager->get<Texture>("victoryPointIcon"));
			playerVictoryPointIcon->setPosition({ playerDevelopmentCardText->getPosition().x + 40, currentY });
			auto playerVictoryPointText = std::make_shared<DrawableText>(application.m_defaultFont, Vector2<float>{  playerVictoryPointIcon->getPosition().x + playerVictoryPointIcon->getSize().width + 16, currentY }, "0");

			auto addToDrawableHolder = [this, &player](std::string_view id, const auto& item) {
				application.m_drawableHolder->add(std::string("player_") + application.colorTypeToColorAssetString.at(player->getColor()) + "_" + id.data(), item);
			};
			addToDrawableHolder("color", colorIcon);
			addToDrawableHolder("name", playerName);
			addToDrawableHolder("resourcesIcon", playerResourcesIcon);
			addToDrawableHolder("resourcesText", playerResourcesText);
			addToDrawableHolder("developmentCardIcon", playerDevelopmentCardIcon);
			addToDrawableHolder("developmentCardText", playerDevelopmentCardText);
			addToDrawableHolder("victoryPointIcon", playerVictoryPointIcon);
			addToDrawableHolder("victoryPointText", playerVictoryPointText);
		}
	}
	// End Player Table

	application.push(std::make_shared<States::PlayerThinking>(application));
}

void States::StartedGame::onExit()
{

}
