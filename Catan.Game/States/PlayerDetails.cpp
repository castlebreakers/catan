#include "PlayerDetails.h"
#include <Catan.Game/CatanApplication.h>

States::PlayerDetails::PlayerDetails(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

std::vector<std::shared_ptr<HumanPlayer>> States::PlayerDetails::getPlayers() const
{
	std::vector<std::shared_ptr<HumanPlayer>> players;
	static const auto colors = std::array
	{
		Piece::Color::Red,
		Piece::Color::Blue,
		Piece::Color::Orange,
		Piece::Color::White
	};
	for (int index = 0; index < application.m_playerCount; ++index)
	{
		auto textBox = std::dynamic_pointer_cast<GUI::TextBox>(application.m_drawableHolder->get("player_name_box_" + std::to_string(index)));
		players.push_back(std::make_shared<HumanPlayer>(textBox->getText(), colors[index]));
	}
	return players;
}

void States::PlayerDetails::onEnter()
{
	const int kWidth = 120;
	const int kHeight = 40;
	const int kHeightOffset = 10;
	const int kX = 970;
	const int kY = 345;
	const int kMaxLength = 15;
	for(int index = 0; index < application.m_playerCount; ++index)
	{
		m_textBoxes.push_back(
			std::make_shared<GUI::TextBox>(
				application.m_defaultFont, Vector2<float>(kX, kY + index * (kHeight + kHeightOffset)), Size<>(kWidth, kHeight), kMaxLength, Vector2<float>(8, 0)
			)
		);
		application.m_drawableHolder->add("player_name_box_" + std::to_string(index), m_textBoxes[m_textBoxes.size() - 1]);
	}
	application.m_drawableHolder->add("gameStartButton", application.createButton("Start", { 740, 450 }, { 90, 50 }, 50, { 10, -15 }));
}

void States::PlayerDetails::onExit()
{
	application.m_drawableHolder->removeStartingWith("player_name_box_");
	application.m_drawableHolder->remove("gameStartButton");
}