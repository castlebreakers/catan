#include "GameEnd.h"
#include <Catan.Game/CatanApplication.h>

States::GameEnd::GameEnd(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::GameEnd::onEnter()
{
	application.m_isGameRunning = false;
	application.m_drawableHolder->clear();
	auto windowSize = application.getWindowSize();
	auto winnerText = std::make_shared<DrawableText>(
		application.m_defaultFont,
		Vector2<float>(windowSize.width / 2 - 180, windowSize.height / 2 - 90),
		std::string("Winner: ") + application.m_game->getWinner()->getName() + "\nClick to restart...",
		Color(0, 0, 0),
		72
		);
	application.m_drawableHolder->add("winnerText", winnerText);
}

void States::GameEnd::onExit()
{
	//application.m_drawableHolder->remove("winnerText");
	application.m_drawableHolder->clear();
}
