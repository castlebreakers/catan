#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class PlaceRoad : public CatanState
	{
	public:
		explicit PlaceRoad(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}