#pragma once 
#include <Catan.Game/CatanState.h> 

namespace States
{
	class PlaceThief : public CatanState
	{
	public:
		explicit PlaceThief(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}