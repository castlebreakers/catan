#include <Catan.Game/CatanState.h>
#include <memory>
#include <Catan.Game/CatanApplication.h>
#include "MainMenu.h"

States::MainMenu::MainMenu(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}


void States::MainMenu::onEnter()
{
	auto windowSize = application.getWindowSize();
	auto logoText = std::make_shared<DrawableText>(application.m_defaultFont, Vector2<float>(0, application.kLogoY), "Catan");

	logoText->setCharacterSize(application.kLogoCharacterSize);
	logoText->setPosition(Vector2<float>((windowSize.width - application.kLogoCharacterSize * 1.8) / 2, application.kLogoY));

	application.m_drawableHolder->add("catanLogo", logoText);
	application.m_drawableHolder->add("playButton", application.createButton("Play", { 700, 500 }, application.kButtonSize, application.kMainCharacterSize, { 50, 0 }));
	application.m_drawableHolder->add("optionsButton", application.createButton("Options", { 700, 600 }, application.kButtonSize, application.kMainCharacterSize, { 20, 0 }));
	application.m_drawableHolder->add("exitButton", application.createButton("Exit", { 700, 700 }, application.kButtonSize, CatanApplication::kMainCharacterSize, { 50, 0 }));

	application.setBackgroundColor({ 255, 255, 255 });
}

void States::MainMenu::onExit()
{
	application.m_drawableHolder->remove("catanLogo");
	application.m_drawableHolder->remove("playButton");
	application.m_drawableHolder->remove("optionsButton");
	application.m_drawableHolder->remove("exitButton");
}
