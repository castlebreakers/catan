#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class PlaceCity : public CatanState
	{
	public:
		explicit PlaceCity(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}