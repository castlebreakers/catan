#pragma once 
#include <Catan.Game/CatanState.h> 

namespace States
{
	class PlayerThinking : public CatanState
	{
	public:
		explicit PlayerThinking(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
