#include "PlayMonopolyCard.h"
#include <Catan.Game/CatanApplication.h>

States::PlayMonopolyCard::PlayMonopolyCard(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::PlayMonopolyCard::onEnter()
{
	application.messageBox("Click on the left side of the screen\non the resource you want to get from all players.");
}

void States::PlayMonopolyCard::onExit()
{

}
