#include "PlaceRoad.h"
#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/States/PlayerThinking.h>
#include <Catan.Graphics/Texture.h>


States::PlaceRoad::PlaceRoad(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::PlaceRoad::onEnter()
{
	auto edges = application.m_game->getAvailableEdges();
	if (edges.empty())
	{
		application.messageBox("You cannot place a road\n anywhere right now.");
		application.pop();
	}
	for (const auto& edgePosition : edges)
	{
		auto[y, x, edge] = edgePosition;
		auto placeRoadTexture = application.m_assetManager->get<Texture>(std::string("placeRoadHelper_") + Hex::Data::Edges::edgeToString.at(edge));
		application.addEdgeSprite("edge_", edgePosition, placeRoadTexture);
	}
}

void States::PlaceRoad::onExit()
{
	application.m_drawableHolder->removeStartingWith("edge");
}