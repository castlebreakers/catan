#include "PlaceSettlement.h"
#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/States/PlayerThinking.h>
#include <Catan.Graphics/Texture.h>

States::PlaceSettlement::PlaceSettlement(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::PlaceSettlement::onEnter()
{
	auto cornerTexture = application.m_assetManager->get<Texture>("placeBuildingHelper");
	auto corners = application.m_game->getAvailableCornersSettlement(application.isAtGameBeginning());
	if (corners.empty())
	{
		application.messageBox("You cannot place a settlement\n anywhere right now.");
		application.pop();	
	}
	for (const auto& cornerPosition : corners)
	{
		application.addCornerSprite("corner_", cornerPosition, cornerTexture, 1);
	}
}

void States::PlaceSettlement::onExit()
{
	application.m_drawableHolder->removeStartingWith("corner");
}