#include "PlaceCity.h" 
#include <Catan.Game/CatanApplication.h>
#include <Catan.Graphics/Texture.h>

States::PlaceCity::PlaceCity(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::PlaceCity::onEnter()
{
	auto cornerTexture = application.m_assetManager->get<Texture>("placeBuildingHelper");
	auto corners = application.m_game->getAvailableCornersCity();
	if (corners.empty())
		application.messageBox("You cannot place a city\n anywhere right now.");
	for (const auto& cornerPosition : corners)
	{
		application.addCornerSprite("corner_", cornerPosition, cornerTexture, 2);
	}
}

void States::PlaceCity::onExit()
{
	application.m_drawableHolder->removeStartingWith("corner");
}