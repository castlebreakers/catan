#pragma once 
#include <Catan.Core/HumanPlayer.h>
#include <Catan.Game/CatanState.h> 
#include <Catan.Graphics/GUI/TextBox.h>

namespace States
{
	class PlayerDetails : public CatanState
	{
	public:
		explicit PlayerDetails(CatanApplication& catanApplication);
		std::vector<std::shared_ptr<HumanPlayer>> getPlayers() const;
		void onEnter() override;
		void onExit() override;
	private:
		std::vector<std::shared_ptr<GUI::TextBox>> m_textBoxes;
	};
}
