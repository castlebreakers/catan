#include "DrawBase.h"
#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/TileSprite.h>
#include <Catan.Game/States/ZeroDecision.h>

States::DrawBase::DrawBase(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::DrawBase::onEnter() 
{
	if (!application.m_isGameRunning) return;
	// Start Hex things
	{
		application.m_assetManager->save("developmentCardIcon", application.m_assetLoader->loadFromFile<Texture>("resources/development_card.png"));
		application.m_assetManager->save("resourcesIcon", application.m_assetLoader->loadFromFile<Texture>("resources/resources.png"));
		application.m_assetManager->save("victoryPointIcon", application.m_assetLoader->loadFromFile<Texture>("resources/victory_point.png"));
		application.m_assetManager->save("developmentCardUsed", application.m_assetLoader->loadFromFile<Texture>("development_cards/used.png"));
		application.loadAssetsFromMap(application.hexTypeToHexAssetString, "hex_", "hexes/");
		application.loadAssetsFromMap(application.harborTypeToHarborAssetString, "harbor_", "harbors/");
		application.loadAssetsFromMap(application.developmentCardTypeToAssetString, "card_", "development_cards/");
		application.loadAssetsFromMap(Hex::Data::Edges::edgeToString, "harbor_generic_", "harbors/generic_");
		application.loadAssetsFromMap(Hex::Data::Edges::edgeToString, "placeRoadHelper_", "helpers/roads/");

		for (const auto&[color, name] : CatanApplication::colorTypeToColorAssetString)
		{
			application.m_assetManager->save(std::string("settlement_") + name, application.m_assetLoader->loadFromFile<Texture>(std::string("buildings/settlement_") + name + ".png"));
			application.m_assetManager->save(std::string("city_") + name, application.m_assetLoader->loadFromFile<Texture>(std::string("buildings/city_") + name + ".png"));
			application.loadAssetsFromMap(Hex::Data::Edges::edgeToString, std::string("road_") + name + "_", std::string("buildings/roads_") + name + "/");
		}

		auto windowSize = application.getWindowSize();
		const auto& hexSpecification = *application.m_hexSpecification;

		auto addTileSprite = [this, &hexSpecification](auto& toAssetStringMap, auto tileObject, std::string_view idPrefix, std::optional<Tile>& tile, int colIndex, int rowIndex) -> std::shared_ptr<Sprite> {
			auto iterator = toAssetStringMap.find(tileObject.getType());
			if (iterator == std::end(toAssetStringMap))
				return nullptr;
			auto sprite = std::make_shared<TileSprite>(application.m_assetManager->get<Texture>(std::string(idPrefix) + iterator->second), *tile);
			sprite->setPosition(
				application.hexBoardToScreenCoordinates({ colIndex, rowIndex })
			);
			auto spriteIdentifier = std::string(idPrefix) + std::to_string(rowIndex) + "_" + std::to_string(colIndex);
			application.m_drawableHolder->add(spriteIdentifier, sprite);
			return std::dynamic_pointer_cast<TileSprite>(application.m_drawableHolder->get(spriteIdentifier));
		};

		for (int rowIndex = 0; rowIndex < BoardContainer::kBoardSize; ++rowIndex)
			for (int colIndex = 0; colIndex < BoardContainer::kBoardSize; ++colIndex)
			{
				auto& tile = application.m_game->board->getTile(rowIndex, colIndex);

				if (!tile) continue;

				if (tile->resource().getType() == Resource::Type::NotAType)
				{
					auto sprite = addTileSprite(application.harborTypeToHarborAssetString, tile->harbor(), "harbor_", tile, colIndex, rowIndex);
					if (tile->harbor().getType() != Harbor::Type::Generic) continue;
					auto pairCoordinates = BoardContainer::pair{ rowIndex, colIndex };
					auto textureId = std::string("harbor_generic_") +
						Hex::Data::Edges::edgeToString.at(BoardContainer::harborPositionToEdge.at(pairCoordinates));
					sprite->setTexture(application.m_assetManager->get<Texture>(textureId));
				}
				else
				{
					auto tileSprite = addTileSprite(application.hexTypeToHexAssetString, tile->resource(), "hex_", tile, colIndex, rowIndex);
					if (tile->resource().getType() == Resource::Type::Nothing || tile->resource().getType() == Resource::Type::Water) continue;
					auto tileNumber = tile->resource().getTileNumber();
					auto tileNumberTexture = application.m_assetManager->get<Texture>(std::string("number_") + std::to_string(tileNumber));
					auto tileNumberSprite = std::make_shared<Sprite>(tileNumberTexture);
					tileNumberSprite->setZOrder(5);
					tileNumberSprite->setScale({ 0.5f, 0.5f });
					auto tileNumberPosition = Vector2<float>{
						tileSprite->getPosition().x + (tileSprite->getSize().width - tileNumberSprite->getSize().width) / 2,
						tileSprite->getPosition().y + (tileSprite->getSize().height - tileNumberSprite->getSize().height) / 2
					};
					tileNumberSprite->setPosition(tileNumberPosition);
					application.m_drawableHolder->add(std::string("number_") + std::to_string(rowIndex) + "_" + std::to_string(colIndex), tileNumberSprite);
				}
			}
	}
	// End Hex things
	application.m_drawableHolder->add("playerTurnText", std::make_shared<DrawableText>(
		application.m_defaultFont, Vector2<float>(640, 0), "It's [Player Name]'s turn.", Color(255, 0, 0), 48)
	);
	application.addLowerButton("hint", "Hint", 1470.f, 50, application.kLowerButtonTextColor, application.kHintButtonBackgroundColor);
	application.push(std::make_shared<ZeroDecision>(application));
}

void States::DrawBase::onExit()
{ 
} 
