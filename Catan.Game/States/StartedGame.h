#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class StartedGame : public CatanState
	{
	public:
		explicit StartedGame(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
