#include "GameConfiguration.h" 
#include <Catan.Game/CatanApplication.h>
#include <Catan.Graphics/GUI/TextBox.h>

States::GameConfiguration::GameConfiguration(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}


void States::GameConfiguration::onEnter()
{
	static constexpr auto kCharacterSize = 64;
	auto playersCountText = std::make_shared<DrawableText>(application.m_defaultFont,
		Vector2<float>(580, 310),
		"Number of players: ");
	playersCountText->setCharacterSize(kCharacterSize);

	auto playersCountBox = std::make_shared<GUI::TextBox>(application.m_defaultFont, Vector2<float>(970, 345), Size<>(30, 30), 1, Vector2<float>(8, 0));

	application.m_drawableHolder->add("playersCountText", playersCountText);
	application.m_drawableHolder->add("playersCountBox", playersCountBox);
	application.m_drawableHolder->add("nextButton", application.createButton("Next", { 740, 450 }, { 90, 50 }, 50, { 10, -15 }));
}

void States::GameConfiguration::onExit()
{
	application.m_drawableHolder->remove("playersCountText");
	application.m_drawableHolder->remove("playersCountBox");
	application.m_drawableHolder->remove("nextButton");
}
