#pragma once 
#include <Catan.Game/CatanState.h>

namespace States
{
	class GameEnd : public CatanState
	{
	public:
		explicit GameEnd(CatanApplication& catanApplication);
		void onEnter() override;
		void onExit() override;
	};
}
