#include "ZeroDecision.h"
#include <Catan.Game/CatanApplication.h>
#include <Catan.Game/States/PlaceSettlement.h>
#include <Catan.Game/States/StartedGame.h>

States::ZeroDecision::ZeroDecision(CatanApplication& catanApplication)
	: CatanState(catanApplication)
{
}

void States::ZeroDecision::onEnter()
{
	if (!application.m_isGameRunning) return;
	application.nextPlayer();
	if (!application.isAtGameBeginning())
		application.push(std::make_shared<States::StartedGame>(application));
	else
		application.push(std::make_shared<States::PlaceSettlement>(application));
}

void States::ZeroDecision::onExit()
{

}
