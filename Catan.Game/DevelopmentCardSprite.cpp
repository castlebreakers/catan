#include "DevelopmentCardSprite.h"

DevelopmentCardSprite::DevelopmentCardSprite(const std::shared_ptr<Texture>& texture, DevelopmentCard& developmentCard) :
	Sprite(texture),
	m_developmentCard(developmentCard)
{
}

DevelopmentCard& DevelopmentCardSprite::getDevelopmentCard()
{
	return m_developmentCard;
}
