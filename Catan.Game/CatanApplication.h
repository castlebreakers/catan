#pragma once
#include <Catan.Graphics/Application.h>
#include <Catan.Graphics/AssetLoader.h>
#include <Catan.Graphics/AssetManager.hpp>
#include <Catan.Graphics/Renderer.h>
#include <Catan.Graphics/IFocusable.h>
#include <Catan.Graphics/DrawableHolder.h>
#include <Catan.Graphics/IStateMachine.h>
#include <Catan.Graphics/GUI/Button.h>
#include <Catan.Graphics/GUI/MessageBox.h>
#include <Catan.Utils/FileConfiguration.h>
#include <Catan.Utils/Logger.h>
#include <Catan.Core/Game.h>
#include <Catan.Core/Exceptions.h>
#include <Catan.Game/HexSpecification.h>
#include <Catan.AI/HintProvider.h>
#include <stack>

namespace States
{
	class DrawBase;
	class GameConfiguration;
	class GameEnd;
	class MainMenu;
	class PlaceCity;
	class PlaceRoad;
	class PlaceSettlement;
	class PlaceThief;
	class PlayerDetails;
	class PlayerThinking;
	class PlayMonopolyCard;
	class StartedGame;
	class ZeroDecision;

}

class CatanApplication :
	public Application,
	public IStateMachine,
	public std::enable_shared_from_this<CatanApplication>
{
	friend class CatanEventListener;

public:
	friend class States::DrawBase;
	friend class States::GameConfiguration;
	friend class States::GameEnd;
	friend class States::MainMenu;
	friend class States::PlaceCity;
	friend class States::PlaceRoad;
	friend class States::PlaceSettlement;
	friend class States::PlaceThief;
	friend class States::PlayerDetails;
	friend class States::PlayerThinking;
	friend class States::PlayMonopolyCard;
	friend class States::StartedGame;
	friend class States::ZeroDecision;

	static std::shared_ptr<CatanApplication> create(const Size<>& size, std::string_view windowName,
		const std::shared_ptr<FileConfiguration>& configurationFile, const Color& backgroundColor = { 255, 255, 255 }, bool fullscreen = false);


	void push(std::shared_ptr<IState> state) override;
	void pop() override;
	const std::shared_ptr<IState> current() const noexcept override;

protected:
	void start() override;
	void update(float deltaTime) override;
	void draw(float deltaTime) override;

private:
	CatanApplication(const Size<>& size, std::string_view windowName, const std::shared_ptr<FileConfiguration>& configurationFile, const Color& backgroundColor, bool fullscreen);
	
	void init();
	void initializeGame(const std::vector<std::shared_ptr<HumanPlayer>>& players);
	void downloadAssets();
	void loadAssets();

	template <typename MapType>
	void loadAssetsFromMap(MapType& toAssetStringMap, std::string_view idPrefix, std::string_view pathPrefix);

	template <typename T>
	void popUntilFound();

	void messageBox(std::string_view message);
	void focusElement(std::shared_ptr<IFocusable> element);
	void unfocusCurrentElement();
	void nextPlayer();
	void placeSettlement(const HumanPlayer::CornerPosition & cornerPosition);
	void placeCity(const HumanPlayer::CornerPosition & cornerPosition);
	void placeRoad(const HumanPlayer::EdgePosition& edgePosition);
	void moveThief(const Vector2<int>& boardCoordinates);
	void buyDevelopmentCard();

	void redrawDevelopmentCards();
	void playDevelopmentCard(DevelopmentCard& developmentCard);

	bool isAtGameBeginning() const;

	bool isInNonActualGameState() const;

	void addCornerSprite(std::string_view idPrefix, const CornerPosition& cornerPosition, std::shared_ptr<Texture> cornerTexture, int zOrderModifier = 0);
	void addEdgeSprite(std::string_view idPrefix, const HumanPlayer::EdgePosition& edgePosition, std::shared_ptr<Texture> edgeTexture, int zOrderModifier = 0);

	void redrawPlayerUI();

	template <typename T>
	void resourceCheck() const;

	std::shared_ptr<GUI::Button> createButton(std::string_view text, const Vector2<float>& position, const Size<>& size, int characterSize, const Vector2<float>& buttonOffset = { 40, 0 }, std::shared_ptr<Font> font = nullptr,
		const Color& textColor = kButtonTextColor, const Color& backgroundColor = kButtonBackgroundColor, const Color& outlineColor = kButtonOutlineColor);

	void addLowerButton(std::string_view id, std::string_view text, float positionX, uint32_t width, Color textColor = kLowerButtonTextColor,
		const Color& backgroundColor = kButtonBackgroundColor, const Color& outlineColor = kButtonOutlineColor);

	void showHint();

	Vector2<float> hexBoardToScreenCoordinates(const Vector2<int>& boardCoordinates);
private:
	std::shared_ptr<AssetLoader> m_assetLoader;
	std::shared_ptr<AssetManager> m_assetManager;
	std::shared_ptr<DrawableHolder> m_drawableHolder;
	std::shared_ptr<Renderer> m_renderer;
	std::shared_ptr<FileConfiguration> m_applicationConfig;
	std::shared_ptr<IFocusable> m_focusedElement = nullptr;
	std::shared_ptr<Font> m_defaultFont;

	std::shared_ptr<Logger> m_logger;
	std::ofstream m_logFile;

	std::shared_ptr<GUI::MessageBox> m_messageBox;

	std::shared_ptr<Game> m_game;
	std::shared_ptr<AI::HintProvider> m_hintProvider;

	std::shared_ptr<HexSpecification> m_hexSpecification;

	std::stack<std::shared_ptr<IState>> m_states;

	Vector2<int> m_boardScreenOffset;

	Size<int> m_boardSize;

	bool m_isFirstTurn = true;
	bool m_isGameRunning = false;

	bool m_usingSoldierCard = false;

	static constexpr int kFirstNormalTurnNumber{ 3 };
private:
	int m_firstDice = 1;
	int m_secondDice = 1;
	int m_playerCount = 0;

	static constexpr auto kEdgeZOrder = 9;
	static constexpr auto kCornerZOrder = 10;

	static constexpr auto kLogoCharacterSize = 408;
	static constexpr auto kLogoY = -50;

	static constexpr auto kLowerButtonY = 860;
	static constexpr auto kLowerButtonHeight = 40;

	static constexpr Size<> kButtonSize{ 200 , 100 };
	static constexpr int kMainCharacterSize{ 72 };
	static constexpr int kLowerButtonCharacterSize{ 24 };
	static constexpr Vector2<float> kLowerButtonTextOffset{ 10, 0 };
	static const Color kLowerButtonTextColor;

	static const Color kButtonBackgroundColor;
	static const Color kButtonOutlineColor;
	static const Color kButtonTextColor;

	static const Color kHintButtonBackgroundColor;

	static const std::unordered_map<Resource::Type, std::string> hexTypeToHexAssetString;
	static const std::unordered_map<Resource::Type, std::string> resourceTypeToResourceAssetString;
	static const std::unordered_map<Harbor::Type, std::string> harborTypeToHarborAssetString;
	static const std::unordered_map<DevelopmentCard::Type, std::string> developmentCardTypeToAssetString;
	static const std::unordered_map<Piece::Color, std::string> colorTypeToColorAssetString;
	static const std::unordered_map<Piece::Color, Color> pieceColorToColor;
};

template <typename MapType>
void CatanApplication::loadAssetsFromMap(MapType& toAssetStringMap, std::string_view idPrefix,
	std::string_view pathPrefix)
{
	for (const auto&[type, name] : toAssetStringMap)
	{
		m_assetManager->save(
			std::string(idPrefix) + name,
			m_assetLoader->loadFromFile<Texture>(std::string(pathPrefix) + name + ".png")
		);
	}
}

template <typename T>
void CatanApplication::popUntilFound()
{
	while (!m_states.empty() && !instanceof<T>(current()))
		pop();
}

template <typename T>
void CatanApplication::resourceCheck() const
{
	if(!isAtGameBeginning() && !m_game->getCurrentPlayer()->hasEnoughResources<T>())
		throw Exceptions::ResourceNotEnoughException("You don't have enough resources !");
}
