#pragma once
#include <Catan.Core/Hex.h>
#include <Catan.Utils/vector.hpp>
#include <Catan.Utils/FileConfiguration.h>

class HexSpecification
{
public:
	HexSpecification(const FileConfiguration& fileConfiguration);
	const Vector2<int>& operator[](Hex::Corner corner) const;
private:
	std::unordered_map<Hex::Corner, Vector2<int>> m_hexTextureCornerCoordinates;
};

std::istream& operator>>(std::istream& inputStream, Vector2<int>& vector);