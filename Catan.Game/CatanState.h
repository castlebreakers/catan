#pragma once
#include <Catan.Graphics/IState.h>

class CatanApplication;
class CatanState : public IState
{
public:
	CatanState(CatanApplication&);
protected:
	CatanApplication& application;
};