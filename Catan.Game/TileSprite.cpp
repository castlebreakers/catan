#include "TileSprite.h"

TileSprite::TileSprite(const std::shared_ptr<Texture>& texture, Tile& tile) :
	Sprite(texture),
	m_tile(tile)
{
}

Tile& TileSprite::getTile()
{
	return m_tile;
}
