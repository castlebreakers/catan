#include "CatanEventListener.h"
#include <Catan.Utils/polymorphics.hpp>
#include <Catan.Graphics/GUI/TextBox.h>
#include <Catan.Game/CornerSprite.h>
#include <Catan.Game/DevelopmentCardSprite.h>
#include <Catan.Game/ResourceSprite.h>
#include <Catan.Game/TileSprite.h>
#include <Catan.Game/EdgeSprite.h>
#include <Catan.Game/States/states.h>
#include <iostream>

std::shared_ptr<CatanEventListener> CatanEventListener::create(std::weak_ptr<CatanApplication> application)
{
	auto eventListener = std::shared_ptr<CatanEventListener>(new CatanEventListener(application));
	eventListener->getApplication()->setEventListener(eventListener);
	return eventListener;
}

void CatanEventListener::onKeyPress(const Keycode & keyCode, bool isAltPressed, bool isControlPressed, bool isShiftPressed, bool isSystemPressed)
{
}

void CatanEventListener::onTextInput(uint32_t unicodeCharValue)
{
	auto m_focusedElement = getApplication()->m_focusedElement;
	if (!instanceof<GUI::TextBox>(m_focusedElement)) return;

	char resultChar;
	if (unicodeCharValue > 127)
		resultChar = '?';
	else
		resultChar = unicodeCharValue;

	std::dynamic_pointer_cast<GUI::TextBox>(m_focusedElement)->input(resultChar);
}

void CatanEventListener::onMouseButtonPress(const MouseButton & mouseButton, const Vector2<>& mousePosition)
{
	auto& app = getApplication();
	if (instanceof<States::GameEnd>(app->current())) {
		app->popUntilFound<States::MainMenu>();
		return;
	}
	try {
		app->unfocusCurrentElement();
		auto drawables = app->m_drawableHolder->getAll();
		if (app->m_messageBox != nullptr && app->m_messageBox->isClicked(mousePosition))
		{
			app->m_messageBox = nullptr;
			return;
		}
		for (auto iterator = drawables.crbegin(); iterator != drawables.crend(); ++iterator)
		{
			if (iterator->first.find("hint") != iterator->first.npos && !instanceof<GUI::Button>(iterator->second)) continue;
			if (iterator->second->isClicked(mousePosition))
			{
				if (instanceof<GUI::Button>(iterator->second))
				{
					if (instanceof<States::MainMenu>(app->current()))
					{
						if (iterator->first == "playButton")
							app->push(std::make_shared<States::GameConfiguration>(*app));
						else if (iterator->first == "exitButton")
							app->close();

					}
					else if (instanceof<States::GameConfiguration>(app->current()))
					{
						if (iterator->first == "nextButton")
						{
							try {
								auto textBox = std::dynamic_pointer_cast<GUI::TextBox>(app->m_drawableHolder->get("playersCountBox"));
								auto playersCount = std::stoi(textBox->getText());
								if (playersCount < 2 || playersCount > 4) throw std::invalid_argument("");
								//app->initializeGame(playersCount);
								app->m_playerCount = playersCount;
								app->push(std::make_shared<States::PlayerDetails>(*app));
							}
							catch (const std::invalid_argument& exception)
							{
								app->messageBox("You have to input a number\n between 2 and 4 players.");
							}
						}
					}
					else if (instanceof<States::PlayerDetails>(app->current()))
					{
						auto state = std::dynamic_pointer_cast<States::PlayerDetails>(app->current());
						app->initializeGame(state->getPlayers());
					}
					if (iterator->first == "hintButton")
					{
						if (iterator->first == "hintButton")
							app->showHint();
					}
				}
				if (instanceof<States::PlaceThief>(app->current()) && instanceof<TileSprite>(iterator->second) && app->m_messageBox == nullptr)
				{
					auto tile = std::dynamic_pointer_cast<TileSprite>(iterator->second)->getTile();
					if (tile.resource().getType() != Resource::Type::NotAType && tile.resource().getType() != Resource::Type::Water)
					{
						auto[i, j] = app->m_game->board->getIndices(tile);
						app->moveThief({ j, i });
					}
				}
				if (instanceof<States::PlayerThinking>(app->current()) || instanceof<States::PlaceSettlement>(app->current()) ||
					instanceof<States::PlaceCity>(app->current()) || instanceof<States::PlaceRoad>(app->current()))
				{
					if (instanceof<DevelopmentCardSprite>(iterator->second))
					{
						app->m_logger->log("You clicked a development card.", Logger::Level::Debug);
						app->playDevelopmentCard(std::dynamic_pointer_cast<DevelopmentCardSprite>(iterator->second)->getDevelopmentCard());
					}
					else if (instanceof<GUI::Button>(iterator->second))
					{
						if (iterator->first == "endTurnButton")
						{
							if(!instanceof<States::GameEnd>(app->current()))
							{
								app->popUntilFound<States::PlayerThinking>();
							}
							app->nextPlayer();
							app->redrawPlayerUI();
						}
						else if (iterator->first == "placeSettlementButton")
						{
							app->resourceCheck<Settlement>();
							app->push(std::make_shared<States::PlaceSettlement>(*app));
						}
						else if (iterator->first == "placeCityButton")
						{
							app->resourceCheck<City>();
							app->push(std::make_shared<States::PlaceCity>(*app));
						}
						else if (iterator->first == "placeRoadButton")
						{
							app->resourceCheck<Road>();
							app->push(std::make_shared<States::PlaceRoad>(*app));
						}
						else if (iterator->first == "buyDevelopmentCardButton")
						{
							app->buyDevelopmentCard();
						}
					}
				}
				if (instanceof<States::PlaceSettlement>(app->current()) && instanceof<CornerSprite>(iterator->second))
				{
					auto cornerSprite = std::dynamic_pointer_cast<CornerSprite>(iterator->second);
					app->placeSettlement(cornerSprite->getCornerPosition());
					break;
				}
				if (instanceof<States::PlaceCity>(app->current()) && instanceof<CornerSprite>(iterator->second))
				{
					auto cornerSprite = std::dynamic_pointer_cast<CornerSprite>(iterator->second);
					app->placeCity(cornerSprite->getCornerPosition());
					break;
				}
				if (instanceof<States::PlaceRoad>(app->current()) && instanceof<EdgeSprite>(iterator->second))
				{
					auto edgeSprite = std::dynamic_pointer_cast<EdgeSprite>(iterator->second);
					app->placeRoad(edgeSprite->getEdgePosition());
					break;
				}
				if (instanceof<States::PlayMonopolyCard>(app->current()) && iterator->first.find("resource_") == 0)
				{
					app->m_game->getCurrentPlayer()->playDevelopmentCard(*app->m_game->players, std::dynamic_pointer_cast<ResourceSprite>(iterator->second)->getResourceType());
					app->popUntilFound<States::PlayerThinking>();
				}
				if (instanceof<GUI::TextBox>(iterator->second))
				{
					app->focusElement(std::dynamic_pointer_cast<GUI::TextBox>(iterator->second));
				}
			}
		}
	}
	catch (std::exception exception) {
		app->messageBox(exception.what());
	}
	app->redrawPlayerUI();
}

CatanEventListener::CatanEventListener(std::weak_ptr<CatanApplication> application) :
	m_application(application)
{
}

std::shared_ptr<CatanApplication> CatanEventListener::getApplication() const
{
	return m_application.lock();
}
