#include "CornerSprite.h"

CornerSprite::CornerSprite(const std::shared_ptr<Texture>& texture, HumanPlayer::CornerPosition& cornerPosition) :
	Sprite(texture),
	m_cornerPosition(cornerPosition)
{
}

HumanPlayer::CornerPosition CornerSprite::getCornerPosition() const
{
	return m_cornerPosition;
}
