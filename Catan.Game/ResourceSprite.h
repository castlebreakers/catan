#pragma once
#include <Catan.Graphics/Sprite.h>
#include <Catan.Core/Resource.h>

class ResourceSprite :
	public Sprite
{
public:
	ResourceSprite(const std::shared_ptr<Texture>& texture, Resource::Type resourceType);
	Resource::Type getResourceType() const;
private:
	Resource::Type m_resourceType;
};
