#pragma once
#include <Catan.Graphics/Sprite.h>
#include <Catan.Core/Tile.h>

class TileSprite :
	public Sprite
{
public:
	TileSprite(const std::shared_ptr<Texture>& texture, Tile& tile);
	Tile& getTile();
private:
	Tile& m_tile;
};
