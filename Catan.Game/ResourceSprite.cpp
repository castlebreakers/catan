#include "ResourceSprite.h"

ResourceSprite::ResourceSprite(const std::shared_ptr<Texture>& texture, Resource::Type resourceType) :
	Sprite(texture),
	m_resourceType(resourceType)
{
}

Resource::Type ResourceSprite::getResourceType() const
{
	return m_resourceType;
}
