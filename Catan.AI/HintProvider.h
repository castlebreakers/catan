#pragma once

#include <Catan.Core/PlayerAction.h>
#include <Catan.Core/HumanPlayer.h>
#include <Catan.Core/Game.h>

#include "catan_ai_exports.h"

#include <algorithm>
#include <vector>

namespace AI
{
	class CATAN_AI_EXPORTS HintProvider
	{
	public:		
		
		static const std::unordered_map<PlayerActionType, std::string_view> actionTypeToMessage;
		
		//where to place a settlement/city   //where to place a road
		//using  smth = std::variant<HumanPlayer::CornerPosition, HumanPlayer::EdgePosition , >;
	public:
		///under construction
		HintProvider(const std::shared_ptr<Game> game, std::ifstream& inputStream);

		void update(PlayerActionType type, uint16_t numberOfPoints) noexcept;

		PlayerActionType requestAdvice() noexcept;

		HumanPlayer::CornerPosition settlementLocationAdvisor(bool isFirstTurn = false) noexcept;

	private:
		std::optional<uint8_t> getBestPlaceLocation( const Tile& tile, const HumanPlayer::CornerPosition& position);
		std::vector<std::pair<PlayerActionType, uint16_t>> m_statistics;

		template <typename Container>
		static uint8_t countDifferentResourceTypes(Container container);

		static const std::unordered_map<uint8_t, float>  m_diceOdds;
		std::shared_ptr<Game> m_game;
	
	};

	template <typename Container>
	uint8_t HintProvider::countDifferentResourceTypes(Container container)
	{
		std::sort(container.begin(), container.end());
		auto last = std::unique(std::begin(container), std::end(container));
		container.erase(last, std::end(container));
		return container.size();
	}

}
