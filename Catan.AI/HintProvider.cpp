#include "HintProvider.h"

#include <Catan.Core/Hex.h>

#include <map>
#include <fstream>
#include <string>
#include <sstream>

namespace AI
{
	const std::unordered_map<PlayerActionType, std::string_view> HintProvider::actionTypeToMessage = 
	{
		{PlayerActionType::PlaceRoad, "You should try to place a road\n as soon as possible."},
		{PlayerActionType::PlaceSettlement, "You should try to place a settlement\n as soon as possible."},
		{PlayerActionType::PlaceCity, "You should try to place a city\n as soon as possible."},
		{PlayerActionType::PlayDevelopmentCard, "You should try to play a development card\n as soon as possible."},
		{PlayerActionType::BuyDevelopmentCard, "You should try to buy a development card\n as soon as possible."},
		{PlayerActionType::NotAnAction, "We could not find a suitable action at this moment."},
		{PlayerActionType::TradeWithBank, "You could try to trade with the bank.\n It has whatever you want."}
	};

	const std::unordered_map<uint8_t, float> HintProvider::m_diceOdds = {
		{2, 2.78},
		{3, 5.56},
		{4, 8.33},
		{5, 11.11},
		{6, 13.89},
		{7, 16.67},
		{8, 13.89},
		{9, 11.11},
		{10, 8.33},
		{11, 5.56},
		{12, 2.78}
	};

	HintProvider::HintProvider(const std::shared_ptr<Game> game, std::ifstream & inputStream) :
		m_game(game)
	{
		std::string currentLine;
		std::istringstream lineParser;

		while (std::getline(inputStream, currentLine))
		{
			uint8_t actionType = 0;
			uint16_t numberOfPoints = 0;

			lineParser.clear();
			lineParser.str(currentLine);

			lineParser >> std::skipws >> actionType >> numberOfPoints;

			m_statistics.emplace_back(std::make_pair(static_cast<PlayerActionType>(actionType - '0'), numberOfPoints));
		}
	}

	void HintProvider::update(PlayerActionType type, uint16_t numberOfPoints) noexcept
	{
		auto it = std::find_if(std::begin(m_statistics), std::end(m_statistics),
			[type](const auto& value)
		{
			return type == value.first;
		});

		m_statistics[std::distance(it, std::begin(m_statistics))].second += numberOfPoints;
	}


	PlayerActionType HintProvider::requestAdvice() noexcept
	{
		//Sorting the statistics vector depending on the number of points earned by each move.
		std::sort(std::begin(m_statistics), std::end(m_statistics),
			[](const auto& lhs, const auto& rhs)
		{
			return lhs.second > rhs.second;
		});

		for (auto[actionType, pointsCount] : m_statistics)
		{
			switch (actionType)
			{
			case PlayerActionType::PlaceRoad:
			{
				if (m_game->getCurrentPlayer()->hasEnoughResources<Road>() && (m_game->getCurrentPlayer()->hasAtLeastOneRoad()))
				{
					return PlayerActionType::PlaceRoad;
				}
				break;
			}

			case PlayerActionType::PlaceSettlement:
			{
				if (m_game->getCurrentPlayer()->hasEnoughResources<Settlement>() && m_game->getCurrentPlayer()->hasAtLeastOneSettlement())
				{
					return PlayerActionType::PlaceSettlement;
				}
				break;
			}
			case PlayerActionType::PlaceCity:
			{
				if (m_game->getCurrentPlayer()->hasEnoughResources<City>() && m_game->getCurrentPlayer()->hasAtLeastOneCity())
				{
					return PlayerActionType::PlaceCity;
				}
				break;
			}

			case PlayerActionType::PlayDevelopmentCard:
			{
				if(m_game->getCurrentPlayer()->hand.totalNumberOfUnusedDevelopmentCards())
					return PlayerActionType::PlayDevelopmentCard;
			}

			case PlayerActionType::BuyDevelopmentCard:
			{
				if (m_game->getCurrentPlayer()->hasEnoughResources<DevelopmentCard>() &&
					m_game->bank->remainingDevelopmentCards())
				{
					return PlayerActionType::BuyDevelopmentCard;
				}
				break;
			}

			case PlayerActionType::TradeWithBank:
			{
				return PlayerActionType::NotAnAction;
			}
			}
		}
		return PlayerActionType::NotAnAction;
	}

	std::optional<uint8_t> HintProvider::getBestPlaceLocation(const Tile& tile, const HumanPlayer::CornerPosition& position)
	{
		auto[row, column, corner] = position;

		Resource::Type firstResourceType;
		Resource::Type secondResourceType;

		auto result = HumanPlayer::doAdjacentIntersectionsContainNoPieces(*m_game->board, tile, position, firstResourceType, secondResourceType);

		if (!result)
			return std::nullopt;

		std::vector<Resource::Type> resourceTypes{
			firstResourceType,
			secondResourceType,
			tile.resource().getType()
		};

		return countDifferentResourceTypes(resourceTypes);
	}
	/* A good settlement spot is there where :
	-> the intersection has 3 different resources
	-> the tile number has at least one 6 or 8
	*/
	HumanPlayer::CornerPosition HintProvider::settlementLocationAdvisor( bool isFirstTurn) noexcept
	{
		auto player = m_game->getCurrentPlayer();
		auto board = m_game->board;
		const auto& boardContainer = board->getBoardContainer().getContainer();

		std::pair<HumanPlayer::CornerPosition, float> bestLocationToplace(HumanPlayer::CornerPosition(-1, -1, Hex::Corner::NotCorner), -1);

		auto sumOfAdjacentTiles = [&](std::optional<Tile> firstTile, std::optional<Tile> secondTile, int i, int j)
		{
			static constexpr uint8_t kDefaultDesertNumber = 0;
			uint16_t currentSumOfTiles = 0;

			if (firstTile->resource().getTileNumber() != kDefaultDesertNumber)
				currentSumOfTiles += m_diceOdds.at(firstTile->resource().getTileNumber());

			if(secondTile->resource().getTileNumber() != kDefaultDesertNumber)
				currentSumOfTiles += m_diceOdds.at(secondTile->resource().getTileNumber());

			if(board->getTile(i, j)->resource().getTileNumber() != kDefaultDesertNumber)
				currentSumOfTiles += m_diceOdds.at(board->getTile(i, j)->resource().getTileNumber());

			return currentSumOfTiles;
		};

		const auto hasEnoughSettlementResources = [&](const HumanPlayer& player)
		{
			static constexpr uint8_t kNumberOfResourceNeeded = 1;
			return (player.hand.numberOfCards(Resource::Type::Brick) >= kNumberOfResourceNeeded
				&& player.hand.numberOfCards(Resource::Type::Lumber) >= kNumberOfResourceNeeded
				&& player.hand.numberOfCards(Resource::Type::Wool) >= kNumberOfResourceNeeded
				&& player.hand.numberOfCards(Resource::Type::Grain) >= kNumberOfResourceNeeded);
		};

		static constexpr uint8_t kMinimumThreshold = 1;

		static constexpr auto corners = std::array
		{
			Hex::Corner::Bottom, Hex::Corner::BottomLeft, Hex::Corner::BottomRight, 
			Hex::Corner::Upper, Hex::Corner::UpperLeft, Hex::Corner::UpperRight
		};

		for (uint8_t i = 0; i < boardContainer.size(); ++i)
		{
			for (uint8_t j = 0; j < boardContainer[i].size(); ++j)
			{
				if (!boardContainer[i][j])
					continue;

				if (auto resource = boardContainer[i][j]->resource();
					(resource.getType() == Resource::Type::NotAType) ||
					(resource.getType() == Resource::Type::Water) ||
					(resource.getType() == Resource::Type::Nothing))
					continue;

				const auto& hex = boardContainer[i][j]->hex;

				for (auto corner : corners)
				{
					auto numberOfDifferentResourceTypes =
						getBestPlaceLocation(*boardContainer[i][j], HumanPlayer::CornerPosition(i, j, corner));

					const auto&[firstOffsets, secondOffsets] = Hex::Data::Corners::adjacentTiles.at(corner);

					const auto& firstNeighbour = board->getTile(i + firstOffsets.first, j + firstOffsets.second);
					const auto& secondNeighbour = board->getTile(i + secondOffsets.first, j + secondOffsets.second);

					auto adjacentTileProbabilitySum = sumOfAdjacentTiles(firstNeighbour, secondNeighbour, i, j);

					if (numberOfDifferentResourceTypes > kMinimumThreshold)
					{
						if (adjacentTileProbabilitySum > bestLocationToplace.second)
						{
							if(isFirstTurn)
								bestLocationToplace = std::make_pair(HumanPlayer::CornerPosition(i, j, corner), adjacentTileProbabilitySum);
							else
							{
								if (hasEnoughSettlementResources(*player))
								{
									if (player->hasAdjacentRoad(*board, *boardContainer[i][j], HumanPlayer::CornerPosition(i, j, corner)))
									{
										bestLocationToplace = std::make_pair(HumanPlayer::CornerPosition(i, j, corner), adjacentTileProbabilitySum);
									}
								}
							}
						}
					}
				}	
			}
		}
		return bestLocationToplace.first;
	}
}

