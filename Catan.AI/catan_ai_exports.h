
#ifndef CATAN_AI_EXPORTS_H
#define CATAN_AI_EXPORTS_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define CATAN_AI_EXPORTS
#  define CATAN_AI_NO_EXPORT
#else
#  ifndef CATAN_AI_EXPORTS
#    ifdef Catan_AI_EXPORTS
        /* We are building this library */
#      define CATAN_AI_EXPORTS __declspec(dllexport)
#    else
        /* We are using this library */
#      define CATAN_AI_EXPORTS __declspec(dllimport)
#    endif
#  endif

#  ifndef CATAN_AI_NO_EXPORT
#    define CATAN_AI_NO_EXPORT 
#  endif
#endif

#ifndef CATAN_AI_DEPRECATED
#  define CATAN_AI_DEPRECATED __declspec(deprecated)
#endif

#ifndef CATAN_AI_DEPRECATED_EXPORT
#  define CATAN_AI_DEPRECATED_EXPORT CATAN_AI_EXPORTS CATAN_AI_DEPRECATED
#endif

#ifndef CATAN_AI_DEPRECATED_NO_EXPORT
#  define CATAN_AI_DEPRECATED_NO_EXPORT CATAN_AI_NO_EXPORT CATAN_AI_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef CATAN_AI_NO_DEPRECATED
#    define CATAN_AI_NO_DEPRECATED
#  endif
#endif

#endif /* CATAN_AI_EXPORTS_H */
